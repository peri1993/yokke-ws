package com.mega.ws.util;

import java.io.ByteArrayOutputStream;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

public class QrHelperSettings {
	
	public static byte[] getQrCodeImage (String text, int width, int height) {
		try {
			QRCodeWriter qrWriter 	= new QRCodeWriter();
			BitMatrix bitMatrix 	= qrWriter.encode(text, BarcodeFormat.QR_CODE, width, height);
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			MatrixToImageWriter.writeToStream(bitMatrix, "png", byteArrayOutputStream);
			byte[] pngData = byteArrayOutputStream.toByteArray();
			return pngData;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
