package com.mega.ws.util;

import java.nio.charset.Charset;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.google.api.client.repackaged.org.apache.commons.codec.binary.Base64;
import com.mega.ws.config.JbpmConfig;

@Component
public class HeaderSettings {

	@Autowired
	private JbpmConfig jbpmConfig;
	
	public HttpHeaders setupHeaderWithAuth() {
		// setup header auth
		HttpHeaders httpHeaders = new HttpHeaders();
		String encoding = jbpmConfig.getUsername() + ":" + jbpmConfig.getPassword();
		byte[] encodedAuth = Base64.encodeBase64(encoding.getBytes(Charset.forName("US-ASCII")));
		String authHeader = new String(encodedAuth);

		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", "Basic " + authHeader);
		headers.add("Content-Type", "application/json");
		headers.add("Accept", "application/json");
		
		return headers;
	}

	public HttpHeaders setupHeader() {
		HttpHeaders httpHeaders = new HttpHeaders();

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json");
		headers.add("Accept", "application/json");
		//headers.add("Authorization", "Bearer " + auth);
		return httpHeaders;
	}
	
	public HttpHeaders setupHeaderContentType() {
		HttpHeaders httpHeaders = new HttpHeaders();

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json");
		
		return httpHeaders;
	}
	
	public HttpHeaders setupHeaderAccept() {
		HttpHeaders httpHeaders = new HttpHeaders();

		HttpHeaders headers = new HttpHeaders();
		headers.add("Accept", "application/json");
		
		return httpHeaders;
	}
}
