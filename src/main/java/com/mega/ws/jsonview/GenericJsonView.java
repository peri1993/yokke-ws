package com.mega.ws.jsonview;


public class GenericJsonView {
	
//    public static class OnlyId{
//		
//	}OnlyId
	
	public static class Simple {
		
	}

	public static class Detail  extends Simple{
		
	}
	
	public static class Summary  extends Detail{
	
	}

	public static class Full extends Summary {
	}
	
	public static class Meet extends Summary{
		
	}
}
