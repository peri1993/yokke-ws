package com.mega.ws.request;

public class LeaveStatusRequest {
	private String leave_status;

	public String getLeave_status() {
		return leave_status;
	}

	public void setLeave_status(String leave_status) {
		this.leave_status = leave_status;
	}

}
