package com.mega.ws.request;

public class ApprovalChangeRequest {

	private String ds1_id;
	private String ds2_id;
	private String instancesId;
	private String username;
	private String assigner;

	public String getAssigner() {
		return assigner;
	}

	public void setAssigner(String assigner) {
		this.assigner = assigner;
	}

	public String getDs1_id() {
		return ds1_id;
	}

	public void setDs1_id(String ds1_id) {
		this.ds1_id = ds1_id;
	}

	public String getDs2_id() {
		return ds2_id;
	}

	public void setDs2_id(String ds2_id) {
		this.ds2_id = ds2_id;
	}

	public String getInstancesId() {
		return instancesId;
	}

	public void setInstancesId(String instancesId) {
		this.instancesId = instancesId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

}
