package com.mega.ws.request;

public class BranchRequest {

	private BranchIdRequest id;
	private String address;
	private String empSubAreaDesc;
	private String npwp;
	private String latitudeCode;
	private String longitudeCode;
	private String radius;
	
	public String getRadius() {
		return radius;
	}

	public void setRadius(String radius) {
		this.radius = radius;
	}

	public BranchIdRequest getId() {
		return id;
	}

	public void setId(BranchIdRequest id) {
		this.id = id;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmpSubAreaDesc() {
		return empSubAreaDesc;
	}

	public void setEmpSubAreaDesc(String empSubAreaDesc) {
		this.empSubAreaDesc = empSubAreaDesc;
	}

	public String getNpwp() {
		return npwp;
	}

	public void setNpwp(String npwp) {
		this.npwp = npwp;
	}

	public String getLatitudeCode() {
		return latitudeCode;
	}

	public void setLatitudeCode(String latitudeCode) {
		this.latitudeCode = latitudeCode;
	}

	public String getLongitudeCode() {
		return longitudeCode;
	}

	public void setLongitudeCode(String longitudeCode) {
		this.longitudeCode = longitudeCode;
	}

}
