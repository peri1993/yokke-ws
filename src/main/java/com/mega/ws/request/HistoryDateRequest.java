package com.mega.ws.request;

public class HistoryDateRequest {

	private String username;
	private String absencesType;
	private String startDate;
	private String endDate;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getAbsencesType() {
		return absencesType;
	}

	public void setAbsencesType(String absencesType) {
		this.absencesType = absencesType;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

}
