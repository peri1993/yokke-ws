package com.mega.ws.request;

public class LeaveRequest extends BaseModuleRequest {

	private String employee_id;
	private String employee_name;
	private String ds1_id;
	private String approval_ds_1;
	private String ds2_id;
	private String approval_ds_2;
	private String comments;
	private String leave_status;
	private String leave_reason;
	private String subject;
	private String leave_start_date;
	private String leave_end_date;
	private String quota_taken;
	private String absences_type;
	private String absenceDescription;
	private String description;
	private String remaining_quota;
	private String lastRemaining;
	private String total_taken_quota;
	private String carryOverFlag;

	public String getLastRemaining() {
		return lastRemaining;
	}

	public void setLastRemaining(String lastRemaining) {
		this.lastRemaining = lastRemaining;
	}

	public String getCarryOverFlag() {
		return carryOverFlag;
	}

	public void setCarryOverFlag(String carryOverFlag) {
		this.carryOverFlag = carryOverFlag;
	}

	public String getRemaining_quota() {
		return remaining_quota;
	}

	public void setRemaining_quota(String remaining_quota) {
		this.remaining_quota = remaining_quota;
	}

	public String getTotal_taken_quota() {
		return total_taken_quota;
	}

	public void setTotal_taken_quota(String total_taken_quota) {
		this.total_taken_quota = total_taken_quota;
	}

	public String getAbsenceDescription() {
		return absenceDescription;
	}

	public void setAbsenceDescription(String absenceDescription) {
		this.absenceDescription = absenceDescription;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAbsences_type() {
		return absences_type;
	}

	public void setAbsences_type(String absences_type) {
		this.absences_type = absences_type;
	}

	public String getQuota_taken() {
		return quota_taken;
	}

	public void setQuota_taken(String quota_taken) {
		this.quota_taken = quota_taken;
	}

	public String getLeave_start_date() {
		return leave_start_date;
	}

	public void setLeave_start_date(String leave_start_date) {
		this.leave_start_date = leave_start_date;
	}

	public String getLeave_end_date() {
		return leave_end_date;
	}

	public void setLeave_end_date(String leave_end_date) {
		this.leave_end_date = leave_end_date;
	}

	public String getEmployee_id() {
		return employee_id;
	}

	public void setEmployee_id(String employee_id) {
		this.employee_id = employee_id;
	}

	public String getEmployee_name() {
		return employee_name;
	}

	public void setEmployee_name(String employee_name) {
		this.employee_name = employee_name;
	}

	public String getDs1_id() {
		return ds1_id;
	}

	public void setDs1_id(String ds1_id) {
		this.ds1_id = ds1_id;
	}

	public String getApproval_ds_1() {
		return approval_ds_1;
	}

	public void setApproval_ds_1(String approval_ds_1) {
		this.approval_ds_1 = approval_ds_1;
	}

	public String getDs2_id() {
		return ds2_id;
	}

	public void setDs2_id(String ds2_id) {
		this.ds2_id = ds2_id;
	}

	public String getApproval_ds_2() {
		return approval_ds_2;
	}

	public void setApproval_ds_2(String approval_ds_2) {
		this.approval_ds_2 = approval_ds_2;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getLeave_status() {
		return leave_status;
	}

	public void setLeave_status(String leave_status) {
		this.leave_status = leave_status;
	}

	public String getLeave_reason() {
		return leave_reason;
	}

	public void setLeave_reason(String leave_reason) {
		this.leave_reason = leave_reason;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

}
