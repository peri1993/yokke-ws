package com.mega.ws.request;

public class AttendanceRequest {

	private AttendanceLogsIdRequest id;
	private String empSubAreaDescription;
	private String longitudeCode;
	private String latitudeCode;
	private String radius;
	private String empArea;
	private String empSubArea;
	private String barcodeTime;
	private UploadFileAttendanceRequest pathImages;
	
	public String getBarcodeTime() {
		return barcodeTime;
	}

	public void setBarcodeTime(String barcodeTime) {
		this.barcodeTime = barcodeTime;
	}

	public UploadFileAttendanceRequest getPathImages() {
		return pathImages;
	}

	public void setPathImages(UploadFileAttendanceRequest pathImages) {
		this.pathImages = pathImages;
	}

	public AttendanceLogsIdRequest getId() {
		return id;
	}

	public void setId(AttendanceLogsIdRequest id) {
		this.id = id;
	}

	public String getEmpSubAreaDescription() {
		return empSubAreaDescription;
	}

	public void setEmpSubAreaDescription(String empSubAreaDescription) {
		this.empSubAreaDescription = empSubAreaDescription;
	}

	public String getLongitudeCode() {
		return longitudeCode;
	}

	public void setLongitudeCode(String longitudeCode) {
		this.longitudeCode = longitudeCode;
	}

	public String getLatitudeCode() {
		return latitudeCode;
	}

	public void setLatitudeCode(String latitudeCode) {
		this.latitudeCode = latitudeCode;
	}

	public String getRadius() {
		return radius;
	}

	public void setRadius(String radius) {
		this.radius = radius;
	}

	public String getEmpArea() {
		return empArea;
	}

	public void setEmpArea(String empArea) {
		this.empArea = empArea;
	}

	public String getEmpSubArea() {
		return empSubArea;
	}

	public void setEmpSubArea(String empSubArea) {
		this.empSubArea = empSubArea;
	}

}
