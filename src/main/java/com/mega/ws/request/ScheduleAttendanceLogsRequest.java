package com.mega.ws.request;

public class ScheduleAttendanceLogsRequest {
	
	private ScheduleAttendanceLogsIdRequest id;
	private String address;
	private String timeStart;
	private String timeEnd;
	
	public ScheduleAttendanceLogsIdRequest getId() {
		return id;
	}
	public void setId(ScheduleAttendanceLogsIdRequest id) {
		this.id = id;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getTimeStart() {
		return timeStart;
	}
	public void setTimeStart(String timeStart) {
		this.timeStart = timeStart;
	}
	public String getTimeEnd() {
		return timeEnd;
	}
	public void setTimeEnd(String timeEnd) {
		this.timeEnd = timeEnd;
	}
	
}
