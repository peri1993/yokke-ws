package com.mega.ws.request;

import java.sql.Timestamp;
import java.util.List;

public class RoleRequest {

	private Long role_id;
	private String roleName;
	private String createdBy;
	private Timestamp createdDt;
	private String updatedBy;
	private Timestamp updatedDt;
	private List<MenuRequest> listMenu;

	public Long getRole_id() {
		return role_id;
	}

	public void setRole_id(Long role_id) {
		this.role_id = role_id;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Timestamp createdDt) {
		this.createdDt = createdDt;
	}

	public List<MenuRequest> getListMenu() {
		return listMenu;
	}

	public void setListMenu(List<MenuRequest> listMenu) {
		this.listMenu = listMenu;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Timestamp updatedDt) {
		this.updatedDt = updatedDt;
	}

}
