package com.mega.ws.request;

public class ChangeWorkHoursRequest extends BaseModuleRequest {

	private String employee_id;
	private String employee_name;
	private String start_time;
	private String end_time;
	private String reason;
	private String approval_ds_1;
	private String approval_ds_1_name;
	private String approval_ds_2;
	private String approval_ds_2_name;
	private String status;
	private String hours_id;

	public String getApproval_ds_2() {
		return approval_ds_2;
	}

	public void setApproval_ds_2(String approval_ds_2) {
		this.approval_ds_2 = approval_ds_2;
	}

	public String getApproval_ds_2_name() {
		return approval_ds_2_name;
	}

	public void setApproval_ds_2_name(String approval_ds_2_name) {
		this.approval_ds_2_name = approval_ds_2_name;
	}

	public String getHours_id() {
		return hours_id;
	}

	public void setHours_id(String hours_id) {
		this.hours_id = hours_id;
	}

	public String getEmployee_id() {
		return employee_id;
	}

	public void setEmployee_id(String employee_id) {
		this.employee_id = employee_id;
	}

	public String getEmployee_name() {
		return employee_name;
	}

	public void setEmployee_name(String employee_name) {
		this.employee_name = employee_name;
	}

	public String getStart_time() {
		return start_time;
	}

	public void setStart_time(String start_time) {
		this.start_time = start_time;
	}

	public String getEnd_time() {
		return end_time;
	}

	public void setEnd_time(String end_time) {
		this.end_time = end_time;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getApproval_ds_1() {
		return approval_ds_1;
	}

	public void setApproval_ds_1(String approval_ds_1) {
		this.approval_ds_1 = approval_ds_1;
	}

	public String getApproval_ds_1_name() {
		return approval_ds_1_name;
	}

	public void setApproval_ds_1_name(String approval_ds_1_name) {
		this.approval_ds_1_name = approval_ds_1_name;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
