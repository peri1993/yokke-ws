package com.mega.ws.request;

public class ApproveRequest {
	private String username;
	private String leave_status;
	private String comments;
	private String taskId;
	private String instancesId;

	public String getInstancesId() {
		return instancesId;
	}

	public void setInstancesId(String instancesId) {
		this.instancesId = instancesId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getLeave_status() {
		return leave_status;
	}

	public void setLeave_status(String leave_status) {
		this.leave_status = leave_status;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

}
