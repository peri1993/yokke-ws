package com.mega.ws.jbpm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import com.mega.ws.config.JbpmConfig;
import com.mega.ws.response.JBPM_MappingResponse;

@Component
public class MappingPermitURL {

	@Autowired
	JbpmConfig jbpmConfig;

	private JBPM_MappingResponse mapping = new JBPM_MappingResponse();

	public JBPM_MappingResponse initiateLeaveRequest() {

		mapping.setUrl(jbpmConfig.getUrl() + "/server/containers/" + jbpmConfig.getContainer() + "/processes/"
				+ jbpmConfig.getPermit_processid() + "/instances");
		mapping.setMethod(HttpMethod.POST);

		return mapping;
	}

	public JBPM_MappingResponse submitLeave(String taskId) {

		mapping.setUrl(jbpmConfig.getUrl() + "server/containers/" + jbpmConfig.getContainer() + "/tasks/" + taskId
				+ "/states/completed");
		mapping.setMethod(HttpMethod.PUT);

		return mapping;
	}

	public JBPM_MappingResponse showActiveTaskByProcessId(Integer processId) {

		mapping.setUrl(jbpmConfig.getUrl() + "/server/queries/processes/instances/" + processId);
		mapping.setMethod(HttpMethod.GET);

		return mapping;
	}

	public JBPM_MappingResponse completedTask(Integer taskId) {

		mapping.setUrl(jbpmConfig.getUrl() + "server/containers/" + jbpmConfig.getContainer() + "/tasks/" + taskId
				+ "/states/completed");
		mapping.setMethod(HttpMethod.PUT);

		return mapping;
	}

	public JBPM_MappingResponse instancesOwner() {

		mapping.setUrl(jbpmConfig.getUrl() + "server/queries/tasks/instances/owners");
		mapping.setMethod(HttpMethod.GET);

		return mapping;
	}

	public JBPM_MappingResponse instancesVariables(Integer instancesId) {

		mapping.setUrl(jbpmConfig.getUrl() + "server/containers/" + jbpmConfig.getContainer() + "/processes/instances/"
				+ instancesId + "/variables/instances");
		mapping.setMethod(HttpMethod.GET);

		return mapping;
	}

	public JBPM_MappingResponse cekActiveData(String instancesId) {

		mapping.setUrl(jbpmConfig.getUrl() + "server/queries/processes/instances/" + instancesId);
		mapping.setMethod(HttpMethod.GET);

		return mapping;
	}

	public JBPM_MappingResponse activeTask() {

		mapping.setUrl(jbpmConfig.getUrl() + "server/queries/tasks/instances/pot-owners");
		mapping.setMethod(HttpMethod.GET);

		return mapping;
	}

}
