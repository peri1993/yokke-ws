package com.mega.ws.jbpm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import com.mega.ws.config.JbpmConfig;
import com.mega.ws.config.MinovaConfig;
import com.mega.ws.response.JBPM_MappingResponse;

@Component
public class MappingURL {

	@Autowired
	JbpmConfig jbpmConfig;

	@Autowired
	MinovaConfig minovaConfig;

	private JBPM_MappingResponse mapping = new JBPM_MappingResponse();

	public JBPM_MappingResponse initiateLeaveRequest() {

		mapping.setUrl(jbpmConfig.getUrl() + "/server/containers/" + jbpmConfig.getContainer() + "/processes/"
				+ jbpmConfig.getLeave_processid() + "/instances");
		mapping.setMethod(HttpMethod.POST);

		return mapping;
	}
	
	public JBPM_MappingResponse initiateAbsenceRequest() {

		mapping.setUrl(jbpmConfig.getUrl() + "/server/containers/" + jbpmConfig.getContainer() + "/processes/"
				+ jbpmConfig.getAbsence_processid() + "/instances");
		mapping.setMethod(HttpMethod.POST);

		return mapping;
	}

	public JBPM_MappingResponse submitLeave(String taskId) {

		mapping.setUrl(jbpmConfig.getUrl() + "server/containers/" + jbpmConfig.getContainer() + "/tasks/" + taskId
				+ "/states/completed");
		mapping.setMethod(HttpMethod.PUT);

		return mapping;
	}

	public JBPM_MappingResponse showActiveTaskByProcessId(Integer processId) {

		mapping.setUrl(jbpmConfig.getUrl() + "/server/queries/processes/instances/" + processId);
		mapping.setMethod(HttpMethod.GET);

		return mapping;
	}

	public JBPM_MappingResponse completedTask(Integer taskId) {

		mapping.setUrl(jbpmConfig.getUrl() + "server/containers/" + jbpmConfig.getContainer() + "/tasks/" + taskId
				+ "/states/completed");
		mapping.setMethod(HttpMethod.PUT);

		return mapping;
	}

	public JBPM_MappingResponse instancesOwner() {

		mapping.setUrl(jbpmConfig.getUrl() + "server/queries/tasks/instances/owners");
		mapping.setMethod(HttpMethod.GET);

		return mapping;
	}

	public JBPM_MappingResponse instancesVariables(Integer instancesId) {

		mapping.setUrl(jbpmConfig.getUrl() + "server/containers/" + jbpmConfig.getContainer() + "/processes/instances/"
				+ instancesId + "/variables/instances");
		mapping.setMethod(HttpMethod.GET);

		return mapping;
	}

	public JBPM_MappingResponse cekActiveData(String instancesId) {

		mapping.setUrl(jbpmConfig.getUrl() + "server/queries/processes/instances/" + instancesId);
		mapping.setMethod(HttpMethod.GET);

		return mapping;
	}

	public JBPM_MappingResponse activeTask() {

		mapping.setUrl(jbpmConfig.getUrl() + "server/queries/tasks/instances/pot-owners");
		mapping.setMethod(HttpMethod.GET);

		return mapping;
	}

	public JBPM_MappingResponse getTrackingTaskByInstancesId(Long instancesId) {

		mapping.setUrl(jbpmConfig.getUrl() + "server/admin/containers/" + jbpmConfig.getContainer()
				+ "/processes/instances/" + instancesId + "/nodeinstances");
		mapping.setMethod(HttpMethod.GET);

		return mapping;
	}

	public JBPM_MappingResponse getValueApproval(Long instancesId, String variable) {

		mapping.setUrl(jbpmConfig.getUrl() + "server/containers/" + jbpmConfig.getContainer() + "/processes/instances/"
				+ instancesId + "/variable/" + variable);
		mapping.setMethod(HttpMethod.GET);

		return mapping;
	}

	public JBPM_MappingResponse getValueCreateTaskByInstancesId(Long instancesId) {

		mapping.setUrl(jbpmConfig.getUrl() + "server/containers/" + jbpmConfig.getContainer() + "/processes/instances/"
				+ instancesId + "/variable/employee_id");
		mapping.setMethod(HttpMethod.GET);

		return mapping;
	}

	public JBPM_MappingResponse findName(String username) {

		mapping.setUrl(minovaConfig.getUrl() + "request/approval/" + username);
		mapping.setMethod(HttpMethod.GET);

		return mapping;
	}

	public JBPM_MappingResponse checkBlockLeave(String username) {

		mapping.setUrl(minovaConfig.getUrl() + "history/check-block-leave/" + username);
		mapping.setMethod(HttpMethod.GET);

		return mapping;
	}

	public JBPM_MappingResponse getLeaveQuota(String absenceType) {

		mapping.setUrl(minovaConfig.getUrl() + "/findLeaveQuota/" + absenceType);
		mapping.setMethod(HttpMethod.GET);

		return mapping;
	}

	public JBPM_MappingResponse getInformationLogin(String username) {

		mapping.setUrl(minovaConfig.getUrl() + "/find-job-desc/" + username);
		mapping.setMethod(HttpMethod.GET);

		return mapping;
	}

	public JBPM_MappingResponse findLastRemaining(String username) {

		mapping.setUrl(minovaConfig.getUrl() + "/find-last-remaining/" + username);
		mapping.setMethod(HttpMethod.GET);

		return mapping;
	}

	public JBPM_MappingResponse findLastRemainingCarryOver(String username) {

		mapping.setUrl(minovaConfig.getUrl() + "/find-last-remaining-carry-over/" + username);
		mapping.setMethod(HttpMethod.GET);

		return mapping;
	}

	public JBPM_MappingResponse updateData() {

		mapping.setUrl(minovaConfig.getUrl() + "/update-leave");
		mapping.setMethod(HttpMethod.POST);

		return mapping;
	}

	public JBPM_MappingResponse findByAbsenceType(String username, String absencesType) {

		mapping.setUrl(minovaConfig.getUrl() + "/find-absence-type/" + username + "/" + absencesType);
		mapping.setMethod(HttpMethod.GET);

		return mapping;
	}

	public JBPM_MappingResponse updateApproval() {

		mapping.setUrl(minovaConfig.getUrl() + "/update-approval");
		mapping.setMethod(HttpMethod.POST);

		return mapping;
	}

	public JBPM_MappingResponse findUsernameByExternalId(String externalId) {

		mapping.setUrl(minovaConfig.getUrl() + "/find-username-by-external-id/" + externalId);
		mapping.setMethod(HttpMethod.GET);

		return mapping;
	}

	public JBPM_MappingResponse findListUnder(String username) {

		mapping.setUrl(minovaConfig.getUrlAttendance() + "/find-list-under/" + username);
		mapping.setMethod(HttpMethod.GET);

		return mapping;
	}

	public JBPM_MappingResponse findDateHistory() {
		mapping.setUrl(minovaConfig.getUrl() + "/find-date-history");
		mapping.setMethod(HttpMethod.POST);

		return mapping;
	}

	public JBPM_MappingResponse checkDeduction(String username, String startDate, String endDate) {
		mapping.setUrl(minovaConfig.getUrl() + "/check-deduction-validate/" + username + "/startDate/" + startDate
				+ "/endDate/" + endDate);
		mapping.setMethod(HttpMethod.GET);

		return mapping;
	}

	public JBPM_MappingResponse schedulerBracnch() {
		mapping.setUrl(minovaConfig.getUrlAttendance() + "/find-list-branch");
		mapping.setMethod(HttpMethod.POST);

		return mapping;
	}

	public JBPM_MappingResponse getHolidayBetween(String startDate, String endDate) {
		mapping.setUrl(minovaConfig.getUrl() + "/getHolidayBetween/" + startDate + "/" + endDate);
		mapping.setMethod(HttpMethod.GET);

		return mapping;
	}

	public JBPM_MappingResponse findListBranchById(String username) {
		mapping.setUrl(minovaConfig.getUrlAttendance() + "/find-branch-byid/" + username);
		mapping.setMethod(HttpMethod.GET);

		return mapping;
	}

	public JBPM_MappingResponse findAllEmployeeActiveForAbsences() {
		mapping.setUrl(minovaConfig.getUrl() + "/find-all-employee-id-active");
		mapping.setMethod(HttpMethod.GET);

		return mapping;
	}
	
	public JBPM_MappingResponse createAttendance() {
		mapping.setUrl(minovaConfig.getUrl() + "/create-attendance");
		mapping.setMethod(HttpMethod.POST);
		
		return mapping;
	}
	
	public JBPM_MappingResponse findDateDeduction(String username) {
		mapping.setUrl(minovaConfig.getUrl() + "/find-date-deduction/" + username);
		mapping.setMethod(HttpMethod.GET);

		return mapping;
	}

}
