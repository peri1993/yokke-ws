package com.mega.ws.response;

public class AttendanceLogsResponse {
	private String empId;
	private String fullName;
	private String empSubAreaDescription;
	private String empArea;
	private String empSubArea;
	private String landscape;
	private String startDate;
	private String endDate;
	private String latitudeCode;
	private String longitudeCode;
	private String radius;
	private String timeStart;
	private String timeEnd;
	
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	public String getEmpSubAreaDescription() {
		return empSubAreaDescription;
	}
	public void setEmpSubAreaDescription(String empSubAreaDescription) {
		this.empSubAreaDescription = empSubAreaDescription;
	}
	public String getEmpArea() {
		return empArea;
	}
	public void setEmpArea(String empArea) {
		this.empArea = empArea;
	}
	public String getEmpSubArea() {
		return empSubArea;
	}
	public void setEmpSubArea(String empSubArea) {
		this.empSubArea = empSubArea;
	}
	public String getLandscape() {
		return landscape;
	}
	public void setLandscape(String landscape) {
		this.landscape = landscape;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getLatitudeCode() {
		return latitudeCode;
	}
	public void setLatitudeCode(String latitudeCode) {
		this.latitudeCode = latitudeCode;
	}
	public String getLongitudeCode() {
		return longitudeCode;
	}
	public void setLongitudeCode(String longitudeCode) {
		this.longitudeCode = longitudeCode;
	}
	public String getRadius() {
		return radius;
	}
	public void setRadius(String radius) {
		this.radius = radius;
	}
	public String getTimeStart() {
		return timeStart;
	}
	public void setTimeStart(String timeStart) {
		this.timeStart = timeStart;
	}
	public String getTimeEnd() {
		return timeEnd;
	}
	public void setTimeEnd(String timeEnd) {
		this.timeEnd = timeEnd;
	}
}
