package com.mega.ws.response;

public class TaskResponse {

	private String taskId;
	private String taskName;
	private String taskSubject;
	private String taskDescription;
	private String taskStatus;
	private String taskCreatedBy;
	private String taskCreatedDate;
	private String taskInstancesId;

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public String getTaskSubject() {
		return taskSubject;
	}

	public void setTaskSubject(String taskSubject) {
		this.taskSubject = taskSubject;
	}

	public String getTaskDescription() {
		return taskDescription;
	}

	public void setTaskDescription(String taskDescription) {
		this.taskDescription = taskDescription;
	}

	public String getTaskStatus() {
		return taskStatus;
	}

	public void setTaskStatus(String taskStatus) {
		this.taskStatus = taskStatus;
	}

	public String getTaskCreatedBy() {
		return taskCreatedBy;
	}

	public void setTaskCreatedBy(String taskCreatedBy) {
		this.taskCreatedBy = taskCreatedBy;
	}

	public String getTaskCreatedDate() {
		return taskCreatedDate;
	}

	public void setTaskCreatedDate(String taskCreatedDate) {
		this.taskCreatedDate = taskCreatedDate;
	}

	public String getTaskInstancesId() {
		return taskInstancesId;
	}

	public void setTaskInstancesId(String taskInstancesId) {
		this.taskInstancesId = taskInstancesId;
	}

}
