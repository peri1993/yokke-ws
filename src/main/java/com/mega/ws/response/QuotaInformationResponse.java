package com.mega.ws.response;

public class QuotaInformationResponse {

	private Integer lastRemaining;
	private Integer processRemaining;
	private Integer total;

	public Integer getLastRemaining() {
		return lastRemaining;
	}

	public void setLastRemaining(Integer lastRemaining) {
		this.lastRemaining = lastRemaining;
	}

	public Integer getProcessRemaining() {
		return processRemaining;
	}

	public void setProcessRemaining(Integer processRemaining) {
		this.processRemaining = processRemaining;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

}
