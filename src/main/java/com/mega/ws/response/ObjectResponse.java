package com.mega.ws.response;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ObjectResponse<T> extends BaseResponse {

    private T value;

    public ObjectResponse(String responseValue, int status, T objectResponse) {
        super(responseValue, status);
        this.value = objectResponse;
    }

    public T getObjectResponse() {
        return value;
    }

    public void setObjectResponse(T objectResponse) {
        this.value = objectResponse;
    }

}
