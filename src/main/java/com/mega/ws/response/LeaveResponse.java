package com.mega.ws.response;

public class LeaveResponse {

	private String employee_id;
	private String employee_name;
	private String ds1_id;
	private String approval_ds_1;
	private String ds2_id;
	private String approval_ds_2;
	private String comments;
	private String leave_status;
	private String leave_reason;
	private String subject;

	public String getEmployee_id() {
		return employee_id;
	}

	public void setEmployee_id(String employee_id) {
		this.employee_id = employee_id;
	}

	public String getEmployee_name() {
		return employee_name;
	}

	public void setEmployee_name(String employee_name) {
		this.employee_name = employee_name;
	}

	public String getDs1_id() {
		return ds1_id;
	}

	public void setDs1_id(String ds1_id) {
		this.ds1_id = ds1_id;
	}

	public String getApproval_ds_1() {
		return approval_ds_1;
	}

	public void setApproval_ds_1(String approval_ds_1) {
		this.approval_ds_1 = approval_ds_1;
	}

	public String getDs2_id() {
		return ds2_id;
	}

	public void setDs2_id(String ds2_id) {
		this.ds2_id = ds2_id;
	}

	public String getApproval_ds_2() {
		return approval_ds_2;
	}

	public void setApproval_ds_2(String approval_ds_2) {
		this.approval_ds_2 = approval_ds_2;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getLeave_status() {
		return leave_status;
	}

	public void setLeave_status(String leave_status) {
		this.leave_status = leave_status;
	}

	public String getLeave_reason() {
		return leave_reason;
	}

	public void setLeave_reason(String leave_reason) {
		this.leave_reason = leave_reason;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

}
