package com.mega.ws.response;

public class QuotaLeaveResponse {
	
	private int total;

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}
	
}
