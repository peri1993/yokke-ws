package com.mega.ws.response;

import java.util.List;

public class ListResponse<T> extends BaseResponse  {

    private List<T> value;

    public ListResponse(String responseValue, int status, List<T> value) {
        super(responseValue, status);
        this.value = value;
    }

    public List<T> getListResponse() {
        return value;
    }

    public void setListResponse(List<T> listResponse) {
        this.value = listResponse;
    }

}
