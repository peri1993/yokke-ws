package com.mega.ws.response;

import java.util.List;

public class PageResponse<T> extends BaseResponse {

	private List<T> listResponse;

	private int pageNumber;
	private int pageSize;
	private int totalPage;
	private boolean last;
	private Long totalRows;

	public PageResponse(String message, int status, List<T> listResponse, int pageNumber, int pageSize, int totalPage,
			boolean last, Long totalRows) {
		super(message, status);
		this.listResponse = listResponse;
		this.pageNumber = pageNumber;
		this.pageSize = pageSize;
		this.totalPage = totalPage;
		this.last = last;
		this.totalRows = totalRows;
	}

	public List<T> getListResponse() {
		return listResponse;
	}

	public void setListResponse(List<T> listResponse) {
		this.listResponse = listResponse;
	}

	public int getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}

	public boolean isLast() {
		return last;
	}

	public void setLast(boolean last) {
		this.last = last;
	}

	public Long getTotalRows() {
		return totalRows;
	}

	public void setTotalRows(Long totalRows) {
		this.totalRows = totalRows;
	}

}
