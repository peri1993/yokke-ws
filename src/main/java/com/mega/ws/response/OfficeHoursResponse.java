package com.mega.ws.response;

public class OfficeHoursResponse {

	private String id;
	private String name;
	private String startTime;
	private String endtTime;
	private String createdBy;
	private String createdDt;
	private String updatedBy;
	private String updatedDt;
	private String locationExtend;
	private String desc;
	private String reason;
	
	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndtTime() {
		return endtTime;
	}

	public void setEndtTime(String endtTime) {
		this.endtTime = endtTime;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(String createdDt) {
		this.createdDt = createdDt;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(String updatedDt) {
		this.updatedDt = updatedDt;
	}

	public String getLocationExtend() {
		return locationExtend;
	}

	public void setLocationExtend(String locationExtend) {
		this.locationExtend = locationExtend;
	}

}
