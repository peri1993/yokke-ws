package com.mega.ws.response;

public class PropertiesResponse {

    public static String STATUS_MESSAGE_OK = "SUCCESS";

    public static String STATUS_MESSAGE_CREATED = "CREATED";

    public static String STATUS_INTERNAL_SERVER_ERROR = "INTERNAL SERVER ERROR";

    public static String STATUS_CLIENT_ERRROR = "CLIENT BAD REQUEST";

    public static String GCN_MESSAGE_SUCCESS = "Success";

    public static String GCN_MESSAGE_DATA_NOT_FOUND = "Data not found";

    public static String GCN_REQUEST_NOT_COMPLETE = "Request not complete";

    public static String GCN_REQUEST_TIMEOUT = "Request timeout";
    
    public static String INPUT_ERROR_NUMERIC = "Please Input number for ";

    public static int STATUS_CODE_OK = 200;

    public static int STATUS_CODE_CREATED = 201;

    public static int STATUS_CODE_INTERNAL_SERVER_ERROR = 500;

    public static int STATUS_CODE_CLIENT_ERROR = 400;

    public static int GCN_SUCCESS_CODE = 01;

    public static int GCN_DATA_NOT_FOUND_CODE = 02;

    public static int GCN_REQUEST_NOT_COMPLETE_CODE =03;

    public static int GCN_REQUEST_TIMEOUT_CODE =03;
}
