package com.mega.ws.response;

public class UserOfficeHoursResponse {

	private String empId;
	private String fullname;
	private OfficeHoursResponse officeHours;

	public String getEmpId() {
		return empId;
	}

	public void setEmpId(String empId) {
		this.empId = empId;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public OfficeHoursResponse getOfficeHours() {
		return officeHours;
	}

	public void setOfficeHours(OfficeHoursResponse officeHours) {
		this.officeHours = officeHours;
	}

}
