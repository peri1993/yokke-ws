package com.mega.ws.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "uploadpath")
@PropertySource("classpath:config.properties")

public class UploadPathConfig {
	
	private String url;
	private String location_document;
	private String path_file;
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getLocation_document() {
		return location_document;
	}
	public void setLocation_document(String location_document) {
		this.location_document = location_document;
	}
	public String getPath_file() {
		return path_file;
	}
	public void setPath_file(String path_file) {
		this.path_file = path_file;
	}
}
