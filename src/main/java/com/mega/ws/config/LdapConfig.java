package com.mega.ws.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "ldap")
//@PropertySource("file:C:/config.properties")
@PropertySource("classpath:config.properties")
public class LdapConfig {

	private String url;
	private String salt;
	private String secret;
	private String search;
	private String verifyPassword;
	private String changePassword;
	private String resetPassword;
	private String deleteUser;
	private String addUser;
	private String resetPasswordMail;
	private Boolean bypass;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	public String getVerifyPassword() {
		return verifyPassword;
	}

	public void setVerifyPassword(String verifyPassword) {
		this.verifyPassword = verifyPassword;
	}

	public String getChangePassword() {
		return changePassword;
	}

	public void setChangePassword(String changePassword) {
		this.changePassword = changePassword;
	}

	public String getResetPassword() {
		return resetPassword;
	}

	public void setResetPassword(String resetPassword) {
		this.resetPassword = resetPassword;
	}

	public String getDeleteUser() {
		return deleteUser;
	}

	public void setDeleteUser(String deleteUser) {
		this.deleteUser = deleteUser;
	}

	public String getAddUser() {
		return addUser;
	}

	public void setAddUser(String addUser) {
		this.addUser = addUser;
	}

	public String getResetPasswordMail() {
		return resetPasswordMail;
	}

	public void setResetPasswordMail(String resetPasswordMail) {
		this.resetPasswordMail = resetPasswordMail;
	}

	public Boolean getBypass() {
		return bypass;
	}

	public void setBypass(Boolean bypass) {
		this.bypass = bypass;
	}

}
