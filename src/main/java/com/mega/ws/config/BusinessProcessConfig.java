package com.mega.ws.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "process")
@PropertySource("classpath:config.properties")
public class BusinessProcessConfig {
	private String leave;
	private String permit;
	private String carry;
	private String changeHours;

	public String getChangeHours() {
		return changeHours;
	}

	public void setChangeHours(String changeHours) {
		this.changeHours = changeHours;
	}

	public String getCarry() {
		return carry;
	}

	public void setCarry(String carry) {
		this.carry = carry;
	}

	public String getLeave() {
		return leave;
	}

	public void setLeave(String leave) {
		this.leave = leave;
	}

	public String getPermit() {
		return permit;
	}

	public void setPermit(String permit) {
		this.permit = permit;
	}

}
