package com.mega.ws.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "minova")
@PropertySource("classpath:config.properties")
public class MinovaConfig {
	private String url;
	private String urlAttendance;

	public String getUrlAttendance() {
		return urlAttendance;
	}

	public void setUrlAttendance(String urlAttendance) {
		this.urlAttendance = urlAttendance;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
