package com.mega.ws.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "jbpm")
//@PropertySource("file:C:/fwd/config/rre/config.properties")
@PropertySource("classpath:config.properties")
public class JbpmConfig {

	private String url;
	private String container;
	private String leave_processid;
	private String username;
	private String password;
	private String permit_processid;
	private String location_document;
	private String path_file;
	private String absence_processid;

	public String getAbsence_processid() {
		return absence_processid;
	}

	public void setAbsence_processid(String absence_processid) {
		this.absence_processid = absence_processid;
	}

	public String getPath_file() {
		return path_file;
	}

	public void setPath_file(String path_file) {
		this.path_file = path_file;
	}

	public String getLocation_document() {
		return location_document;
	}

	public void setLocation_document(String location_document) {
		this.location_document = location_document;
	}

	public String getPermit_processid() {
		return permit_processid;
	}

	public void setPermit_processid(String permit_processid) {
		this.permit_processid = permit_processid;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getContainer() {
		return container;
	}

	public void setContainer(String container) {
		this.container = container;
	}

	public String getLeave_processid() {
		return leave_processid;
	}

	public void setLeave_processid(String leave_processid) {
		this.leave_processid = leave_processid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
