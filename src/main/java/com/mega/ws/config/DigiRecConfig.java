package com.mega.ws.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "digirec")
//@PropertySource("file:C:/fwd/config/rre/config.properties")
@PropertySource("classpath:config.properties")
public class DigiRecConfig {

	private String plancode;
	private String prefix;
	private String notification_url;
	private String new_candidate_agent_url;
	private String response_url;

	public String getPlancode() {
		return plancode;
	}

	public void setPlancode(String plancode) {
		this.plancode = plancode;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getNotification_url() {
		return notification_url;
	}

	public void setNotification_url(String notification_url) {
		this.notification_url = notification_url;
	}

	public String getNew_candidate_agent_url() {
		return new_candidate_agent_url;
	}

	public void setNew_candidate_agent_url(String new_candidate_agent_url) {
		this.new_candidate_agent_url = new_candidate_agent_url;
	}

	public String getResponse_url() {
		return response_url;
	}

	public void setResponse_url(String response_url) {
		this.response_url = response_url;
	}
}
