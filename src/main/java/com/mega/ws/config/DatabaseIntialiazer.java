package com.mega.ws.config;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.mega.ws.constants.AuthoritiesConstants;
import com.mega.ws.model.Menu;
import com.mega.ws.model.OfficeHours;
import com.mega.ws.model.Role;
import com.mega.ws.model.RoleUser;
import com.mega.ws.repository.MenuRepository;
import com.mega.ws.repository.OfficeHoursRepository;
import com.mega.ws.repository.RoleRepository;
import com.mega.ws.repository.RoleUserRepository;
import com.mega.ws.repository.UserOfficeHoursRepository;

//@Component
public class DatabaseIntialiazer implements CommandLineRunner {

	private RoleRepository roleRepository;
	private MenuRepository menuRepository;
	private RoleUserRepository roleUserRepository;
	private OfficeHoursRepository officeHoursRepository;
	private UserOfficeHoursRepository userOfficeHoursRepository;

	public DatabaseIntialiazer(RoleRepository roleRepository, MenuRepository menuRepository,
			RoleUserRepository roleUserRepository, OfficeHoursRepository officeHoursRepository, UserOfficeHoursRepository userOfficeHoursRepository) {
		this.roleRepository = roleRepository;
		this.menuRepository = menuRepository;
		this.roleUserRepository = roleUserRepository;
		this.officeHoursRepository = officeHoursRepository;
		this.userOfficeHoursRepository = userOfficeHoursRepository;
	}

	@Override
	public void run(String... args) throws Exception {
		Role getRole = roleRepository.findByRoleName("default");
		Role adminRole = roleRepository.findByRoleName("admin");

		if (getRole == null) {
			Role role = new Role();
			role.setRoleName("default");
			role.setCreatedBy("system");
			role.setCreatedDt(new Timestamp(System.currentTimeMillis()));

			Set<Menu> listMenu = new HashSet<Menu>();
			Menu menu1 = new Menu();
			menu1.setName(AuthoritiesConstants.INBOX);
			menu1.setRole(role);
			listMenu.add(menu1);

			Menu menu2 = new Menu();
			menu2.setName(AuthoritiesConstants.REQUEST_LEAVE);
			menu2.setRole(role);
			listMenu.add(menu2);

			Menu menu3 = new Menu();
			menu3.setName(AuthoritiesConstants.HISTORY_LEAVE);
			menu3.setRole(role);
			listMenu.add(menu3);

			Menu menu4 = new Menu();
			menu4.setName(AuthoritiesConstants.REQUEST_PERMIT);
			menu4.setRole(role);
			listMenu.add(menu4);

			Menu menu5 = new Menu();
			menu5.setName(AuthoritiesConstants.HISTORY_PERMIT);
			menu5.setRole(role);
			listMenu.add(menu5);

			Menu menu6 = new Menu();
			menu6.setName(AuthoritiesConstants.NOTIFICATION);
			menu6.setRole(role);
			listMenu.add(menu6);

			Menu menu7 = new Menu();
			menu7.setName(AuthoritiesConstants.HISTORY_ATTENDANCE);
			menu7.setRole(role);
			listMenu.add(menu7);

			Menu menu8 = new Menu();
			menu8.setName(AuthoritiesConstants.LIST_ATTENDANCE);
			menu8.setRole(role);
			listMenu.add(menu8);

			Menu menu9 = new Menu();
			menu9.setName(AuthoritiesConstants.QR_ATTENDANCE);
			menu9.setRole(role);
			listMenu.add(menu9);
			
			Menu menu10 = new Menu();
			menu10.setName(AuthoritiesConstants.ASSIGNER_HISTORY);
			menu10.setRole(role);
			listMenu.add(menu10);
			
			Menu menu11 = new Menu();
			menu11.setName(AuthoritiesConstants.INFORMATION_WORK_HOURS);
			menu11.setRole(role);
			listMenu.add(menu11);
			
			role.setListMenu(listMenu);

			RoleUser roleUser = new RoleUser();
			roleUser.setCreatedBy("system");
			roleUser.setCreatedDt(new Timestamp(System.currentTimeMillis()));
			roleUser.setUsername("default");
			roleUser.setRole(role);

			roleRepository.save(role);
			roleUserRepository.save(roleUser);

		}

		if (adminRole == null) {
			Role role = new Role();
			role.setRoleName("admin");
			role.setCreatedBy("system");
			role.setCreatedDt(new Timestamp(System.currentTimeMillis()));

			Set<Menu> listMenu = new HashSet<Menu>();
			Menu menu1 = new Menu();
			menu1.setName(AuthoritiesConstants.INBOX);
			menu1.setRole(role);
			listMenu.add(menu1);

			Menu menu2 = new Menu();
			menu2.setName(AuthoritiesConstants.REQUEST_LEAVE);
			menu2.setRole(role);
			listMenu.add(menu2);

			Menu menu3 = new Menu();
			menu3.setName(AuthoritiesConstants.HISTORY_LEAVE);
			menu3.setRole(role);
			listMenu.add(menu3);

			Menu menu4 = new Menu();
			menu4.setName(AuthoritiesConstants.REQUEST_PERMIT);
			menu4.setRole(role);
			listMenu.add(menu4);

			Menu menu5 = new Menu();
			menu5.setName(AuthoritiesConstants.HISTORY_PERMIT);
			menu5.setRole(role);
			listMenu.add(menu5);

			Menu menu6 = new Menu();
			menu6.setName(AuthoritiesConstants.NOTIFICATION);
			menu6.setRole(role);
			listMenu.add(menu6);

			Menu menu7 = new Menu();
			menu7.setName(AuthoritiesConstants.RE_ASSIGN_TASK);
			menu7.setRole(role);
			listMenu.add(menu7);

			Menu menu8 = new Menu();
			menu8.setName(AuthoritiesConstants.ROLE_MANAGEMENT);
			menu8.setRole(role);
			listMenu.add(menu8);

			Menu menu9 = new Menu();
			menu9.setName(AuthoritiesConstants.USER_MANAGEMENT);
			menu9.setRole(role);
			listMenu.add(menu9);
			
			Menu menu10 = new Menu();
			menu10.setName(AuthoritiesConstants.HISTORY_ATTENDANCE);
			menu10.setRole(role);
			listMenu.add(menu10);
			
			Menu menu11 = new Menu();
			menu11.setName(AuthoritiesConstants.LIST_ATTENDANCE);
			menu11.setRole(role);
			listMenu.add(menu11);
			
			Menu menu12 = new Menu();
			menu12.setName(AuthoritiesConstants.QR_ATTENDANCE);
			menu12.setRole(role);
			listMenu.add(menu12);
			
			Menu menu13 = new Menu();
			menu13.setName(AuthoritiesConstants.BRANCH_CONF);
			menu13.setRole(role);
			listMenu.add(menu13);
			
			Menu menu14 = new Menu();
			menu14.setName(AuthoritiesConstants.ALL_HISTORY_ASSIGNER);
			menu14.setRole(role);
			listMenu.add(menu14);
			
			Menu menu15 = new Menu();
			menu15.setName(AuthoritiesConstants.INFORMATION_WORK_HOURS);
			menu15.setRole(role);
			listMenu.add(menu15);

			role.setListMenu(listMenu);

			RoleUser roleUser = new RoleUser();
			roleUser.setCreatedBy("system");
			roleUser.setCreatedDt(new Timestamp(System.currentTimeMillis()));
			roleUser.setUsername("11111111");
			roleUser.setFullname("Employee");
			roleUser.setRole(role);

			roleRepository.save(role);
			roleUserRepository.save(roleUser);

		}
		
		List<OfficeHours> listHours = officeHoursRepository.findAll();
		
		if (listHours.isEmpty()) {
			OfficeHours model = new OfficeHours();
			model.setCreatedBy("System");
			model.setCreatedDt(new Timestamp(System.currentTimeMillis()));
			model.setName("default");
			model.setStartTime("08:00");
			model.setEndtTime("17:00");
			model.setLocationExtend("WIB");
			officeHoursRepository.save(model);
			
			OfficeHours model2 = new OfficeHours();
			model2.setCreatedBy("System");
			model2.setCreatedDt(new Timestamp(System.currentTimeMillis()));
			model2.setName("toleran");
			model2.setStartTime("09:00");
			model2.setEndtTime("18:00");
			model2.setLocationExtend("WIB");
			officeHoursRepository.save(model2);
		}

	}

}
