package com.mega.ws.service;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mega.ws.model.FIleAttachment;
import com.mega.ws.repository.FileAttachmentRepository;
import com.mega.ws.service.dto.FileAttachmentDTO;

@Service
public class FileAttachmentService extends BaseService {

	@Autowired
	private FileAttachmentRepository fileAttachmentRepository;

	@SuppressWarnings("rawtypes")
	public List<FileAttachmentDTO> listFile(String username, String date) {
		Map<String, String> map = preparedDateSchedule(date);
		List<FileAttachmentDTO> listDto = new ArrayList<FileAttachmentDTO>();
		try {
			List<FIleAttachment> listModel = fileAttachmentRepository.listFileFromChange(username,
					new Timestamp(format.parse(map.get("startDate")).getTime()), new Timestamp(format.parse(map.get("endDate")).getTime()));
			
			for (Iterator iterator = listModel.iterator(); iterator.hasNext();) {
				FileAttachmentDTO dto = new FileAttachmentDTO();
				FIleAttachment file = (FIleAttachment) iterator.next();
				dto.setCreatedBy(file.getCreatedBy());
				dto.setCreatedDt(String.valueOf(file.getCreatedDt()));
				dto.setDirectory(file.getDirectory());
				dto.setFileName(file.getFileName());
				dto.setFileSize(file.getFileSize());
				dto.setFileType(file.getFileType());
				dto.setStatus(file.getStatus());
				listDto.add(dto);
			}
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return listDto;
	}
	
	public String findFileType(String filename, String status) {
		return fileAttachmentRepository.findFileType(filename, status);
	}

}
