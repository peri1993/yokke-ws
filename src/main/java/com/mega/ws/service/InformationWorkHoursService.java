package com.mega.ws.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mega.ws.model.OfficeHours;
import com.mega.ws.model.UserOfficeHours;
import com.mega.ws.repository.OfficeHoursRepository;
import com.mega.ws.repository.UserOfficeHoursRepository;
import com.mega.ws.request.ChangeWorkHoursRequest;
import com.mega.ws.response.OfficeHoursResponse;
import com.mega.ws.response.UserOfficeHoursResponse;

@Service
public class InformationWorkHoursService extends BaseService{
	
	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(InformationWorkHoursService.class);
	
	@Autowired
	private UserOfficeHoursRepository userOfficeHoursRepository;
	
	@Autowired
	private OfficeHoursRepository officeHoursRepository;
	
	public UserOfficeHoursResponse detailWorkHours(String username){
		try {
			List<UserOfficeHours> list = userOfficeHoursRepository.findByEmpId(username);
			UserOfficeHoursResponse response = setModel(list);
			return response;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
	}
	
	private UserOfficeHoursResponse setModel(List<UserOfficeHours> list) {
		UserOfficeHoursResponse response = new UserOfficeHoursResponse();
		if (!list.isEmpty()) {
			for (int i = 0; i < list.size(); i++) {
				UserOfficeHours model = list.get(i);
				response.setEmpId(model.getEmpId());
				response.setFullname(model.getFullname());
				
				OfficeHours hours = model.getOfficeHours();
				OfficeHoursResponse hoursOffice = new OfficeHoursResponse();
				hoursOffice.setCreatedBy(hours.getCreatedBy());
				hoursOffice.setCreatedDt(formateTimestamp(hours.getCreatedDt()));
				hoursOffice.setUpdatedBy(hours.getUpdatedBy());
				hoursOffice.setUpdatedDt(formateTimestamp(hours.getUpdatedDt()));
				hoursOffice.setName(hours.getName());
				hoursOffice.setStartTime(hours.getStartTime());
				hoursOffice.setEndtTime(hours.getEndtTime());
				hoursOffice.setLocationExtend(hours.getLocationExtend());
				hoursOffice.setReason(hours.getReason());
				
				response.setOfficeHours(hoursOffice);
			}
		}
		
		return response;
	}
	
	public List<OfficeHoursResponse> listWorkHours(){
		List<OfficeHoursResponse> listResponse = new ArrayList<OfficeHoursResponse>();
		try {
			List<OfficeHours> list = officeHoursRepository.findAll();
			listResponse = setModelWorkHours(list);
			return listResponse;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
	}
	
	private List<OfficeHoursResponse> setModelWorkHours(List<OfficeHours> list){
		List<OfficeHoursResponse> listResponse = new ArrayList<OfficeHoursResponse>();
		for (int i = 0; i < list.size(); i++) {
			OfficeHours hours = list.get(i);
			
			OfficeHoursResponse response = new OfficeHoursResponse();
			response.setId(String.valueOf(hours.getHours_id()));
			response.setCreatedBy(hours.getCreatedBy());
			response.setCreatedDt(formateTimestamp(hours.getCreatedDt()));
			response.setUpdatedBy(hours.getUpdatedBy());
			response.setUpdatedDt(formateTimestamp(hours.getUpdatedDt()));
			response.setName(hours.getName());
			response.setStartTime(hours.getStartTime());
			response.setEndtTime(hours.getEndtTime());
			response.setLocationExtend(hours.getLocationExtend());
			response.setDesc(hours.getStartTime().concat(":").concat(hours.getEndtTime()).concat(" ").concat(hours.getLocationExtend()));
			response.setReason(hours.getReason());
			listResponse.add(response);
		}
		return listResponse;
	}
	
	public OfficeHours findOfficeHoursById(String hoursId) {
		return officeHoursRepository.findOne(Long.valueOf(hoursId));
	}
	
	public void udpateWorkHours(ChangeWorkHoursRequest request) {
		UserOfficeHours user = mappingObject(request);
		userOfficeHoursRepository.save(user);
	}
	
	private UserOfficeHours mappingObject(ChangeWorkHoursRequest request) {
		List<UserOfficeHours> listUser = userOfficeHoursRepository.findByEmpId(request.getEmployee_id());
		try {
			UserOfficeHours user = listUser.get(0);
			user.setOfficeHours(officeHoursRepository.findOne(new Long(request.getHours_id())));
			return user;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
	}

}
