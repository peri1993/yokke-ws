package com.mega.ws.service;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.mega.ws.model.InstancesLogs;
import com.mega.ws.repository.InstancesLogsRepository;
import com.mega.ws.service.dto.InstancesLogsDTO;
import com.mega.ws.service.dto.ReAssignerDTO;
import com.mega.ws.service.mapper.LogsMapper;

@Service
public class InstancesLogsService extends BaseService{

	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(InstancesLogsService.class);
	private List<InstancesLogsDTO> listEmptyDto = new ArrayList<InstancesLogsDTO>();

	public InstancesLogsService(InstancesLogsRepository repository, LogsMapper instancesLogsMapper) {
		this.repository = repository;
		this.instancesLogsMapper = instancesLogsMapper;
	}

	public void save(InstancesLogs model) {
		repository.save(model);
	}

	public Page<InstancesLogsDTO> listInbox(String username, List<Long> taskId, String paramRequesterId,
			String paramRequesterName, String paramBusinessProcess, String paramAbsencesDesc, String paramTaskId, String paramDateKey,
			String paramStartDate, String paramEndDate, Pageable pageable) throws ParseException {
		if ("".equals(paramStartDate) && "".equals(paramTaskId) && "".equals(paramDateKey)) {
			return repository.findByUsername(username, paramRequesterId, paramRequesterName, paramBusinessProcess,
					paramAbsencesDesc, pageable).map(instancesLogsMapper::entityToDTO);
		} else if (!"".equals(paramTaskId)) {

			InstancesLogs model = taskLogsService.getInstancesMaxIDByTask(new Long(paramTaskId));
			return repository.findByTaskId(username, model.getInstancesId(), pageable)
					.map(instancesLogsMapper::entityToDTO);
		} else {
			String startDate = paramStartDate;
			String endDate = paramEndDate;
			if("startDt".equals(paramDateKey)) {
				return repository.findByStartDate(username, startDate, endDate, pageable).map(instancesLogsMapper::entityToDTO);
			}else {
				return repository.findByEndDate(username, startDate, endDate, pageable).map(instancesLogsMapper::entityToDTO);
			}
		}
	}
	
	public Page<InstancesLogsDTO> listOutbox(String username, List<Long> taskId, String paramRequesterId,
			String paramRequesterName, String paramBusinessProcess, String paramAbsencesType, String paramTaskId, String paramDateKey,
			String paramStartDate, String paramEndDate, Pageable pageable) throws ParseException {
		if ("".equals(paramStartDate) && "".equals(paramTaskId) && "".equals(paramDateKey)) {
			return repository.findOutboxByUsername(username, paramRequesterId, paramRequesterName, paramBusinessProcess,
					paramAbsencesType, pageable).map(instancesLogsMapper::entityToDTO);
		} else if (!"".equals(paramTaskId)) {
			InstancesLogs model = taskLogsService.getInstancesMaxIDByTask(new Long(paramTaskId));
			return repository.findOuboxByTaskId(username, model.getInstancesId(), pageable)
					.map(instancesLogsMapper::entityToDTO);
		} else {
			String startDate = paramStartDate;
			String endDate = paramEndDate;
			if("startDt".equals(paramDateKey)) {
				return repository.findOutboxByStartDate(username, startDate, endDate, pageable).map(instancesLogsMapper::entityToDTO);
			}else {
				return repository.findOutboxByEndDate(username, startDate, endDate, pageable).map(instancesLogsMapper::entityToDTO);
			}
		}
	}

	public Page<ReAssignerDTO> listAssigner(String paramInstancesId, String paramBusinessProcess,String paramAbsencesType, String paramAbsencesDesc,
			String paramRequesterId, String paramRequesterName, String paramAssigner, String paramAssignerName, Pageable pageable) {
		try {
				if(!"".equals(paramInstancesId)) {
					return repository.findByUsernameForAssignFilterWithInstancesId(new Long(paramInstancesId), pageable).map(instancesLogsMapper::entityToDTOForAssigner);
				} else if(!"".equals(paramAssigner)){
					return repository.findByUsernameForAssignFilterWithAssigner(paramAssigner, pageable).map(instancesLogsMapper::entityToDTOForAssigner);
				} else if(!"".equals(paramRequesterId)){
					return repository.findByUsernameForAssignFilterWithRequesterId(paramRequesterId, pageable).map(instancesLogsMapper::entityToDTOForAssigner);
				} else if(!"".equals(paramAbsencesType)){
					return repository.findByUsernameForAssignFilterWithAbsencesType(paramAbsencesType, pageable).map(instancesLogsMapper::entityToDTOForAssigner);
				} else if(!"".equals(paramAssignerName)){
					return repository.findByUsernameForAssignFilterWithAssignerName(paramAssignerName, pageable).map(instancesLogsMapper::entityToDTOForAssigner);
				} else {
					return repository.findByUsernameForAssign(paramAssigner, paramRequesterName, paramBusinessProcess, paramAbsencesDesc, pageable).map(instancesLogsMapper::entityToDTOForAssigner);
				}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
	}

	public Page<InstancesLogsDTO> historyTask(String username, String businessProcess,
			String paramInstancesId, String paramAbsencesDesc, String paramRequesterId, String paramRequesterName, String paramDateKey, String paramStartDate, String paramEndDate, Pageable pageable) {
		try {
			if (!"".equals(paramInstancesId)) {
				return repository.historyTaskFilterWithInstancesId(new Long(paramInstancesId), businessProcess, pageable).map(instancesLogsMapper::entityToDTO);
			} else if(!"".equals(paramRequesterId)){
				return repository.historyTaskFilterWithRequesterId(businessProcess, paramRequesterId, pageable).map(instancesLogsMapper::entityToDTO);
			} else if(!"".equals(paramDateKey)){
				if ("startDt".equals(paramDateKey)) {
					return repository.historyTaskFilterWithLeaveStartDate(username, businessProcess, paramStartDate, paramEndDate, pageable).map(instancesLogsMapper::entityToDTO);
				} else {
					return repository.historyTaskFilterWithLeaveEndDate(username, businessProcess, paramStartDate, paramEndDate, pageable).map(instancesLogsMapper::entityToDTO);
				}
			} else {
				return repository.historyTask(username, businessProcess, paramAbsencesDesc, paramRequesterName, pageable).map(instancesLogsMapper::entityToDTO);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		Page<InstancesLogsDTO> pageDTO = new PageImpl<InstancesLogsDTO>(listEmptyDto, pageable, 0);
		return pageDTO;
	}
	
	public Page<InstancesLogsDTO> historyTaskForMobile(String username, String businessProcess,
			String paramInstancesId, String paramAbsencesDesc, String paramRequesterId, String paramRequesterName, String paramDateKey, String paramStartDate, String paramEndDate, Pageable pageable) {
		try {
			if (!"".equals(paramInstancesId)) {
				return repository.historyTaskFilterWithInstancesIdForMobile(new Long(paramInstancesId), businessProcess, pageable).map(instancesLogsMapper::entityToDTO);
			} else if(!"".equals(paramRequesterId)){
				return repository.historyTaskFilterWithRequesterIdForMobile(businessProcess, paramRequesterId, pageable).map(instancesLogsMapper::entityToDTO);
			} else if(!"".equals(paramDateKey)){
				if ("startDt".equals(paramDateKey)) {
					return repository.historyTaskFilterWithLeaveStartDateForMobile(username, businessProcess, paramStartDate, paramEndDate, pageable).map(instancesLogsMapper::entityToDTO);
				} else {
					return repository.historyTaskFilterWithLeaveEndDateForMobile(username, businessProcess, paramStartDate, paramEndDate, pageable).map(instancesLogsMapper::entityToDTO);
				}
			} else {
				return repository.historyTaskForMobile(username, businessProcess, paramAbsencesDesc, paramRequesterName, pageable).map(instancesLogsMapper::entityToDTO);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		Page<InstancesLogsDTO> pageDTO = new PageImpl<InstancesLogsDTO>(listEmptyDto, pageable, 0);
		return pageDTO;
	}

	public Page<InstancesLogsDTO> historyTaskAll(String username, String paramAbsencesDesc, String paramRequesterId, String paramRequesterName,
			Pageable pageable) {
		return repository.historyTaskAll(username, paramAbsencesDesc, paramRequesterId, paramRequesterName, pageable).map(instancesLogsMapper::entityToDTO);
				
	}

	public void updateApprove(String assigner, Timestamp updateDt, Long taskId, Long instancesId) {
		try {
			InstancesLogs model = findByInstancesId(instancesId);
			model.setAssigner(assigner);
			model.setUpdatedDt(updateDt);
			// model.setTaskId(taskId);
			save(model);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public InstancesLogs findByInstancesId(Long instancesId) {
		return repository.findByInstancesId(instancesId);
	}

	public void deleteInstancesLogs(Long instancesId) {
		repository.delete(instancesId);
	}

	public InstancesLogs getInstancesLogById(Long instancesId) {
		return repository.getInstancesLogsByInstancesId(instancesId);
	}

	public String getRequesterId(Long instancesId) {
		return repository.getRequesterIdByInstancesId(instancesId);
	}
	
	public Long countInstancesLogByAssigner(String username) {
		Long count = repository.countInstancesLogsByAssigner(username);
		if (count != null && count < 0) {
			return new Long(0);
		}
		return count;
	}
	
	public Long countInstancesLogByReqIdOutbox(String username) {
		Long count = repository.countInstancesLogsByReqIdOutbox(username);
		if (count != null && count < 0) {
			return new Long(0);
		}
		return count;
	}
	
	public Long countInstancesLogByRequesterId(String username, String businessProcess) {
		Long count = repository.countInstancesLogsByRequesterId(username, businessProcess);
		if (count != null && count < 0) {
			return new Long(0);
		}
		return count;
	}
	
	public Long countInstancesLogsByReqIdHistory(String username) {
		Long count = repository.countInstancesLogsByReqIdHistory(username);
		if(count != null && count < 0) {
			return new Long(0);
		}
		return count;
	}
	
	public Long countInstancesLogByRequesterIdAll(String username) {
		Long count = repository.countInstancesLogsByRequesterIdAll(username);
		if (count != null && count < 0) {
			return new Long(0);
		}
		return count;
	}
	
	public List<InstancesLogs> listDataDate(String requesterId){
		return repository.getListInstanceLogsDataDate(requesterId);
	}
	
	public List<InstancesLogs> findDateDeduction(String requesterId, String startDt, String endDt){
		return repository.findDateDeduction(requesterId, startDt, endDt);
	}
	
	public List<InstancesLogs> checkAssigner(Long instancesId, String assigner){
		return repository.checkAssignerTask(instancesId, assigner);
	}

}
