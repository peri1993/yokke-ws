package com.mega.ws.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.mega.ws.model.InstancesLogs;
import com.mega.ws.model.Notifications;
import com.mega.ws.service.dto.NotificationDTO;

@Service
public class NotificationService  extends BaseService{
	
	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(NotificationService.class);
	
	public void saveNotif(Notifications notification) {
		notificationRepository.save(notification);
	}
	
	public Page<NotificationDTO> listNotifUser(String assigner, String paramInstancesId, String paramBusinessProcess, String paramSubject, String paramStatus, String paramRequesterId, String paramRequesterName, String paramStartDate, String paramEndDate, String paramDescription, Pageable pageable){
		if (paramStartDate != null && !"".equals(paramStartDate)) {
			Map<String, String> map = setFormateDateBetween(paramStartDate, paramEndDate);
			try {
				return notificationRepository.listNotifFilterByDate(assigner, new Timestamp(formated.parse(map.get("startDate")).getTime()), new Timestamp(formated.parse(map.get("endDate")).getTime()), pageable).map(instancesLogsMapper::entityToDTOForNotication);
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		} else if(!"".equals(paramSubject) || !"".equals(paramRequesterId) || !"".equals(paramRequesterName)){
			List<InstancesLogs> listInstancesLogs = new ArrayList<InstancesLogs>();
			if(!"".equals(paramSubject)) {
				listInstancesLogs = instancesLogsRepository.findListInstancesFilterBySubject(paramSubject);
			}
			else if(!"".equals(paramRequesterId)) {
				listInstancesLogs = instancesLogsRepository.findListInstancesFilterByRequesterID(paramRequesterId);
			}
			else {
				listInstancesLogs = instancesLogsRepository.findListInstancesFilterByRequesterName(paramRequesterName);
			}
			if(!listInstancesLogs.isEmpty()) {
				return notificationRepository.listNotifFilterByInstances(assigner, listInstancesLogs, pageable).map(instancesLogsMapper::entityToDTOForNotication);
			}
		} else if(!"".equals(paramInstancesId)){
			InstancesLogs instancesLog = instancesLogsRepository.findListInstancesFilterByNotifInstancesId(new Long(paramInstancesId));
			if (instancesLog != null) {
				return notificationRepository.findByinstancesId(instancesLog, assigner, pageable).map(instancesLogsMapper::entityToDTOForNotication);
			} 
		} else {
			return notificationRepository.listNotif(assigner, paramBusinessProcess, paramStatus, paramDescription, pageable).map(instancesLogsMapper::entityToDTOForNotication);
		}
		List<NotificationDTO> list = new ArrayList<NotificationDTO>();
		Page<NotificationDTO> model = new PageImpl<NotificationDTO>(list, pageable, 0);
		return model;
	}
	
	public List<Notifications> listNotifUser(String username){
		return notificationRepository.listNotifUser(username);
	}
	
	public void save(Notifications model) {
		notificationRepository.save(model);
	}
	
	public void update(String id, String assigner, String instancesId) {
		InstancesLogs instances = instancesService.getInstancesLogById(new Long(instancesId));
		if(instances != null) {
			if(id != null) {
				Notifications model = notificationRepository.findByInstancesIdAndAssigner(new Long(id), assigner, instances);
				if ("N".equals(model.getFlagRead())) {
					model.setFlagRead("Y");
					save(model);	
				}
			}else {
				List<Notifications> listModel = notificationRepository.listfindByInstancesIdAndAssigner(assigner, instances);
				for (int i = 0; i < listModel.size(); i++) {
					Notifications model = listModel.get(i);
					model.setFlagRead("Y");
					save(model);
				}
			}
		}
	}
	
	public Notifications findByInstancesIdDesc(InstancesLogs instanceslog) {
		return notificationRepository.findByInstancesIdDesc(instanceslog);
	}
}
