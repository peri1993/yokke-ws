package com.mega.ws.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mega.ws.config.JbpmConfig;
import com.mega.ws.constants.ModuleConstants;
import com.mega.ws.model.FIleAttachment;
import com.mega.ws.repository.UploadFileAttachmentReposity;
import com.mega.ws.request.UploadFileAttendanceRequest;
import com.mega.ws.request.UploadFileRequest;
import com.mega.ws.service.mapper.BaseMapper;

@Service
public class UploadFileService extends BaseMapper {

	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(UploadFileService.class);
	
	@Autowired
	private UploadFileAttachmentReposity repository;
	
	@Autowired
	JbpmConfig jbpmConfig;
	
	@Autowired
	BaseMapper baseMapper;
	
	public void save(List<UploadFileRequest> request) {
		try {
			for (int i = 0; i < request.size(); i++) {
				UploadFileRequest req = request.get(i);
				FIleAttachment model = new FIleAttachment();
				model.setDirectory(req.getDirectory());
				model.setFileName(req.getFileName());
				model.setFileDescription(req.getFileDescription());
				model.setFileSize(req.getFileSize());
				model.setFileType(req.getFileType());
				model.setCreatedDt(req.getCreatedDt());
				model.setCreatedBy(req.getCreatedBy());
				model.setModuleName(ModuleConstants.PERMIT_MODULE);
				repository.save(model);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	public void saveAtt(UploadFileAttendanceRequest request) {
		FIleAttachment validate = getFileByFilename(request.getFileName());
		FIleAttachment model = new FIleAttachment();
		try {
			if(validate == null) {
				model.setDirectory(request.getDirectory());
				model.setFileName(request.getFileName());
				model.setFileDescription(request.getFileDescription());
				model.setFileSize(request.getFileSize());
				model.setFileType(request.getFileType());
				model.setCreatedDt(request.getCreatedDt());
				model.setCreatedBy(request.getCreatedBy());
				model.setStatus("In");
				model.setModuleName(ModuleConstants.FILE_JADWAL_MODULE);
				repository.save(model);
			}else if(validate.getStatus().equals("In")) {
				model.setDirectory(request.getDirectory());
				model.setFileName(request.getFileName());
				model.setFileDescription(request.getFileDescription());
				model.setFileSize(request.getFileSize());
				model.setFileType(request.getFileType());
				model.setCreatedDt(request.getCreatedDt());
				model.setCreatedBy(request.getCreatedBy());
				model.setStatus("Out");
				model.setModuleName(ModuleConstants.FILE_JADWAL_MODULE);
				repository.save(model);
			}else{
				repository.delete(validate);
				model.setDirectory(request.getDirectory());
				model.setFileName(request.getFileName());
				model.setFileDescription(request.getFileDescription());
				model.setFileSize(request.getFileSize());
				model.setFileType(request.getFileType());
				model.setCreatedDt(request.getCreatedDt());
				model.setCreatedBy(request.getCreatedBy());
				model.setStatus("Out");
				model.setModuleName(ModuleConstants.FILE_JADWAL_MODULE);
				repository.save(model);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	public FIleAttachment getFileByFilename(String filename) {
		return repository.findByFileName(filename);
	}
}
