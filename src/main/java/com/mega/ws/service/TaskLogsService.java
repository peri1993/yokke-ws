package com.mega.ws.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.mega.ws.model.InstancesLogs;
import com.mega.ws.model.TaskLogs;
import com.mega.ws.service.dto.NotificationDTO;
import com.mega.ws.service.dto.TaskLogsDTO;

@Service
public class TaskLogsService extends BaseService{

	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(TaskLogsService.class);
	
	public void save(TaskLogs model) {
		taskLogRepository.save(model);
	}
	
	public TaskLogs findData(String createBy, InstancesLogs instancesId, Long taskId) {
		return taskLogRepository.findTaskLogs(createBy, instancesId, taskId);
	}
	
	public InstancesLogs getInstancesById(Long taskId) {
		return taskLogRepository.getInstancesByTaskId(taskId);
	}
	
	public InstancesLogs getInstancesMaxIDByTask(Long taskId) {
		return taskLogRepository.getInstancesMaxIDByTask(taskId);
	}
	
	public List<TaskLogs> detailTaskLogs(String instancesId){
		InstancesLogs instancesModel = instancesLogsRepository.getInstancesLogsByInstancesId(new Long(instancesId));
		return taskLogRepository.detailTaskLogs(instancesModel);
	}
	
	public Page<TaskLogsDTO> findHistoryAssigner(String username, String instanceId, String paramCreatedBy, String paramStartDate, String paramEndDate, String paramStages, 
			Pageable pageable) {
		
		if (paramStartDate != null && !"".equals(paramStartDate)) {
			Map<String, String> map = setFormateDateBetween(paramStartDate, paramEndDate);
			try {
				return taskLogRepository.getTaskLogsByIdDate(username, new Timestamp(formated.parse(map.get("startDate")).getTime()), new Timestamp(formated.parse(map.get("endDate")).getTime()), pageable).map(taskLogsMapper::entityTaskLogsDTO);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else if (!"".equals(instanceId) && null != instanceId) {
			return taskLogRepository.getTaskLogsByIdInstance(username, new Long(instanceId), pageable).map(taskLogsMapper::entityTaskLogsDTO);
		}else {
			return taskLogRepository.getTaskLogsById(username, paramCreatedBy, paramStages, pageable).map(taskLogsMapper::entityTaskLogsDTO);
		}
		List<TaskLogsDTO> list = new ArrayList<TaskLogsDTO>();
		Page<TaskLogsDTO> model = new PageImpl<TaskLogsDTO>(list, pageable, 0);
		return model;
	}
	
	public Page<TaskLogsDTO> findHistoryAll (String instanceId, String paramCreatedBy, String paramStages, 
			String paramUserStages, String paramStartDate, String paramEndDate, Pageable pageable){
		
		if (paramStartDate != null && !"".equals(paramStartDate)) {
			Map<String, String> map = setFormateDateBetween(paramStartDate, paramEndDate);
			try {
				return taskLogRepository.findAllCompleteTaskByDate(new Timestamp(formated.parse(map.get("startDate")).getTime()), new Timestamp(formated.parse(map.get("endDate")).getTime()), pageable).map(taskLogsMapper::entityTaskLogsDTO);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else if (!"".equals(instanceId) && null != instanceId) {
			return taskLogRepository.findAllCompleteTaskByInstanceId(new Long(instanceId), pageable).map(taskLogsMapper::entityTaskLogsDTO);
		}else {
			return taskLogRepository.findAllCompleteTask(paramCreatedBy, paramStages, paramUserStages, pageable).map(taskLogsMapper::entityTaskLogsDTO);
		}
		List<TaskLogsDTO> list = new ArrayList<TaskLogsDTO>();
		Page<TaskLogsDTO> model = new PageImpl<TaskLogsDTO>(list, pageable, 0);
		return model;
	}
	
}
