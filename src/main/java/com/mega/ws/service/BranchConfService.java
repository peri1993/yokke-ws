package com.mega.ws.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.mega.ws.identity.BranchConfID;
import com.mega.ws.model.BranchConf;
import com.mega.ws.request.BranchIdRequest;
import com.mega.ws.request.BranchRequest;
import com.mega.ws.service.dto.BranchConfDTO;

@Service
public class BranchConfService extends BaseService {

	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(BranchConfService.class);

	public Page<BranchConfDTO> listBranch(String empArea, String empSubArea, String address,
			String empSubAreaDescription, String npwp, String latitudeCode, String longitudeCode, String radius, Pageable pageable) {
		return branchConfRepository.listBranch(empArea, empSubArea, address, empSubAreaDescription, npwp, latitudeCode,
				longitudeCode, radius, pageable).map(branchConfMapper::entityBranchToDto);
	}

	public BranchConf findBranchByID(BranchIdRequest request) {
		BranchConfID id = new BranchConfID();
		try {
			id.setLandscape("100");
			id.setEmpArea(request.getEmpArea());
			id.setEmpSubArea(request.getEmpSubArea());
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return branchConfRepository.findBranchById(id);
	}

	public void updateBranch(BranchRequest request) {
		BranchConf branch = findBranchByID(request.getId());
		if (branch != null) {
			branch.setLatitudeCode(request.getLatitudeCode());
			branch.setLongitudeCode(request.getLongitudeCode());
			branch.setRadius(request.getRadius());
			branchConfRepository.save(branch);
		} 
	}
}
