package com.mega.ws.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.mega.ws.identity.AttendanceLogsID;
import com.mega.ws.identity.ScheduleAttendanceID;
import com.mega.ws.model.AttendanceLogs;
import com.mega.ws.model.ScheduleAttendance;
import com.mega.ws.request.AttendanceLogsIdRequest;
import com.mega.ws.request.AttendanceRequest;
import com.mega.ws.request.ScheduleAttendanceLogsIdRequest;
import com.mega.ws.request.ScheduleAttendanceLogsRequest;
import com.mega.ws.service.dto.AttendanceLogsDTO;
import com.mega.ws.service.dto.ScheduleAttendanceDTO;

@Service
public class AttendanceLogsService extends BaseService {

	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(AttendanceLogsService.class);

	public AttendanceLogs validateById(String empId, String date) {
		
		return attendanceLogsRepository.validationTime(empId, date);
	}

	public void createAttendances(AttendanceRequest request, AttendanceLogsIdRequest req) {
		AttendanceLogs attendance = new AttendanceLogs();
		AttendanceLogsID attId = new AttendanceLogsID();
		AttendanceLogs validte	= validateById(req.getEmpId(), req.getStartDate());
		try {
			if(validte == null) {
				attId.setStartDate(req.getStartDate());
				attId.setEndDate(req.getEndDate());
				attId.setEmpId(req.getEmpId());
				attId.setLandscape("100");
				attId.setFullName(req.getFullName());
				
				attendance.setId(attId);
				attendance.setTimeStart(parseTime(req.getTime()));
				attendance.setTimeEnd(null);
				attendance.setEmpArea(request.getEmpArea());
				attendance.setEmpSubArea(request.getEmpSubArea());
				attendance.setEmpSubAreaDescription(request.getEmpSubAreaDescription());
				attendance.setLatitudeCode(request.getLatitudeCode());
				attendance.setLongitudeCode(request.getLongitudeCode());
				attendance.setRadius(request.getRadius());
				attendanceLogsRepository.save(attendance);
			}else if(validte.getTimeEnd() == null) {
				attId.setStartDate(req.getStartDate());
				attId.setEndDate(req.getEndDate());
				attId.setEmpId(req.getEmpId());
				attId.setLandscape("100");
				attId.setFullName(req.getFullName());
				
				attendance = attendanceLogsRepository.findOne(attId);
				attendance.setTimeEnd(parseTime(req.getTime()));
				attendance.setLongitudeCodeOut(request.getLongitudeCode());
				attendance.setLatitudeCodeOut(request.getLatitudeCode());
				attendanceLogsRepository.save(attendance);
			}else{
				attId.setStartDate(req.getStartDate());
				attId.setEndDate(req.getEndDate());
				attId.setEmpId(req.getEmpId());
				attId.setLandscape("100");
				attId.setFullName(req.getFullName());
				
				attendance = attendanceLogsRepository.findOne(attId);
				attendance.setTimeEnd(parseTime(req.getTime()));
				attendance.setLongitudeCodeOut(request.getLongitudeCode());
				attendance.setLatitudeCodeOut(request.getLatitudeCode());
				attendanceLogsRepository.save(attendance);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public Page<ScheduleAttendanceDTO> listAttendance(String empId, Pageable pageable){
		return scheduleLogsRepository.listAttendance(empId, pageable).map(scheduleAttendanceMapper::entityScheduleToDTO);
	}
	
	public ScheduleAttendance validateByIdSchedule(String empId, String startDate, String endDate) {
		return scheduleLogsRepository.inputValidation(empId, startDate, endDate);
	}
	
	public List<ScheduleAttendance> flagAttendance(String username, String startDt) {
		return scheduleLogsRepository.flagAttendance(username, startDt);
	}
	
	public void deleteScheduleAttendance(ScheduleAttendanceLogsRequest request) {
		ScheduleAttendance schedule = scheduleLogsRepository.inputValidation(request.getId().getEmpId(), request.getId().getStartDate(), request.getId().getEndDate());
		if(schedule != null) {
			scheduleLogsRepository.delete(schedule);
		}
	}
	
	public void createScheduleAttendance(ScheduleAttendanceLogsRequest request, ScheduleAttendanceLogsIdRequest req) {
		ScheduleAttendance schedule = new ScheduleAttendance();
		ScheduleAttendanceID schId = new ScheduleAttendanceID();
		ScheduleAttendance validate = validateByIdSchedule(req.getEmpId(), req.getStartDate(), req.getEndDate());
		
		try {
			if(validate == null) {
				schId.setEmpId(req.getEmpId());
				schId.setStartDate(req.getStartDate());
				schId.setEndDate(req.getEndDate());
				
				schedule.setId(schId);
				schedule.setAddress(request.getAddress());
				schedule.setTimeStart(request.getTimeStart());
				schedule.setTimeEnd(request.getTimeEnd());
				scheduleLogsRepository.save(schedule);
			}
		} catch (Exception e) {
			e.printStackTrace();
			e.getMessage();
		}
	}
	
	public Page<AttendanceLogsDTO> listEmpAttendance(String username, Pageable pageable){
		return attendanceLogsRepository.listEmpAttendance(username, pageable).map(attendanceMapper::entityAttendanceDTO);
	}
	
	public Page<AttendanceLogsDTO> listHistoryAttendanceEmployee(String username, String startDt, String endDt, Pageable pageable){
		return attendanceLogsRepository.listHistoryAttendanceEmployee(username, startDt, endDt, pageable).map(attendanceMapper::entityAttendanceDTO);
	}
	
	public AttendanceLogs listEmpAttedanceByDate(String username, String startDate) {
		return attendanceLogsRepository.listEmpAttendanceByDate(username, startDate);
	}
}
