package com.mega.ws.service;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mega.ws.repository.AttendanceLogsRepository;
import com.mega.ws.repository.BranchConfRepository;
import com.mega.ws.repository.InstancesLogsRepository;
import com.mega.ws.repository.NotificationRepository;
import com.mega.ws.repository.RoleRepository;
import com.mega.ws.repository.RoleUserRepository;
import com.mega.ws.repository.ScheduleAttendanceLogsRepository;
import com.mega.ws.repository.TaskLogsRepository;
import com.mega.ws.service.mapper.AttendanceMapper;
import com.mega.ws.service.mapper.BranchConfMapper;
import com.mega.ws.service.mapper.LogsMapper;
import com.mega.ws.service.mapper.RoleUserMapper;
import com.mega.ws.service.mapper.ScheduleAttendanceMapper;
import com.mega.ws.service.mapper.TaskLogsMapper;

@Service
public class BaseService {
	
	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(BaseService.class);
	
	@Autowired
	public NotificationRepository notificationRepository;
	
	@Autowired
	public InstancesLogsService instancesService;
	
	@Autowired
	public LogsMapper instancesLogsMapper;
	
	@Autowired
	public BranchConfMapper branchConfMapper;
	
	@Autowired
	public ScheduleAttendanceMapper scheduleAttendanceMapper;
	
	@Autowired
	public TaskLogsMapper taskLogsMapper;
	
	@Autowired
	public AttendanceMapper attendanceMapper;
	
	@Autowired
	public RoleRepository roleRepository;

	@Autowired
	public RoleUserMapper roleUsermapper;

	@Autowired
	public RoleUserRepository userRepository;
	
	@Autowired
	public InstancesLogsRepository repository;

	@Autowired
	public TaskLogsService taskLogsService;
	
	@Autowired
	public TaskLogsRepository taskLogRepository;
	
	@Autowired
	public InstancesLogsRepository instancesLogsRepository;
	
	@Autowired
	public BranchConfRepository branchConfRepository;
	
	@Autowired
	public AttendanceLogsRepository attendanceLogsRepository;
	
	@Autowired
	public ScheduleAttendanceLogsRepository scheduleLogsRepository;
	
	public SimpleDateFormat formated = new SimpleDateFormat("dd-MM-YYYY HH:mm:ss");
	
	public SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	
	public SimpleDateFormat formatTime = new SimpleDateFormat("HH:mm:ss");
	
	public static final String STRING_EMPTY = "";
	
	public Map<String, String> setFormateDateBetween(String startDate, String endDate) {
		Map<String, String> map = new HashMap<String, String>();

		String start = startDate.concat(" ").concat("00:00:01");
		String end = endDate.concat(" ").concat("23:59:59");
		map.put("startDate", start);
		map.put("endDate", end);

		return map;
	}
	
	public String formateTimestamp(Timestamp timestamp) {
		if (timestamp == null) {
			return null;
		}else {
			Date date = new Date(timestamp.getTime());
			return format.format(date);
		}
	}
	
	public String parseTime(Timestamp timestamp) {
		if (timestamp == null) {
			return null;
		}else {
			Date date = new Date(timestamp.getTime());
			return formatTime.format(date);
		}
	}
	
	public String parseDateMappingValue1(String strDate) {
		String formateDateStr = STRING_EMPTY;
		try {
			String formateDay = String.valueOf(strDate.subSequence(6, 8));
			String formateMonth = String.valueOf(strDate.subSequence(4, 6));
			String formateYear = String.valueOf(strDate.subSequence(0, 4));

			if (strDate != null && strDate != STRING_EMPTY) {
				formateDateStr = formateDay.concat("-").concat(formateMonth).concat("-").concat(formateYear);
				
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		return formateDateStr;
	}
	
	public Map<String, String> preparedDateSchedule(String date){
		String varDate = parseDateMappingValue1(date);
		Map<String, String> map = setFormateDateBetween(varDate, varDate);
		
		return map;
	}
	
	public Map<String, String> setFormateDateBetweenSameDay(String startDate, String endDate) {
		Map<String, String> map = new HashMap<String, String>();

		String start = startDate;
		String end = endDate;
		map.put("startDate", start);
		map.put("endDate", end);

		return map;
	}

}
