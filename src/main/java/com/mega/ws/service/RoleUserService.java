package com.mega.ws.service;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.mega.ws.model.Role;
import com.mega.ws.model.RoleUser;
import com.mega.ws.service.dto.RoleDTO;
import com.mega.ws.service.dto.UserDTO;

@Service
public class RoleUserService extends BaseService{

	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(RoleUserService.class);

	public void saveRole(Role role) {
		roleRepository.save(role);
	}

	public Page<RoleDTO> getAllRole(String roleName, String createdBy, Pageable pageable) {
		return roleRepository.findById(roleName, createdBy, pageable).map(roleUsermapper::entityToDTORole);
	}

	public void delete(Long roleId) {
		roleRepository.delete(roleId);
	}

	public Role detailRole(Long roleId) {
		return roleRepository.findOne(roleId);
	}

	public Role findByRoleName(String roleName) {
		return roleRepository.findByRoleName(roleName);
	}

	public void updateRole(Role role) {
		roleRepository.save(role);
	}

	public Page<UserDTO> getAllUser(String username, String fullname, String createdBy, String roleName,
			String createdDtStart, String createdDtEnd, String updatedBy, String updatedDtStart, String updatedDtEnd,
			Pageable pageable) {
		if (!"".equals(createdDtStart) && !"".equals(createdDtEnd)) {
			Map<String, String> map = setFormateDateBetween(createdDtStart, createdDtEnd);
			try {
				return userRepository
						.findByCreatedDt(new Timestamp(formated.parse(map.get("startDate")).getTime()),
								new Timestamp(formated.parse(map.get("endDate")).getTime()), pageable)
						.map(roleUsermapper::entityUserToDTO);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		} else if (!"".equals(updatedDtStart) && !"".equals(updatedDtEnd)) {
			Map<String, String> map = setFormateDateBetween(updatedDtStart, updatedDtEnd);
			try {
				return userRepository
						.findByUpdatedDt(new Timestamp(formated.parse(map.get("startDate")).getTime()),
								new Timestamp(formated.parse(map.get("endDate")).getTime()), pageable)
						.map(roleUsermapper::entityUserToDTO);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		} else if (!"".equals(roleName)) {

			return userRepository.listByRoleName(roleName, pageable).map(roleUsermapper::entityUserToDTO);

		} else {
			return userRepository.findById(username, fullname, createdBy, updatedBy, pageable)
					.map(roleUsermapper::entityUserToDTO);
		}
		return null;
	}

	public RoleUser findUserByUsername(String username) {
		return userRepository.findByUsername(username);
	}

	public RoleUser findUserRoleByID(Long userRoleId) {
		return userRepository.findById(userRoleId);
	}

	public void saveUserRole(RoleUser user) {
		try {
			userRepository.save(user);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public void deleteUserRole(Long userRoleId) {
		userRepository.delete(userRoleId);
	}
}
