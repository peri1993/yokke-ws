package com.mega.ws.service.dto;

public class BranchConfDTO {

	private String empArea;
	private String empSubArea;
	private String landscape;
	private String empSubAreaDescription;
	private String address;
	private String npwp;
	private String latitudeCode;
	private String longitudeCode;
	private String radius;
	
	public String getRadius() {
		return radius;
	}

	public void setRadius(String radius) {
		this.radius = radius;
	}

	public String getEmpArea() {
		return empArea;
	}

	public void setEmpArea(String empArea) {
		this.empArea = empArea;
	}

	public String getEmpSubArea() {
		return empSubArea;
	}

	public void setEmpSubArea(String empSubArea) {
		this.empSubArea = empSubArea;
	}

	public String getLandscape() {
		return landscape;
	}

	public void setLandscape(String landscape) {
		this.landscape = landscape;
	}

	public String getEmpSubAreaDescription() {
		return empSubAreaDescription;
	}

	public void setEmpSubAreaDescription(String empSubAreaDescription) {
		this.empSubAreaDescription = empSubAreaDescription;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getNpwp() {
		return npwp;
	}

	public void setNpwp(String npwp) {
		this.npwp = npwp;
	}

	public String getLatitudeCode() {
		return latitudeCode;
	}

	public void setLatitudeCode(String latitudeCode) {
		this.latitudeCode = latitudeCode;
	}

	public String getLongitudeCode() {
		return longitudeCode;
	}

	public void setLongitudeCode(String longitudeCode) {
		this.longitudeCode = longitudeCode;
	}

}
