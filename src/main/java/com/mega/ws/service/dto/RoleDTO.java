package com.mega.ws.service.dto;

import java.util.List;

import com.mega.ws.model.Menu;

public class RoleDTO {

	private Long role_id;
	private String roleName;
	private String createdBy;
	private String createdDt;
	private String updatedBy;
	private String updatedDt;
	private List<Menu> listMenu;
	private String menuAccess;

	public String getMenuAccess() {
		return menuAccess;
	}

	public void setMenuAccess(String menuAccess) {
		this.menuAccess = menuAccess;
	}

	public Long getRole_id() {
		return role_id;
	}

	public void setRole_id(Long role_id) {
		this.role_id = role_id;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(String createdDt) {
		this.createdDt = createdDt;
	}

	public List<Menu> getListMenu() {
		return listMenu;
	}

	public void setListMenu(List<Menu> listMenu) {
		this.listMenu = listMenu;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(String updatedDt) {
		this.updatedDt = updatedDt;
	}

}
