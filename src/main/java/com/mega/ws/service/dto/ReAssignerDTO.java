package com.mega.ws.service.dto;

import java.io.Serializable;

@SuppressWarnings("serial")
public class ReAssignerDTO implements Serializable {

	private Long instancesId;
	private Long taskId;
	private String assigner;
	private String assignerName;
	private String requesterId;
	private String requesterName;
	private String businessProcess;
	private String subject;
	private String createdDt;
	private String flagTaskComplete;
	private String stages;
	private String updatedDt;
	private String absencesType;
	private String absencesDesc;
	private String approvalDs1;
	private String nameApprovalDs1;
	private String approvalDs2;
	private String nameApprovalDs2;
	private String startDate;
	private String endDate;
	private String approvalStagesName;
	
	
	
	public String getApprovalStagesName() {
		return approvalStagesName;
	}

	public void setApprovalStagesName(String approvalStagesName) {
		this.approvalStagesName = approvalStagesName;
	}

	public String getAssignerName() {
		return assignerName;
	}

	public void setAssignerName(String assignerName) {
		this.assignerName = assignerName;
	}

	public Long getInstancesId() {
		return instancesId;
	}

	public void setInstancesId(Long instancesId) {
		this.instancesId = instancesId;
	}

	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getAssigner() {
		return assigner;
	}

	public void setAssigner(String assigner) {
		this.assigner = assigner;
	}

	public String getRequesterId() {
		return requesterId;
	}

	public void setRequesterId(String requesterId) {
		this.requesterId = requesterId;
	}

	public String getRequesterName() {
		return requesterName;
	}

	public void setRequesterName(String requesterName) {
		this.requesterName = requesterName;
	}

	public String getBusinessProcess() {
		return businessProcess;
	}

	public void setBusinessProcess(String businessProcess) {
		this.businessProcess = businessProcess;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(String createdDt) {
		this.createdDt = createdDt;
	}

	public String getFlagTaskComplete() {
		return flagTaskComplete;
	}

	public void setFlagTaskComplete(String flagTaskComplete) {
		this.flagTaskComplete = flagTaskComplete;
	}

	public String getStages() {
		return stages;
	}

	public void setStages(String stages) {
		this.stages = stages;
	}

	public String getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(String updatedDt) {
		this.updatedDt = updatedDt;
	}

	public String getAbsencesType() {
		return absencesType;
	}

	public void setAbsencesType(String absencesType) {
		this.absencesType = absencesType;
	}

	public String getAbsencesDesc() {
		return absencesDesc;
	}

	public void setAbsencesDesc(String absencesDesc) {
		this.absencesDesc = absencesDesc;
	}

	public String getApprovalDs1() {
		return approvalDs1;
	}

	public void setApprovalDs1(String approvalDs1) {
		this.approvalDs1 = approvalDs1;
	}

	public String getNameApprovalDs1() {
		return nameApprovalDs1;
	}

	public void setNameApprovalDs1(String nameApprovalDs1) {
		this.nameApprovalDs1 = nameApprovalDs1;
	}

	public String getApprovalDs2() {
		return approvalDs2;
	}

	public void setApprovalDs2(String approvalDs2) {
		this.approvalDs2 = approvalDs2;
	}

	public String getNameApprovalDs2() {
		return nameApprovalDs2;
	}

	public void setNameApprovalDs2(String nameApprovalDs2) {
		this.nameApprovalDs2 = nameApprovalDs2;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

}
