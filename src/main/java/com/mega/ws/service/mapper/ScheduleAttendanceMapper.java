package com.mega.ws.service.mapper;

import org.springframework.stereotype.Service;

import com.mega.ws.model.ScheduleAttendance;
import com.mega.ws.service.dto.ScheduleAttendanceDTO;

@Service
public class ScheduleAttendanceMapper extends BaseMapper{
	
	public ScheduleAttendanceDTO entityScheduleToDTO(ScheduleAttendance model) {
		ScheduleAttendanceDTO dto = new ScheduleAttendanceDTO();
		try {
			dto.setEmpId(model.getId().getEmpId());
			dto.setStartDate(parseDate(model.getId().getStartDate()));
			dto.setEndDate(parseDate(model.getId().getEndDate()));
			dto.setAddress(model.getAddress());
			dto.setTimeStart(model.getTimeStart());
			dto.setTimeEnd(model.getTimeEnd());
		} catch (Exception e) {
			// TODO: handle exception
		}
		return dto;
	}
}
