package com.mega.ws.service.mapper;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mega.ws.model.Menu;
import com.mega.ws.model.Role;
import com.mega.ws.model.RoleUser;
import com.mega.ws.repository.MenuRepository;
import com.mega.ws.service.dto.RoleDTO;
import com.mega.ws.service.dto.UserDTO;

@Service
public class RoleUserMapper extends BaseMapper{
	
	@Autowired
	private MenuRepository menuReposity;
	
	public RoleDTO entityToDTORole(Role role) {
		RoleDTO dto = new RoleDTO();
		try {
			List<Menu> listMenu = menuReposity.findAllMenuByRole(role);
			dto.setCreatedBy(role.getCreatedBy());
			dto.setCreatedDt(formateTimestamp(role.getCreatedDt()));
			dto.setUpdatedBy(role.getUpdatedBy());
			dto.setUpdatedDt(formateTimestamp(role.getUpdatedDt()));
			dto.setRole_id(role.getRole_id());
			dto.setRoleName(role.getRoleName());
			dto.setListMenu(listMenu);
			dto.setMenuAccess(setMenuAccess(listMenu));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return dto;
	}
	
	public UserDTO entityUserToDTO(RoleUser user) {
		UserDTO dto = new UserDTO();
		try {
			dto.setId(user.getId());
			dto.setUsername(user.getUsername());
			dto.setFullname(user.getFullname());
			dto.setCreatedBy(user.getCreatedBy());
			dto.setCreatedDt(formateTimestamp(user.getCreatedDt()));
			dto.setUpdatedBy(user.getUpdatedBy());
			dto.setUpdatedDt(formateTimestamp(user.getUpdatedDt()));
			dto.setRole(entityToDTORole(user.getRole()));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		return dto;
	}
	
	private String setMenuAccess(List<Menu> listMenu) {
		String menuAccess = null;
		for (int i = 0; i < listMenu.size(); i++) {
			Menu menu = listMenu.get(i);
			if (menuAccess == null) {
				menuAccess = menu.getName();
			}else {
				menuAccess = menuAccess.concat(", ").concat(menu.getName()); 
			}
		}
		return menuAccess;
	}

}
