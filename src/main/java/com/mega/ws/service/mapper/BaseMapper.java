package com.mega.ws.service.mapper;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.mega.ws.jbpm.MappingURL;
import com.mega.ws.response.JBPM_MappingResponse;
import com.mega.ws.util.HeaderSettings;

@Service
public class BaseMapper {
	SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
	SimpleDateFormat format3 = new SimpleDateFormat("yyyyMMdd");
	SimpleDateFormat format4 = new SimpleDateFormat("dd-MM-yyyy");
	private static String STRING_EMPTY = "";
	
	@Autowired
	public MappingURL mappingURL;

	@Autowired
	public HeaderSettings headerSettings;
	
	public RestTemplate restTemplate = new RestTemplate();
	
	public String formateTimestamp(Timestamp timestamp) {
		if (timestamp == null) {
			return null;
		}else {
			Date date = new Date(timestamp.getTime());
			return format3.format(date);
		}
	}
	
	public String formateTimestamp2(Timestamp timestamp) {
		if (timestamp == null) {
			return null;
		}else {
			Date date = new Date(timestamp.getTime());
			return format4.format(date);
		}
	}
	
	public String formateTimestamp(String timestamp) {
		String dates = null;
		try {
			if(timestamp != null && !"null".equals(timestamp)) {
				Date date = format.parse(timestamp);
				dates = format.format(date);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		return dates;
	}
	
	public String parseDate(String strDate) {
		String formateDateStr = STRING_EMPTY;
		try {
			if (strDate != null && strDate != STRING_EMPTY) {
				String formateYear = String.valueOf(strDate.subSequence(0, 4));
				String formateMonth = String.valueOf(strDate.subSequence(4, 6));
				String formateDate = String.valueOf(strDate.subSequence(6, 8));
				formateDateStr = formateDate.concat("-").concat(formateMonth).concat("-").concat(formateYear);
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		return formateDateStr;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public String findName(String username) {
		String fullName = null;

		try {
			HttpEntity<Object> entity = new HttpEntity<>(headerSettings.setupHeaderAccept());
			JBPM_MappingResponse task = mappingURL.findName(username);

			Object response = restTemplate.exchange(task.getUrl(), task.getMethod(), entity, Object.class);

			ResponseEntity<HashMap> responseEntity = (ResponseEntity<HashMap>) response;
			fullName = (String) responseEntity.getBody().get("fullname");

		} catch (Exception e) {
			e.printStackTrace();
		}

		return fullName;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Integer findQuotaTaken(String startDate, String endDate) {
		Integer quota = null;
		
		try {
			HttpEntity<Object> entity = new HttpEntity<Object>(headerSettings.setupHeaderAccept());
			JBPM_MappingResponse task = mappingURL.getHolidayBetween(startDate, endDate);
			
			Object response = restTemplate.exchange(task.getUrl(), task.getMethod(), entity, Object.class);
			
			ResponseEntity<HashMap> responseEntity = (ResponseEntity<HashMap>) response;
			quota = (Integer) responseEntity.getBody().get("total");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return quota;
	}

}
