package com.mega.ws.service.mapper;

import org.springframework.stereotype.Service;

import com.mega.ws.model.BranchConf;
import com.mega.ws.service.dto.BranchConfDTO;

@Service
public class BranchConfMapper extends BaseMapper {

	
	public BranchConfDTO entityBranchToDto(BranchConf model) {
		BranchConfDTO dto = new BranchConfDTO();
		try {
			dto.setAddress(model.getAddress());
			dto.setEmpArea(model.getId().getEmpArea());
			dto.setEmpSubArea(model.getId().getEmpSubArea());
			dto.setLandscape(model.getId().getLandscape());
			dto.setEmpSubAreaDescription(model.getEmpSubAreaDescription());
			dto.setNpwp(model.getNpwp());
			dto.setLatitudeCode(model.getLatitudeCode());
			dto.setLongitudeCode(model.getLongitudeCode());
			dto.setRadius(model.getRadius());
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return dto;
	}
}
