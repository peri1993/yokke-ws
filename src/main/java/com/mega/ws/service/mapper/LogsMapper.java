package com.mega.ws.service.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mega.ws.constants.PropertiesConstant;
import com.mega.ws.model.InstancesLogs;
import com.mega.ws.model.Notifications;
import com.mega.ws.repository.TaskLogsRepository;
import com.mega.ws.service.dto.InstancesLogsDTO;
import com.mega.ws.service.dto.NotificationDTO;
import com.mega.ws.service.dto.ReAssignerDTO;

@Service
public class LogsMapper extends BaseMapper{
	
	@Autowired
	private TaskLogsRepository taskLogReposity;
	
	public InstancesLogsDTO entityToDTO(InstancesLogs model) {
		InstancesLogsDTO result = new InstancesLogsDTO();
		try {
			result.setInstancesId(model.getInstancesId());
			result.setTaskId(taskLogReposity.getTaskId(model));
			result.setBusinessProcess(model.getBusinessProcess());
			result.setAbsencesType(model.getAbsencesType());
			result.setAbsencesDesc(model.getAbsencesDesc());
			result.setRequesterId(model.getRequesterId());
			result.setRequesterName(model.getRequesterName());
			result.setStartDate(parseDate(model.getStartDate()));
			result.setEndDate(parseDate(model.getEndDate()));
			result.setFlagTaskComplete(taskLogReposity.getFlagTask(model));
			result.setCreatedDt(formateTimestamp2(model.getCreatedDt()));
			result.setQuotaTaken(findQuotaTaken(model.getStartDate(), model.getEndDate()));
			
			result.setApprovalStagesName(setNameStages(model.getAssigner(), model.getApprovalDs1(), model.getApprovalDs2()));
			
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		return result;
	} 
	
	
	public ReAssignerDTO entityToDTOForAssigner(InstancesLogs model) {
		ReAssignerDTO result = new ReAssignerDTO();
		try {
			result.setInstancesId(model.getInstancesId());
			result.setTaskId(taskLogReposity.getTaskId(model));
			result.setBusinessProcess(model.getBusinessProcess());
			result.setAbsencesType(model.getAbsencesType());
			result.setAbsencesDesc(model.getAbsencesDesc());
			result.setRequesterId(model.getRequesterId());
			result.setRequesterName(model.getRequesterName());
			result.setAssigner(model.getAssigner());
			result.setAssignerName(findName(result.getAssigner()));
			result.setApprovalDs1(model.getApprovalDs1());
			result.setNameApprovalDs1(model.getNameApprovalDs1());
			result.setApprovalDs2(model.getApprovalDs2());
			result.setNameApprovalDs2(model.getNameApprovalDs2());
			result.setStartDate(parseDate(model.getStartDate()));
			result.setEndDate(parseDate(model.getEndDate()));
			result.setCreatedDt(formateTimestamp(String.valueOf(model.getCreatedDt())));
			result.setSubject(model.getSubject());
			result.setStages(taskLogReposity.lastStatus(model.getRequesterId(), model));
			result.setFlagTaskComplete(taskLogReposity.getFlagTask(model));
			result.setUpdatedDt(formateTimestamp(String.valueOf(model.getUpdatedDt())));
			result.setApprovalStagesName(setNameStages(model.getAssigner(), model.getApprovalDs1(), model.getApprovalDs2()));
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		return result;
	} 
	
	
	private String setNameStages(String assigner, String approvalDs1, String approvalDs2) {
		String name = null;
		
		if (null != assigner) {
			if(assigner.equals(approvalDs1)) {
				name = PropertiesConstant.TASK_STAGES_1.concat(" - ").concat(findName(approvalDs1)).concat(" - ").concat(approvalDs1);
			}else if(assigner.equals(approvalDs1) && assigner.equals(approvalDs2)) {
				name = PropertiesConstant.TASK_ONE_APPROVAL;
			}else{
				name = PropertiesConstant.TASK_STAGES_2.concat(" - ").concat(findName(approvalDs2).concat(" - ").concat(approvalDs2));
			}
		}
		return name;
	}
	
	public NotificationDTO entityToDTOForNotication(Notifications notif) {
		NotificationDTO result = new NotificationDTO();
		try {
			result.setId(String.valueOf(notif.getId()));
			result.setInstancesId(String.valueOf(notif.getInstancesId().getInstancesId()));
			result.setBusinessProcess(notif.getBusinesProcess());
			result.setSubject(notif.getInstancesId().getSubject());
			result.setStatus(notif.getStages());
			result.setRequester(notif.getInstancesId().getRequesterId()+" - "+notif.getInstancesId().getRequesterName());
			result.setCreatedDt(formateTimestamp(notif.getCreatedDt()));
			result.setDescription(notif.getDescription());
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		return result;
	}


}
