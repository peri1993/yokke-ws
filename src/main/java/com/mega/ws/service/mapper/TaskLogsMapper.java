package com.mega.ws.service.mapper;

import org.springframework.stereotype.Service;

import com.mega.ws.model.TaskLogs;
import com.mega.ws.service.dto.TaskLogsDTO;

@Service
public class TaskLogsMapper extends BaseMapper {
	
	public TaskLogsDTO entityTaskLogsDTO(TaskLogs model) {
		TaskLogsDTO dto = new TaskLogsDTO();
		
		try {
			dto.setTaskId(String.valueOf(model.getTaskId()));
			dto.setInstancesId(String.valueOf(model.getInstanceId().getInstancesId()));
			dto.setCreatedBy(model.getCreatedBy().concat(" - ").concat(findName(model.getCreatedBy())));
			dto.setCreatedDt(formateTimestamp2(model.getCreatedDt()));
			dto.setUserStages(model.getUserStages().concat(" - ").concat(findName(model.getUserStages())));
			dto.setStages(model.getStages());
			dto.setFlagTaskComplete(model.getFlagTaskComplete());
			dto.setBussinessProcess(String.valueOf(model.getInstanceId().getBusinessProcess()));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return dto;
	}
}
