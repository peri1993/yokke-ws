package com.mega.ws.service.mapper;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mega.ws.model.AttendanceLogs;
import com.mega.ws.service.FileAttachmentService;
import com.mega.ws.service.dto.AttendanceLogsDTO;
import com.mega.ws.service.dto.FileAttachmentDTO;

@Service
public class AttendanceMapper extends BaseMapper {
	
	@Autowired
	private FileAttachmentService fileAttachmentService;
	
	public AttendanceLogsDTO entityAttendanceDTO(AttendanceLogs model) {
		AttendanceLogsDTO dto = new AttendanceLogsDTO();
		try {
			
			List<FileAttachmentDTO> listDto = fileAttachmentService.listFile(model.getId().getEmpId(), model.getId().getStartDate());
			dto.setEmpId(model.getId().getEmpId());
			dto.setStartDate(parseDate(model.getId().getStartDate()));
			dto.setEndDate(parseDate(model.getId().getEndDate()));
			dto.setEmpArea(model.getEmpArea());
			dto.setEmpSubArea(model.getEmpSubArea());
			dto.setEmpSubAreaDescription(model.getEmpSubAreaDescription());
			dto.setLandscape("100");
			dto.setLongitudeCode(model.getLongitudeCode());
			dto.setLatitudeCode(model.getLatitudeCode());
			dto.setLongitudeCodeOut(model.getLongitudeCodeOut());
			dto.setLatitudeCodeOut(model.getLatitudeCodeOut());
			dto.setRadius(model.getRadius());
			dto.setTimeStart(model.getTimeStart());
			dto.setTimeEnd(model.getTimeEnd());
			dto.setListFile(listDto);
			dto.setFullName(findName(model.getId().getEmpId()));
			dto.setFileIn(fileIn(listDto));
			dto.setFileOut(fileOut(listDto));
			dto.setFileDirIn(fileDirIn(listDto));
			dto.setFileDirOut(fileDirOut(listDto));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dto;
	}
	
	private String fileIn(List<FileAttachmentDTO> listFile) {
		if (!listFile.isEmpty()) {
			for (int i = 0; i < listFile.size(); i++) {
				FileAttachmentDTO dto = listFile.get(i);
				if ("In".equals(dto.getStatus())) {
					return dto.getFileName();
				}
			}
			return null;
		} else {
			return null;
		}
	}
	
	private String fileOut(List<FileAttachmentDTO> listFile) {
		if (!listFile.isEmpty()) {
			for (int i = 0; i < listFile.size(); i++) {
				FileAttachmentDTO dto = listFile.get(i);
				if ("Out".equals(dto.getStatus())) {
					return dto.getFileName();
				}
			}
			return null;
		} else {
			return null;
		}
	}
	
	private String fileDirIn(List<FileAttachmentDTO> listFile) {
		if (!listFile.isEmpty()) {
			for (int i = 0; i < listFile.size(); i++) {
				FileAttachmentDTO dto = listFile.get(i);
				if ("In".equals(dto.getStatus())) {
					return dto.getDirectory();
				}
			}
			return null;
		} else {
			return null;
		}
	}
	
	private String fileDirOut(List<FileAttachmentDTO> listFile) {
		if (!listFile.isEmpty()) {
			for (int i = 0; i < listFile.size(); i++) {
				FileAttachmentDTO dto = listFile.get(i);
				if ("In".equals(dto.getStatus())) {
					return dto.getDirectory();
				}
			}
			return null;
		} else {
			return null;
		}
	}
}
