package com.mega.ws.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import com.fasterxml.jackson.annotation.JsonView;
import com.mega.ws.jsonview.GenericJsonView;

@MappedSuperclass
public class AbstractEntity  implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = -4940507391357407590L;

	@Column(name = "create_datetime", updatable = false)
	@JsonView(GenericJsonView.Simple.class)
	private Date createDate;

	@Column(name = "last_modified_datetime", updatable = true)
	@JsonView(GenericJsonView.Simple.class)
	private Date lastModifiedDate;


	
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

}
