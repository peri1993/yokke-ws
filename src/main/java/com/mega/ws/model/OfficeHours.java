package com.mega.ws.model;

import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "office_hours")
public class OfficeHours {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
	@SequenceGenerator(name = "sequenceGenerator")
	private Long hours_id;

	@Column(name = "name")
	private String name;

	@Column(name = "start_time")
	private String startTime;

	@Column(name = "end_time")
	private String endtTime;

	@Column(name = "created_by")
	private String createdBy;

	@Column(name = "created_dt")
	private Timestamp createdDt;

	@Column(name = "updated_by")
	private String updatedBy;

	@Column(name = "updated_dt")
	private Timestamp updatedDt;

	@Column(name = "location_extend")
	private String locationExtend;
	
	@Column(name = "reason")
	private String reason;

	@OneToMany(mappedBy = "officeHours", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<UserOfficeHours> listEmployee;
	
	
	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Long getHours_id() {
		return hours_id;
	}

	public void setHours_id(Long hours_id) {
		this.hours_id = hours_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndtTime() {
		return endtTime;
	}

	public void setEndtTime(String endtTime) {
		this.endtTime = endtTime;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Timestamp createdDt) {
		this.createdDt = createdDt;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Timestamp updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Set<UserOfficeHours> getListEmployee() {
		return listEmployee;
	}

	public void setListEmployee(Set<UserOfficeHours> listEmployee) {
		this.listEmployee = listEmployee;
	}

	public String getLocationExtend() {
		return locationExtend;
	}

	public void setLocationExtend(String locationExtend) {
		this.locationExtend = locationExtend;
	}

}
