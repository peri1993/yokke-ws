package com.mega.ws.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.mega.ws.identity.BranchConfID;

@Entity
@Table(name = "branch_conf")
public class BranchConf implements Serializable {

	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private BranchConfID id;

	@Column(name = "emp_subarea_description")
	private String empSubAreaDescription;

	@Column(name = "address")
	private String address;

	@Column(name = "npwp")
	private String npwp;

	@Column(name = "latitude_code")
	private String latitudeCode;

	@Column(name = "longitude_code")
	private String longitudeCode;
	
	@Column(name = "radius")
	private String radius;
	
	public String getRadius() {
		return radius;
	}

	public void setRadius(String radius) {
		this.radius = radius;
	}

	public BranchConfID getId() {
		return id;
	}

	public void setId(BranchConfID id) {
		this.id = id;
	}

	public String getEmpSubAreaDescription() {
		return empSubAreaDescription;
	}

	public void setEmpSubAreaDescription(String empSubAreaDescription) {
		this.empSubAreaDescription = empSubAreaDescription;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getNpwp() {
		return npwp;
	}

	public void setNpwp(String npwp) {
		this.npwp = npwp;
	}

	public String getLatitudeCode() {
		return latitudeCode;
	}

	public void setLatitudeCode(String latitudeCode) {
		this.latitudeCode = latitudeCode;
	}

	public String getLongitudeCode() {
		return longitudeCode;
	}

	public void setLongitudeCode(String longitudeCode) {
		this.longitudeCode = longitudeCode;
	}

}
