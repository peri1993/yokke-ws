package com.mega.ws.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "instances_logs")
public class InstancesLogs {

	@Id
	@Column(name = "instances_id")
	private Long instancesId;

	@Column(name = "assigner")
	private String assigner;

	@Column(name = "requester_id")
	private String requesterId;

	@Column(name = "requester_name")
	private String requesterName;

	@Column(name = "business_process")
	private String businessProcess;

	@Column(name = "subject")
	private String subject;

	@Column(name = "created_by")
	private String created_by;

	@Column(name = "created_dt")
	private Timestamp createdDt;

	@Column(name = "updated_by")
	private String updatedBy;

	@Column(name = "updated_dt")
	private Timestamp updatedDt;

	@Column(name = "absences_type")
	private String absencesType;

	@Column(name = "absences_desc")
	private String absencesDesc;

	@Column(name = "approval_ds_1")
	private String approvalDs1;

	@Column(name = "name_approval_ds_1")
	private String nameApprovalDs1;

	@Column(name = "approval_ds_2")
	private String approvalDs2;

	@Column(name = "name_approval_ds_2")
	private String nameApprovalDs2;

	@Column(name = "start_date")
	private String startDate;

	@Column(name = "end_date")
	private String endDate;
	
	@Column(name = "reason")
	private String reason;

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getCreated_by() {
		return created_by;
	}

	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Timestamp updatedDt) {
		this.updatedDt = updatedDt;
	}

	public String getAbsencesType() {
		return absencesType;
	}

	public void setAbsencesType(String absencesType) {
		this.absencesType = absencesType;
	}

	public String getAbsencesDesc() {
		return absencesDesc;
	}

	public void setAbsencesDesc(String absencesDesc) {
		this.absencesDesc = absencesDesc;
	}

	public String getApprovalDs1() {
		return approvalDs1;
	}

	public void setApprovalDs1(String approvalDs1) {
		this.approvalDs1 = approvalDs1;
	}

	public String getNameApprovalDs1() {
		return nameApprovalDs1;
	}

	public void setNameApprovalDs1(String nameApprovalDs1) {
		this.nameApprovalDs1 = nameApprovalDs1;
	}

	public String getApprovalDs2() {
		return approvalDs2;
	}

	public void setApprovalDs2(String approvalDs2) {
		this.approvalDs2 = approvalDs2;
	}

	public String getNameApprovalDs2() {
		return nameApprovalDs2;
	}

	public void setNameApprovalDs2(String nameApprovalDs2) {
		this.nameApprovalDs2 = nameApprovalDs2;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public Long getInstancesId() {
		return instancesId;
	}

	public void setInstancesId(Long instancesId) {
		this.instancesId = instancesId;
	}

	public String getAssigner() {
		return assigner;
	}

	public void setAssigner(String assigner) {
		this.assigner = assigner;
	}

	public String getRequesterId() {
		return requesterId;
	}

	public void setRequesterId(String requesterId) {
		this.requesterId = requesterId;
	}

	public String getRequesterName() {
		return requesterName;
	}

	public void setRequesterName(String requesterName) {
		this.requesterName = requesterName;
	}

	public String getBusinessProcess() {
		return businessProcess;
	}

	public void setBusinessProcess(String businessProcess) {
		this.businessProcess = businessProcess;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public Timestamp getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Timestamp createdDt) {
		this.createdDt = createdDt;
	}

}
