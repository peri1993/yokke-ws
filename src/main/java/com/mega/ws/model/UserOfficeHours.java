package com.mega.ws.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "user_office_hours")
public class UserOfficeHours {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
	@SequenceGenerator(name = "sequenceGenerator")
	private Long id;

	@Column(name = "empId")
	private String empId;

	@Column(name = "fullname")
	private String fullname;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "hours_id", insertable = true, updatable = true)
	private OfficeHours officeHours;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmpId() {
		return empId;
	}

	public void setEmpId(String empId) {
		this.empId = empId;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public OfficeHours getOfficeHours() {
		return officeHours;
	}

	public void setOfficeHours(OfficeHours officeHours) {
		this.officeHours = officeHours;
	}

}
