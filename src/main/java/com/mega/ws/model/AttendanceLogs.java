package com.mega.ws.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.mega.ws.identity.AttendanceLogsID;

@Entity
@Table(name = "attendance_logs")
public class AttendanceLogs implements Serializable {

	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private AttendanceLogsID id;

	@Column(name = "emp_subarea")
	private String empSubArea;

	@Column(name = "emp_area")
	private String empArea;

	@Column(name = "emp_subarea_description")
	private String empSubAreaDescription;

	@Column(name = "longitude_code")
	private String longitudeCode;

	@Column(name = "latitude_code")
	private String latitudeCode;

	@Column(name = "longitude_code_out")
	private String longitudeCodeOut;

	@Column(name = "latitude_code_out")
	private String latitudeCodeOut;

	@Column(name = "radius")
	private String radius;

	@Column(name = "time_start")
	private String timeStart;

	@Column(name = "time_end")
	private String timeEnd;
	
	public String getLongitudeCodeOut() {
		return longitudeCodeOut;
	}

	public void setLongitudeCodeOut(String longitudeCodeOut) {
		this.longitudeCodeOut = longitudeCodeOut;
	}

	public String getLatitudeCodeOut() {
		return latitudeCodeOut;
	}

	public void setLatitudeCodeOut(String latitudeCodeOut) {
		this.latitudeCodeOut = latitudeCodeOut;
	}

	public String getTimeStart() {
		return timeStart;
	}

	public void setTimeStart(String timeStart) {
		this.timeStart = timeStart;
	}

	public String getTimeEnd() {
		return timeEnd;
	}

	public void setTimeEnd(String timeEnd) {
		this.timeEnd = timeEnd;
	}

	public AttendanceLogsID getId() {
		return id;
	}

	public void setId(AttendanceLogsID id) {
		this.id = id;
	}

	public String getEmpSubArea() {
		return empSubArea;
	}

	public void setEmpSubArea(String empSubArea) {
		this.empSubArea = empSubArea;
	}

	public String getEmpArea() {
		return empArea;
	}

	public void setEmpArea(String empArea) {
		this.empArea = empArea;
	}

	public String getEmpSubAreaDescription() {
		return empSubAreaDescription;
	}

	public void setEmpSubAreaDescription(String empSubAreaDescription) {
		this.empSubAreaDescription = empSubAreaDescription;
	}

	public String getLongitudeCode() {
		return longitudeCode;
	}

	public void setLongitudeCode(String longitudeCode) {
		this.longitudeCode = longitudeCode;
	}

	public String getLatitudeCode() {
		return latitudeCode;
	}

	public void setLatitudeCode(String latitudeCode) {
		this.latitudeCode = latitudeCode;
	}

	public String getRadius() {
		return radius;
	}

	public void setRadius(String radius) {
		this.radius = radius;
	}

}
