package com.mega.ws.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "notifications")
public class Notifications {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
	@SequenceGenerator(name = "sequenceGenerator")
	private Long id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "instance_id", insertable = true, updatable = true)
	private InstancesLogs instancesId;

	@Column(name = "created_by")
	private String createdBy;

	@Column(name = "created_dt")
	private Timestamp createdDt;

	@Column(name = "assigner")
	private String assigner;

	@Column(name = "flag_read")
	private String flagRead;

	@Column(name = "stages")
	private String stages;

	@Column(name = "description")
	private String description;

	@Column(name = "description_notif")
	private String descriptionNotif;

	@Column(name = "business_process")
	private String businesProcess;

	public String getDescriptionNotif() {
		return descriptionNotif;
	}

	public void setDescriptionNotif(String descriptionNotif) {
		this.descriptionNotif = descriptionNotif;
	}

	public String getBusinesProcess() {
		return businesProcess;
	}

	public void setBusinesProcess(String businesProcess) {
		this.businesProcess = businesProcess;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public InstancesLogs getInstancesId() {
		return instancesId;
	}

	public void setInstancesId(InstancesLogs instancesId) {
		this.instancesId = instancesId;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Timestamp createdDt) {
		this.createdDt = createdDt;
	}

	public String getAssigner() {
		return assigner;
	}

	public void setAssigner(String assigner) {
		this.assigner = assigner;
	}

	public String getFlagRead() {
		return flagRead;
	}

	public void setFlagRead(String flagRead) {
		this.flagRead = flagRead;
	}

	public String getStages() {
		return stages;
	}

	public void setStages(String stages) {
		this.stages = stages;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
