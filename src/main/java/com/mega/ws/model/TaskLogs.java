package com.mega.ws.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "task_logs")
public class TaskLogs {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
	@SequenceGenerator(name = "sequenceGenerator")
	private Long id;

	@Column(name = "task_id")
	private Long taskId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "instance_id", insertable = true, updatable = true)
	private InstancesLogs instanceId;

	@Column(name = "user_stages")
	private String userStages;

	@Column(name = "stages")
	private String stages;

	@Column(name = "created_by")
	private String createdBy;

	@Column(name = "created_dt")
	private Timestamp createdDt;

	@Column(name = "flag_task_complete")
	private String flagTaskComplete;

	public String getUserStages() {
		return userStages;
	}

	public void setUserStages(String userStages) {
		this.userStages = userStages;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public InstancesLogs getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(InstancesLogs instanceId) {
		this.instanceId = instanceId;
	}

	public String getStages() {
		return stages;
	}

	public void setStages(String stages) {
		this.stages = stages;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Timestamp createdDt) {
		this.createdDt = createdDt;
	}

	public String getFlagTaskComplete() {
		return flagTaskComplete;
	}

	public void setFlagTaskComplete(String flagTaskComplete) {
		this.flagTaskComplete = flagTaskComplete;
	}

}
