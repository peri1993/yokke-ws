package com.mega.ws.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.mega.ws.identity.ScheduleAttendanceID;

@Entity
@Table(name = "schedule_attendance")
public class ScheduleAttendance implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private ScheduleAttendanceID id;

	@Column(name = "address")
	private String address;
	
	@Column(name = "time_start")
	private String timeStart;

	@Column(name = "time_end")
	private String timeEnd;


	public String getTimeStart() {
		return timeStart;
	}

	public void setTimeStart(String timeStart) {
		this.timeStart = timeStart;
	}

	public String getTimeEnd() {
		return timeEnd;
	}

	public void setTimeEnd(String timeEnd) {
		this.timeEnd = timeEnd;
	}

	public ScheduleAttendanceID getId() {
		return id;
	}

	public void setId(ScheduleAttendanceID id) {
		this.id = id;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

}
