package com.mega.ws.scheduler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.mega.ws.identity.BranchConfID;
import com.mega.ws.jbpm.MappingURL;
import com.mega.ws.model.BranchConf;
import com.mega.ws.model.OfficeHours;
import com.mega.ws.model.UserOfficeHours;
import com.mega.ws.repository.BranchConfRepository;
import com.mega.ws.repository.OfficeHoursRepository;
import com.mega.ws.repository.UserOfficeHoursRepository;
import com.mega.ws.request.BranchIdRequest;
import com.mega.ws.response.JBPM_MappingResponse;
import com.mega.ws.util.HeaderSettings;

@Component
public class SchedulerController {

	private BranchConfRepository branchConfRepository;

	@Autowired
	public MappingURL mappingURL;

	@Autowired
	public HeaderSettings headerSettings;

	public RestTemplate restTemplate = new RestTemplate();

	private static final String STRING_EMPTY = "";
	
	private OfficeHoursRepository officeHoursRepository;

	private UserOfficeHoursRepository userOfficeHoursRepository;
	
	public SchedulerController(BranchConfRepository branchConfRepository, OfficeHoursRepository  officeHoursRepository, UserOfficeHoursRepository userOfficeHoursRepository) {
		this.branchConfRepository = branchConfRepository;
		this.officeHoursRepository = officeHoursRepository;
		this.userOfficeHoursRepository = userOfficeHoursRepository;
	}
	
	@Scheduled(cron = "0 0 12 * * ?")
	public void schedulerInsertBranch() {

		List<BranchIdRequest> listRequest = new ArrayList<BranchIdRequest>();

		List<BranchConf> listBranch = branchConfRepository.findBranchConf();

		if (!listBranch.isEmpty()) {
			for (int i = 0; i < listBranch.size(); i++) {
				BranchConf conf = listBranch.get(i);
				BranchIdRequest id = new BranchIdRequest();
				id.setEmpArea(conf.getId().getEmpArea());
				id.setEmpSubArea(conf.getId().getEmpSubArea());
				id.setLandscape(conf.getId().getLandscape());
				listRequest.add(id);
			}
		} else {
			BranchIdRequest request = new BranchIdRequest();
			request.setEmpArea(STRING_EMPTY);
			request.setEmpSubArea(STRING_EMPTY);
			request.setLandscape(STRING_EMPTY);
			listRequest.add(request);
		}

		saveBranch(listRequest);

	}
	
	
	//@Scheduled(fixedDelay = 30000)
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Scheduled(cron = "0 0 12 * * ?")
	public void schedulerInsertUserJadwal() {
		
		List<OfficeHours> listHours = officeHoursRepository.findAll();
		
		if (!listHours.isEmpty()) {
			
			OfficeHours hours = new OfficeHours();
			for (int i = 0; i < listHours.size(); i++) {
				if ("default".equals(listHours.get(i).getName())) {
					hours = listHours.get(i);
				}
			}
			
			if (null != hours.getHours_id()) {
				JBPM_MappingResponse mapping = mappingURL.findAllEmployeeActiveForAbsences();
				HttpEntity<Object> requestEntity = new HttpEntity<>(headerSettings.setupHeader());
				Object response = restTemplate.exchange(mapping.getUrl(), mapping.getMethod(), requestEntity, Object.class);
				ResponseEntity<HashMap> responseEntity = (ResponseEntity<HashMap>) response;
				List<HashMap<String, Object>> listUser = (List<HashMap<String, Object>>) responseEntity.getBody().get("listResponse");
				
				for (Iterator iterator = listUser.iterator(); iterator.hasNext();) {
					HashMap<String, Object> model = (HashMap<String, Object>) iterator.next();
					String empId = String.valueOf(model.get("empId"));
					String fullname = String.valueOf(model.get("fullname"));
					
					List<UserOfficeHours> list = userOfficeHoursRepository.findByEmpId(empId);
					if (list.isEmpty()) {
						UserOfficeHours userHours = new UserOfficeHours();
						
						userHours.setEmpId(empId);
						userHours.setFullname(fullname);
						userHours.setOfficeHours(hours);
						userOfficeHoursRepository.save(userHours);
					}
				}
			}
		}

		
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void saveBranch(List<BranchIdRequest> listRequest) {

		JBPM_MappingResponse mapping = mappingURL.schedulerBracnch();
		HttpEntity<List<BranchIdRequest>> requestEntity = new HttpEntity<>(listRequest, headerSettings.setupHeader());
		Object response = restTemplate.exchange(mapping.getUrl(), mapping.getMethod(), requestEntity, Object.class);
		ResponseEntity<HashMap> responseEntity = (ResponseEntity<HashMap>) response;
		List<Object> listBranch = (List<Object>) responseEntity.getBody().get("listResponse");

		if (!listBranch.isEmpty()) {
			for (int i = 0; i < listBranch.size(); i++) {
				LinkedHashMap<String, Object> model = (LinkedHashMap<String, Object>) listBranch.get(i);
				LinkedHashMap<String, Object> id = (LinkedHashMap<String, Object>) model.get("id");

				BranchConfID idBranch = new BranchConfID();
				idBranch.setEmpArea(String.valueOf(id.get("empArea")));
				idBranch.setEmpSubArea(String.valueOf(id.get("empSubArea")));
				idBranch.setLandscape(String.valueOf(id.get("landscape")));

				BranchConf branch = new BranchConf();
				branch.setId(idBranch);
				branch.setAddress(String.valueOf(model.get("address")));
				branch.setEmpSubAreaDescription(String.valueOf(model.get("empSubAreaDescription")));
				branch.setNpwp(String.valueOf(model.get("npwp")));
				branch.setLatitudeCode(STRING_EMPTY);
				branch.setLongitudeCode(STRING_EMPTY);

				BranchConf branchCheck = branchConfRepository.findOne(idBranch);
				if (null != branchCheck) {
					if (!branch.getAddress().equals(branchCheck.getAddress())
							|| !branch.getEmpSubAreaDescription().equals(branchCheck.getEmpSubAreaDescription())
							|| !branch.getNpwp().equals(branchCheck.getNpwp())) {
						branchConfRepository.delete(branchCheck);
						branchConfRepository.save(branch);
					}
				}

				branchConfRepository.save(branch);

			}
		}

	}
}
