package com.mega.ws;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import com.mega.ws.response.LeaveResponse;

@Component
public class CreateRequestWsConnector extends BaseWsConnector{

	@Value("#{'http://localhost:8090/rest/'}")
	private String ws;
	
	@Value("#{'ess-mega-jbpm-kjar-1_0'}")
	private String container;
	
	@Value("#{'ess-mega-jbpm-kjar.leaveProcess'}")
	private String processId;
	
	public String createRequest(LeaveResponse leave) {
		String url = ws.concat("server/containers/").concat(container).concat("/processes/").concat(processId).concat("/instances");
		return consumePost(url, HttpMethod.POST, leave, null, null);
	}
}
