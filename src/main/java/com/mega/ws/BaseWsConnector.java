package com.mega.ws;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonObject;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.AbstractHttpMessage;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.jose4j.jwe.ContentEncryptionAlgorithmIdentifiers;
import org.jose4j.jwe.JsonWebEncryption;
import org.jose4j.jwe.KeyManagementAlgorithmIdentifiers;
import org.jose4j.keys.AesKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import com.google.api.client.repackaged.org.apache.commons.codec.binary.Base64;
import com.google.common.collect.Maps;
import com.mega.ws.response.BaseResponse;

@Component
public class BaseWsConnector {

	protected static Logger logger = LoggerFactory.getLogger(BaseWsConnector.class);
	protected static final String RESPONSE_ENCODING = "UTF-8";
	public static final String PAGE_SIZE = "10";
	public static final String PAGE_SIZE_2 = "20";
	public static final String PAGE_SIZE_25 = "25";

	@Value("#{'wbadmin'}")
	public String jbpmusername;

	@Value("#{'wbadmin'}")
	public String jbpmpassword;

	protected <T> T parseJson(String response, Class<T> clazz)
			throws IOException, JsonParseException, JsonMappingException {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.readValue(response, clazz);
	}

	protected <T> List<T> parseJsonList(String response, Class<T> clazz)
			throws IOException, JsonParseException, JsonMappingException {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.readValue(response, new TypeReference<List<T>>() {
		});
	}
	
	public String consumePost(String path, HttpMethod method, Object requestBody,
			List<NameValuePair> formDataParameter, Locale locale) {
		
		final Map<String, String> headers = Maps.newHashMap();
		return callByPostMethod(path, headers, requestBody, formDataParameter);
	}

	public <T extends BaseResponse> T consumeWs(String path, HttpMethod method, Object requestBody,
			List<NameValuePair> formDataParameter, Class<T> responseClazz, Locale locale) {

		final String url = path;
		logger.info("path: " + url);

		final Map<String, String> headers = Maps.newHashMap();
		if (locale != null) {
			headers.put("language", locale.getLanguage());
		}
		switch (method) {
		case POST:
			return callByPostMethod(url, headers, requestBody, formDataParameter, responseClazz);
		case GET:
			return callByGetMethod(url, headers, formDataParameter, responseClazz);
		case PUT:
			return callByPutMethod(url, headers, requestBody, responseClazz);
		case DELETE:
			return callByDeleteMethod(url, headers, responseClazz);
		default:
			throw new IllegalArgumentException("Unsupported Http method");
		}
	}

	protected void addHeaders(Map<String, String> headers, AbstractHttpMessage method) {
		headers.entrySet().stream().forEach((headParam) -> {
			method.addHeader(headParam.getKey(), headParam.getValue());
		});

		String encoding = jbpmusername + ":" + jbpmpassword;
		byte[] encodedAuth = Base64.encodeBase64(encoding.getBytes(Charset.forName("US-ASCII")));
		String authHeader = new String(encodedAuth);

		method.addHeader("Authorization", "Basic " + authHeader);

		method.addHeader("Content-Type", "application/json");
		method.addHeader("Accept", "application/json");
	}

	private <T extends BaseResponse> ResponseHandler<T> newResponseHandler(final Class<T> responseClazz) {
		return new ResponseHandler<T>() {
			@Override
			public T handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
				final String responseStr = IOUtils.toString(response.getEntity().getContent(), RESPONSE_ENCODING);
				logger.debug("responseStr: " + responseStr);
				return parseJson(responseStr, responseClazz);
			}
		};
	}
	
	private <T extends BaseResponse> T callWithBody(String url, HttpEntityEnclosingRequestBase method,
			Map<String, String> header, Object requestBody, Class<T> responseClazz) {
		try {
			CloseableHttpClient client = (HttpClientBuilder.create()).build();
			URI uri = new URI(url);
			method.setURI(uri);
			if (header != null) {
				this.addHeaders(header, method);
			}
			if (requestBody != null) {
				ObjectMapper mapper = new ObjectMapper();
				String requestStr = mapper.writeValueAsString(requestBody);
				logger.debug("requestStr: " + requestStr);
				method.setEntity(new StringEntity(requestStr));
			}
			return client.execute(method, newResponseHandler(responseClazz));
		} catch (IOException | URISyntaxException e) {
			logger.error(ExceptionUtils.getStackTrace(e));
		}
		return null;
	}

	private String callWithBody(String url, HttpEntityEnclosingRequestBase method,
			Map<String, String> header, Object requestBody, List<NameValuePair> formDataParameter) {
		try {
			CloseableHttpClient client = (HttpClientBuilder.create()).build();
			URI uri = new URI(url);
			method.setURI(uri);
			this.addHeaders(header, method);

			if (requestBody != null) {
				ObjectMapper mapper = new ObjectMapper();
				String requestStr = mapper.writeValueAsString(requestBody);
				logger.debug("requestStr: " + requestStr);
				method.setEntity(new StringEntity(requestStr));
			} else if (formDataParameter != null) {
				if (!formDataParameter.isEmpty()) {
					List<NameValuePair> param = new ArrayList<>();
					param.add(new BasicNameValuePair("grant_type", "grant_type"));
					param.add(new BasicNameValuePair("scopes", "web-app"));
					param.add(new BasicNameValuePair("username", "admin"));
					param.add(new BasicNameValuePair("password", "admin"));

					method.setEntity(new UrlEncodedFormEntity(param, "UTF-8"));

					// method.setEntity(new UrlEncodedFormEntity(param));
				}
			}
			ResponseHandler<String> responseHandler = response -> {
				int status = response.getStatusLine().getStatusCode();
				if (status >= 200 && status < 300) {
					HttpEntity responseEntity = response.getEntity();
					return responseEntity != null ? EntityUtils.toString(responseEntity) : null;
				} else {
					throw new ClientProtocolException("Unexpected response status: " + status);
				}
			};
			String responseBody = client.execute(method, responseHandler);
			return responseBody;
			//return client.execute(method);
		} catch (IOException | URISyntaxException e) {
			logger.error(ExceptionUtils.getStackTrace(e));
		}
		return null;
	}
	
	private <T extends BaseResponse> T callWithBody(String url, HttpEntityEnclosingRequestBase method,
			Map<String, String> header, Object requestBody, List<NameValuePair> formDataParameter,
			Class<T> responseClazz) {
		try {
			CloseableHttpClient client = (HttpClientBuilder.create()).build();
			URI uri = new URI(url);
			method.setURI(uri);
			this.addHeaders(header, method);

			if (requestBody != null) {
				ObjectMapper mapper = new ObjectMapper();
				String requestStr = mapper.writeValueAsString(requestBody);
				logger.debug("requestStr: " + requestStr);
				method.setEntity(new StringEntity(requestStr));
			} else if (formDataParameter != null) {
				if (!formDataParameter.isEmpty()) {
					List<NameValuePair> param = new ArrayList<>();
					param.add(new BasicNameValuePair("grant_type", "grant_type"));
					param.add(new BasicNameValuePair("scopes", "web-app"));
					param.add(new BasicNameValuePair("username", "admin"));
					param.add(new BasicNameValuePair("password", "admin"));

					method.setEntity(new UrlEncodedFormEntity(param, "UTF-8"));

					// method.setEntity(new UrlEncodedFormEntity(param));
				}
			}
			return client.execute(method, newResponseHandler(responseClazz));
		} catch (IOException | URISyntaxException e) {
			logger.error(ExceptionUtils.getStackTrace(e));
		}
		return null;
	}

	@SuppressWarnings("unused")
	private <T extends BaseResponse> T callByPostMethod(String url, Map<String, String> header, Object requestBody,
			Class<T> responseClazz) {
		return callWithBody(url, new HttpPost(), header, requestBody, responseClazz);
	}

	private <T extends BaseResponse> T callByPostMethod(String url, Map<String, String> header, Object requestBody,
			List<NameValuePair> formDataParameter, Class<T> responseClazz) {
		return callWithBody(url, new HttpPost(), header, requestBody, formDataParameter, responseClazz);
	}
	
	private String callByPostMethod(String url, Map<String, String> header, Object requestBody,
			List<NameValuePair> formDataParameter) {
		return callWithBody(url, new HttpPost(), header, requestBody, formDataParameter);
	}

	private <T extends BaseResponse> T callByPutMethod(String url, Map<String, String> header, Object requestBody,
			Class<T> responseClazz) {
		return callWithBody(url, new HttpPut(), header, requestBody, responseClazz);
	}

	private <T extends BaseResponse> T callByGetMethod(final String url, final Map<String, String> headers,
			List<NameValuePair> formDataParameter, final Class<T> responseClazz) {
		try {
			CloseableHttpClient restClient = (HttpClientBuilder.create()).build();
			HttpGet method = new HttpGet();
			URI uri = new URI(url);
			method.setURI(uri);
			if (headers != null) {
				this.addHeaders(headers, method);
			}
			if (formDataParameter != null) {
				uri = new URIBuilder(url).addParameters(formDataParameter).build();
				method.setURI(uri);
			}
			logger.debug("requestStr: " + url);
			return restClient.execute(method, newResponseHandler(responseClazz));
		} catch (IOException | URISyntaxException e) {
			logger.error(ExceptionUtils.getStackTrace(e));
		}
		return null;
	}

	private <T extends BaseResponse> T callByDeleteMethod(final String url, final Map<String, String> headers,
			final Class<T> responseClazz) {
		try {
			CloseableHttpClient restClient = (HttpClientBuilder.create()).build();
			// HttpGet method = new HttpGet();
			HttpDelete method = new HttpDelete();
			URI uri = new URI(url);
			method.setURI(uri);
			if (headers != null) {
				this.addHeaders(headers, method);
			}
			logger.debug("requestStr: " + url);
			return restClient.execute(method, newResponseHandler(responseClazz));
		} catch (IOException | URISyntaxException e) {
			logger.error(ExceptionUtils.getStackTrace(e));
		}
		return null;
	}

	public <T> T decodeJWE(String jwe, Class<T> clazz) throws Exception {
		String jweSerial = "";
		try {
//			byte[] secret = salt.getBytes(StandardCharsets.US_ASCII);
			byte[] secret = ("password").getBytes(StandardCharsets.US_ASCII);
			MessageDigest sha = MessageDigest.getInstance("SHA-1");
			secret = sha.digest(secret);
			secret = Arrays.copyOf(secret, 16);

			Key key = new AesKey(secret);
			JsonWebEncryption jweDecrypt = new JsonWebEncryption();
			jweDecrypt.setAlgorithmHeaderValue(KeyManagementAlgorithmIdentifiers.A128KW);
			jweDecrypt
					.setEncryptionMethodHeaderParameter(ContentEncryptionAlgorithmIdentifiers.AES_128_CBC_HMAC_SHA_256);
			jweDecrypt.setKey(key);
			jweDecrypt.setCompactSerialization(jwe);

			jweSerial = jweDecrypt.getPlaintextString();
			logger.debug("Serialized Encrypted JWE: " + jweSerial);
		} catch (Exception e) {
			logger.error("Error Decrypted JWE: " + e.getMessage());
			throw new Exception(e);
		}

		return parseJson(jweSerial, clazz);
	}

	public <T> T encodeJWE(Object request, Class<T> clazz) throws Exception {
		String serializedJwe = "";
		try {
			ObjectMapper mapper = new ObjectMapper();
			String requestStr = mapper.writeValueAsString(request);

//			byte[] secret = salt.getBytes(StandardCharsets.US_ASCII);
			byte[] secret = ("password").getBytes(StandardCharsets.US_ASCII);
			MessageDigest sha = MessageDigest.getInstance("SHA-1");
			secret = sha.digest(secret);
			secret = Arrays.copyOf(secret, 16);

			Key key = new AesKey(secret);
			JsonWebEncryption jwe = new JsonWebEncryption();
			jwe.setPayload(requestStr);
			jwe.setAlgorithmHeaderValue(KeyManagementAlgorithmIdentifiers.A128KW);
			jwe.setEncryptionMethodHeaderParameter(ContentEncryptionAlgorithmIdentifiers.AES_128_CBC_HMAC_SHA_256);
			jwe.setKey(key);
			serializedJwe = jwe.getCompactSerialization();
			logger.debug("Serialized Encrypted JWE: " + serializedJwe);

		} catch (Exception e) {
			logger.error("Error Encrypted JWE: " + e.getMessage());
			throw new Exception(e);
		}

		JsonObject json = Json.createObjectBuilder().add("encMsg", serializedJwe).build();

		return parseJson(json.toString(), clazz);
	}

}
