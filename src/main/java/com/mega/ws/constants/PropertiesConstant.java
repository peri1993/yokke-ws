package com.mega.ws.constants;

public class PropertiesConstant {
	
	public static final String TASK_STAGES_1 		= "Waiting for Approval 1";
	public static final String TASK_STAGES_2 		= "Waiting for Approval 2";
	public static final String TASK_ONE_APPROVAL	= "Task hanya memiliki 1 Approval";
}
