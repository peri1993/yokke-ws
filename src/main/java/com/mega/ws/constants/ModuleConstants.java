package com.mega.ws.constants;

public class ModuleConstants {

	public static final String PERMIT_MODULE = "permit";
	public static final String LEAVE_MODULE = "leave";
	public static final String CHANGE_MODULE = "change";
	
	public static final String FILE_JADWAL_MODULE = "jadwal";  

}
