package com.mega.ws.constants;

/**
 * Constants for Spring Security authorities.
 */
public final class AuthoritiesConstants {

    public static final String INBOX = "Inbox";

    public static final String REQUEST_LEAVE = "Request Leave";

    public static final String HISTORY_LEAVE = "History Leave";

    public static final String REQUEST_PERMIT = "Request Permit";

    public static final String HISTORY_PERMIT = "History Permit";

    public static final String RE_ASSIGN_TASK = "Assigned Task";

    public static final String NOTIFICATION = "Notification";

    public static final String ROLE_MANAGEMENT = "Role Management";

    public static final String USER_MANAGEMENT = "User Management";
    
    public final static String HISTORY_ATTENDANCE = "Attendance History";
    
	public final static String LIST_ATTENDANCE= "List Attendance";
	
	public final static String QR_ATTENDANCE= "Qr Attendance";
	
	public final static String BRANCH_CONF= "Branch Config";
	
	public final static String ASSIGNER_HISTORY = "Assigner History";
	
	public final static String ALL_HISTORY_ASSIGNER = "All Assigner History";
	
	public final static String INFORMATION_WORK_HOURS = "Information Work Hours";

    private AuthoritiesConstants() {
    }
}
