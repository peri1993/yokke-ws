package com.mega.ws.repository;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.mega.ws.model.InstancesLogs;
import com.mega.ws.model.TaskLogs;

public interface TaskLogsRepository extends JpaRepository<TaskLogs, Long> {

	@Query(value = "SELECT a FROM com.mega.ws.model.TaskLogs a " + "WHERE a.createdBy=?1 " + "AND a.instanceId=?2 "
			+ "AND a.taskId=?3 "
			+ "AND a.id =(SELECT MAX(id) FROM com.mega.ws.model.TaskLogs WHERE createdBy=?1 AND instanceId=?2 AND taskId=?3)")
	TaskLogs findTaskLogs(String createdBy, InstancesLogs instancesId, Long taskId);

	@Query(value = "SELECT MAX(taskId) FROM com.mega.ws.model.TaskLogs WHERE instanceId=?1")
	Long getTaskId(InstancesLogs instancesId);

	@Query(value = "SELECT flagTaskComplete FROM com.mega.ws.model.TaskLogs WHERE instanceId=?1 "
			+ "AND createdDt=(SELECT MAX(createdDt) FROM com.mega.ws.model.TaskLogs WHERE instanceId=?1) "
			+ "AND id=(SELECT MAX(id) FROM com.mega.ws.model.TaskLogs WHERE instanceId=?1)")
	String getFlagTask(InstancesLogs instancesId);

	@Query(value = "SELECT logs.instanceId FROM com.mega.ws.model.TaskLogs logs WHERE logs.taskId=?1")
	InstancesLogs getInstancesByTaskId(Long taskId);

	@Query(value = "SELECT logs.instanceId FROM com.mega.ws.model.TaskLogs logs " + "WHERE logs.taskId=?1 "
			+ "AND logs.id = (SELECT MAX(id) FROM com.mega.ws.model.TaskLogs " + "WHERE instanceId=logs.instanceId) ")
	InstancesLogs getInstancesMaxIDByTask(Long taskId);

	@Query(value = "SELECT logs.stages FROM com.mega.ws.model.TaskLogs logs WHERE logs.createdBy=?1 "
			+ "AND createdDt=(SELECT MAX(createdDt) FROM com.mega.ws.model.TaskLogs WHERE instanceId=?2) "
			+ "AND id=(SELECT MAX(id) FROM com.mega.ws.model.TaskLogs WHERE instanceId=?2)")
	String lastStatus(String createdBy, InstancesLogs instancesId);

	@Query(value = "SELECT logs FROM com.mega.ws.model.TaskLogs logs WHERE logs.instanceId=?1")
	List<TaskLogs> detailTaskLogs(InstancesLogs instancesId);

	@Query(value = "SELECT logs FROM com.mega.ws.model.TaskLogs logs WHERE logs.userStages=?1 " 
			+ "AND upper(coalesce(logs.createdBy, '')) LIKE upper(concat('%', ?2, '%')) "
			+ "AND upper(coalesce(logs.stages, '')) LIKE upper(concat('%', ?3, '%')) "
			+ "AND (logs.stages = 'APPROVE' OR logs.stages = 'REJECT')"
			+ "ORDER BY logs.instanceId DESC")
	Page<TaskLogs> getTaskLogsById(String username, String paramCreatedBy, String paramStages,
			Pageable pageable);
	
	@Query(value = "SELECT logs FROM com.mega.ws.model.TaskLogs logs WHERE logs.userStages=?1 "
			+ "AND logs.instanceId.instancesId = ?2 " 
			+ "AND logs.taskId = 0 "
			+ "ORDER BY logs.instanceId DESC")
	Page<TaskLogs> getTaskLogsByIdInstance(String username, Long instanceId, Pageable pageable);
	
	@Query(value = "SELECT logs FROM com.mega.ws.model.TaskLogs logs WHERE logs.userStages=?1 "
			+ "AND logs.createdDt BETWEEN ?2 AND ?3 " 
			+ "AND logs.taskId = 0 "
			+ "ORDER BY logs.instanceId DESC")
	Page<TaskLogs> getTaskLogsByIdDate(String username, Timestamp startDate, Timestamp endDate, Pageable pageable);

	@Query(value = "SELECT logs FROM com.mega.ws.model.TaskLogs logs WHERE logs.taskId = 0 "
			+ "AND upper(coalesce(logs.createdBy, '')) LIKE upper(concat('%', ?1, '%')) "
			+ "AND upper(coalesce(logs.stages, '')) LIKE upper(concat('%', ?2, '%')) "
			+ "AND upper(coalesce(logs.userStages, '')) LIKE upper(concat('%', ?3, '%')) "
			+ "AND logs.userStages IS NOT NULL " + "ORDER BY logs.instanceId DESC ")
	Page<TaskLogs> findAllCompleteTask(String paramCreatedBy, String paramStages, String paramUserStages,
			Pageable pageable);

	@Query(value = "SELECT logs FROM com.mega.ws.model.TaskLogs logs WHERE logs.taskId = 0 "
			+ "AND logs.instanceId.instancesId = ?1 " 
			+ "AND logs.userStages IS NOT NULL "
			+ "ORDER BY logs.id DESC ")
	Page<TaskLogs> findAllCompleteTaskByInstanceId(Long instanceId, Pageable pageable);
	
	@Query(value = "SELECT logs FROM com.mega.ws.model.TaskLogs logs WHERE logs.taskId = 0 "
			+ "AND logs.createdDt BETWEEN ?1 AND ?2 "
			+ "AND logs.userStages IS NOT NULL "
			+ "ORDER BY logs.instanceId DESC ")
	Page<TaskLogs> findAllCompleteTaskByDate(Timestamp startDate, Timestamp endDate, Pageable pageable);
}
