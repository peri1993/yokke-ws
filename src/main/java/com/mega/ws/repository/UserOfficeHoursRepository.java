package com.mega.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.mega.ws.model.UserOfficeHours;

public interface UserOfficeHoursRepository extends JpaRepository<UserOfficeHours, Long> {
	
	@Query(value = "SELECT user FROM com.mega.ws.model.UserOfficeHours user WHERE user.empId = ?1 ORDER BY user.id ASC")
	List<UserOfficeHours> findByEmpId(String empId);

}
