package com.mega.ws.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.mega.ws.identity.ScheduleAttendanceID;
import com.mega.ws.model.ScheduleAttendance;

public interface ScheduleAttendanceLogsRepository extends JpaRepository<ScheduleAttendance, ScheduleAttendanceID>{
	
	@Query(value = " SELECT logs FROM com.mega.ws.model.ScheduleAttendance logs "
			+ "WHERE logs.id.empId = ?1 "
			/*
			 * + "AND upper(coalesce(logs.id.empId, '')) LIKE upper(concat('%', ?1, '%')) "
			 * +
			 * "AND upper(coalesce(logs.id.startDate, '')) LIKE upper(concat('%', ?2, '%')) "
			 * +
			 * "AND upper(coalesce(logs.id.endDate, '')) LIKE upper(concat('%', ?3, '%')) "
			 * + "AND upper(coalesce(logs.address, '')) LIKE upper(concat('%', ?4, '%')) " +
			 * "AND upper(coalesce(logs.timeStart, '')) LIKE upper(concat('%', ?5, '%')) " +
			 * "AND upper(coalesce(logs.timeEnd, '')) LIKE upper(concat('%', ?6, '%')) "
			 */
			+ "ORDER BY logs.id.startDate DESC ")
	Page<ScheduleAttendance> listAttendance(String empId, Pageable pageable);
	
	@Query(value = " SELECT logs FROM com.mega.ws.model.ScheduleAttendance logs "
			+ "WHERE logs.id.empId = ?1 "
			+ "AND logs.id.startDate = ?2 "
			+ "AND logs.id.endDate = ?3 ")
	ScheduleAttendance inputValidation(String empId, String startDate, String endDate);
	
	@Query(value = "SELECT logs FROM com.mega.ws.model.ScheduleAttendance logs "
			+ "WHERE logs.id.empId = ?1 "
			+ "AND ?2 BETWEEN logs.id.startDate AND logs.id.endDate ")
	List<ScheduleAttendance> flagAttendance(String username, String startDt);
}
