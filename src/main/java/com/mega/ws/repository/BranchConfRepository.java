package com.mega.ws.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.mega.ws.identity.BranchConfID;
import com.mega.ws.model.BranchConf;

public interface BranchConfRepository extends JpaRepository<BranchConf, BranchConfID> {

	@Query(value = " SELECT branch FROM com.mega.ws.model.BranchConf branch ")
	List<BranchConf> findBranchConf();

	@Query(value = "SELECT branch FROM com.mega.ws.model.BranchConf branch "
			+ "WHERE upper(coalesce(branch.id.empArea, '')) LIKE upper(concat('%', ?1, '%')) "
			+ "AND upper(coalesce(branch.id.empSubArea, '')) LIKE upper(concat('%', ?2, '%')) "
			+ "AND upper(coalesce(branch.address, '')) LIKE upper(concat('%', ?3, '%')) "
			+ "AND upper(coalesce(branch.empSubAreaDescription, '')) LIKE upper(concat('%', ?4, '%')) "
			+ "AND upper(coalesce(branch.npwp, '')) LIKE upper(concat('%', ?5, '%')) "
			+ "AND upper(coalesce(branch.latitudeCode, '')) LIKE upper(concat('%', ?6, '%')) "
			+ "AND upper(coalesce(branch.longitudeCode, '')) LIKE upper(concat('%', ?7, '%')) "
			+ "AND upper(coalesce(branch.radius, '')) LIKE upper(concat('%', ?8, '%')) "
			+ "ORDER BY branch.id.empArea ASC ")
	Page<BranchConf> listBranch(String empArea, String empSubArea, String address, String empSubAreaDescription,
			String npwp, String latitudeCode, String longitudeCode, String radius, Pageable pageable);

	@Query(value = "SELECT branch FROM com.mega.ws.model.BranchConf branch " 
	+ "WHERE branch.id = ?1")
	BranchConf findBranchById(BranchConfID id);
}
