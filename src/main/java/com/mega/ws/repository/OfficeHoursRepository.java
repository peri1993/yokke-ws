package com.mega.ws.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mega.ws.model.OfficeHours;

public interface OfficeHoursRepository extends JpaRepository<OfficeHours, Long> {

}
