package com.mega.ws.repository;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.mega.ws.model.InstancesLogs;
import com.mega.ws.model.Notifications;

public interface NotificationRepository extends JpaRepository<Notifications, Long> {

	@Query(value = "SELECT notif FROM com.mega.ws.model.Notifications notif " + "WHERE notif.flagRead='N' "
			+ "AND notif.assigner=?1 "
			+ "AND upper(coalesce(notif.businesProcess, '')) LIKE upper(concat('%', ?2, '%')) "
			+ "AND upper(coalesce(notif.stages, '')) LIKE upper(concat('%', ?3, '%')) "
			+ "AND upper(coalesce(notif.description, '')) LIKE upper(concat('%', ?4, '%')) "
			+ "ORDER BY notif.createdDt DESC")
	Page<Notifications> listNotif(String assigner, String paramBusinessProcess, String status, String description,
			Pageable pageable);

	@Query(value = "SELECT notif FROM com.mega.ws.model.Notifications notif " + "WHERE notif.flagRead='N' "
			+ "AND notif.assigner=?1 " 
			+ "AND notif.instancesId IN (?2) " 
			+ "ORDER BY notif.createdDt DESC")
	Page<Notifications> listNotifFilterByInstances(String assigner, List<InstancesLogs> listInstancesLogs, Pageable pageable);

	@Query(value = "SELECT notif FROM com.mega.ws.model.Notifications notif " + "WHERE notif.flagRead='N' "
			+ "AND notif.assigner=?1 " + "AND notif.createdDt BETWEEN ?2 AND ?3 " + "ORDER BY notif.createdDt DESC")
	Page<Notifications> listNotifFilterByDate(String assigner, Timestamp startDt, Timestamp endDt, Pageable pageable);

	@Query(value = "SELECT notif FROM com.mega.ws.model.Notifications notif WHERE notif.flagRead='N' AND notif.assigner=?1 ORDER BY notif.createdDt DESC")
	List<Notifications> listNotifUser(String assigner);

	@Query(value = "SELECT notif FROM com.mega.ws.model.Notifications notif " + "WHERE notif.id=?1 "
			+ "AND notif.flagRead='N' " + "AND notif.assigner=?2 " + "AND notif.instancesId=?3")
	Notifications findByInstancesIdAndAssigner(Long id, String assigner, InstancesLogs instancesId);

	@Query(value = "SELECT notif FROM com.mega.ws.model.Notifications notif " + "WHERE notif.flagRead='N' "
			+ "AND notif.assigner=?1 " + "AND notif.instancesId=?2")
	List<Notifications> listfindByInstancesIdAndAssigner(String assigner, InstancesLogs instancesId);

	@Query(value = "SELECT notif FROM com.mega.ws.model.Notifications notif WHERE notif.instancesId=?1 "
			+ "AND notif.id= (SELECT MAX(id) FROM com.mega.ws.model.Notifications WHERE instancesId=?1)")
	Notifications findByInstancesIdDesc(InstancesLogs instancesId);
	
	@Query(value = "SELECT notif FROM com.mega.ws.model.Notifications notif "
			+ "WHERE notif.instancesId=?1 "
			+ "AND notif.flagRead='N' "
			+ "AND notif.assigner=?2 "
			+ "ORDER BY notif.createdDt DESC")
	Page<Notifications> findByinstancesId(InstancesLogs instances, String assigner, Pageable pageable);
}
