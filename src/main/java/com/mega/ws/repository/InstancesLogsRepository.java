package com.mega.ws.repository;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.mega.ws.model.InstancesLogs;

public interface InstancesLogsRepository extends JpaRepository<InstancesLogs, Long> {

	@Query(value = "SELECT logs FROM com.mega.ws.model.InstancesLogs logs WHERE logs.assigner=?1 "
			+ "AND upper(coalesce(logs.requesterId, '')) LIKE upper(concat('%', ?2, '%')) "
			+ "AND upper(coalesce(logs.requesterName, '')) LIKE upper(concat('%', ?3, '%')) "
			+ "AND upper(coalesce(logs.businessProcess, '')) LIKE upper(concat('%', ?4, '%')) "
			+ "AND upper(coalesce(logs.absencesDesc, '')) LIKE upper(concat('%', ?5, '%')) ORDER BY logs.instancesId DESC")
	Page<InstancesLogs> findByUsername(String username, String requesterId, String paramRequesterName,
			String paramBusinessProcess, String paramAbsencesDesc, Pageable pageable);

	@Query(value = "SELECT logs FROM com.mega.ws.model.InstancesLogs logs WHERE logs.requesterId=?1 "
			+ "AND logs.assigner != null "
			+ "AND upper(coalesce(logs.requesterId, '')) LIKE upper(concat('%', ?2, '%')) "
			+ "AND upper(coalesce(logs.requesterName, '')) LIKE upper(concat('%', ?3, '%')) "
			+ "AND upper(coalesce(logs.businessProcess, '')) LIKE upper(concat('%', ?4, '%')) "
			+ "AND upper(coalesce(logs.absencesType, '')) LIKE upper(concat('%', ?5, '%')) ORDER BY logs.instancesId DESC")
	Page<InstancesLogs> findOutboxByUsername(String username, String requesterId, String paramRequesterName,
			String paramBusinessProcess, String paramAbsencesType, Pageable pageable);

	@Query(value = "SELECT logs FROM com.mega.ws.model.InstancesLogs logs "
			+ "WHERE upper(coalesce(logs.assigner, '')) LIKE upper(concat('%', ?1, '%')) "
			+ "AND upper(coalesce(logs.requesterName, '')) LIKE upper(concat('%', ?2, '%')) "
			+ "AND upper(coalesce(logs.businessProcess, '')) LIKE upper(concat('%', ?3, '%')) "
			+ "AND upper(coalesce(logs.absencesDesc, '')) LIKE upper(concat('%', ?4, '%')) "
			+ "AND logs.assigner != null " )
	Page<InstancesLogs> findByUsernameForAssign(String assigner, String paramRequesterName, String paramBusinessProcess,
			String paramAbsencesDesc, Pageable pageable);

	@Query(value = "SELECT logs FROM com.mega.ws.model.InstancesLogs logs " + "WHERE logs.assigner != null "
			+ "AND ((logs.assigner = logs.approvalDs1 AND upper(coalesce(logs.nameApprovalDs1, '')) LIKE upper(concat('%', ?1, '%'))) "
			+ "OR (logs.assigner = logs.approvalDs2 AND upper(coalesce(logs.nameApprovalDs2, '')) LIKE upper(concat('%', ?1, '%')))) "
			)
	Page<InstancesLogs> findByUsernameForAssignFilterWithAssignerName(String paramAssignerName, Pageable pageable);

	@Query(value = "SELECT logs FROM com.mega.ws.model.InstancesLogs logs " + "WHERE logs.instancesId=?1 "
			+ "AND logs.assigner != null " )
	Page<InstancesLogs> findByUsernameForAssignFilterWithInstancesId(Long paramInstancesId, Pageable pageable);
	
	@Query(value = "SELECT logs FROM com.mega.ws.model.InstancesLogs logs " + "WHERE logs.assigner=?1 "
			+ "AND logs.assigner != null " )
	Page<InstancesLogs> findByUsernameForAssignFilterWithAssigner(String paramAssigner, Pageable pageable);

	@Query(value = "SELECT logs FROM com.mega.ws.model.InstancesLogs logs " + "WHERE logs.requesterId=?1 "
			+ "AND logs.assigner != null " )
	Page<InstancesLogs> findByUsernameForAssignFilterWithRequesterId(String paramRequesterId, Pageable pageable);

	@Query(value = "SELECT logs FROM com.mega.ws.model.InstancesLogs logs " + "WHERE logs.absencesType=?1 "
			+ "AND logs.assigner != null " )
	Page<InstancesLogs> findByUsernameForAssignFilterWithAbsencesType(String paramAbsencesType, Pageable pageable);

	@Query(value = "SELECT logs FROM com.mega.ws.model.InstancesLogs logs WHERE logs.assigner=?1 "
			+ "AND logs.instancesId = ?2 " + "ORDER BY logs.instancesId DESC")
	Page<InstancesLogs> findByInstancesId(String username, Long paramInstancesId, Pageable pageable);

	@Query(value = "SELECT logs FROM com.mega.ws.model.InstancesLogs logs WHERE logs.assigner=?1 AND logs.instancesId=?2 "
			+ "ORDER BY logs.instancesId DESC")
	Page<InstancesLogs> findByTaskId(String username, Long instancesId, Pageable pageable);

	@Query(value = "SELECT logs FROM com.mega.ws.model.InstancesLogs logs " + "WHERE logs.assigner=?1 "
			+ "AND logs.instancesId=?2 " + "AND logs.assigner != null " + "ORDER BY logs.instancesId DESC")
	Page<InstancesLogs> findOuboxByTaskId(String username, Long instancesId, Pageable pageable);

	@Query(value = "SELECT logs FROM com.mega.ws.model.InstancesLogs logs WHERE logs.assigner=?1 "
			+ "AND logs.createdDt BETWEEN ?2 AND ?3 " + "ORDER BY logs.instancesId DESC")
	Page<InstancesLogs> findByDate(String username, Timestamp paramStartDate, Timestamp paramEndDate,
			Pageable pageable);

	@Query(value = "SELECT logs FROM com.mega.ws.model.InstancesLogs logs " + "WHERE logs.assigner=?1 "
			+ "AND logs.startDate >= ?2 " + "AND logs.startDate <= ?3 " + "ORDER BY logs.instancesId DESC")
	Page<InstancesLogs> findByStartDate(String username, String paramStartDate, String paramEndDate, Pageable pageable);

	@Query(value = "SELECT logs FROM com.mega.ws.model.InstancesLogs logs " + "WHERE logs.assigner=?1 "
			+ "AND logs.assigner != null " + "AND logs.startDate >= ?2 " + "AND logs.startDate <= ?3 "
			+ "ORDER BY logs.instancesId DESC")
	Page<InstancesLogs> findOutboxByStartDate(String username, String paramStartDate, String paramEndDate,
			Pageable pageable);

	@Query(value = "SELECT logs FROM com.mega.ws.model.InstancesLogs logs " + "WHERE logs.assigner=?1 "
			+ "AND logs.endDate >= ?2 " + "AND logs.endDate <= ?3 " + "ORDER BY logs.instancesId DESC")
	Page<InstancesLogs> findByEndDate(String username, String paramStartDate, String paramEndDate, Pageable pageable);

	@Query(value = "SELECT logs FROM com.mega.ws.model.InstancesLogs logs " + "WHERE logs.assigner=?1 "
			+ "AND logs.assigner != null " + "AND logs.endDate >= ?2 " + "AND logs.endDate <= ?3 "
			+ "ORDER BY logs.instancesId DESC")
	Page<InstancesLogs> findOutboxByEndDate(String username, String paramStartDate, String paramEndDate,
			Pageable pageable);

	InstancesLogs findByInstancesId(Long instancesId);

	@Query(value = "UPDATE com.mega.ws.model.InstancesLogs SET assigner=?1, createdDt=?2, taskId=?3 WHERE instancesId=?4")
	void updateInstancesIdLogs(String assigner, Timestamp update, Long taskId, Long instancesId);

	@Query(value = "SELECT logs FROM com.mega.ws.model.InstancesLogs logs WHERE logs.instancesId=?1 ")
	InstancesLogs getInstancesLogsByInstancesId(Long instancesId);

	@Query(value = "SELECT logs.requesterId FROM com.mega.ws.model.InstancesLogs logs WHERE logs.instancesId=?1 ")
	String getRequesterIdByInstancesId(Long instancesId);

	@Query(value = "SELECT logs FROM com.mega.ws.model.InstancesLogs logs WHERE logs.requesterId=?1 "
			+ "AND upper(coalesce(logs.businessProcess, '')) LIKE upper(concat('%', ?2, '%')) "
			+ "AND upper(coalesce(logs.absencesDesc, '')) LIKE upper(concat('%', ?3, '%')) "
			+ "AND upper(coalesce(logs.requesterName, '')) LIKE upper(concat('%', ?4, '%')) " )
	Page<InstancesLogs> historyTask(String username, String businessProcess, String paramAbsencesDesc,
			String paramRequesterName, Pageable pageable);

	@Query(value = "SELECT logs FROM com.mega.ws.model.InstancesLogs logs "
			+ "WHERE upper(coalesce(logs.businessProcess, '')) LIKE upper(concat('%', ?1, '%')) "
			+ "AND logs.requesterId=?2 ")
	Page<InstancesLogs> historyTaskFilterWithRequesterId(String businessProcess, String paramRequesterId,
			Pageable pageable);

	@Query(value = "SELECT logs FROM com.mega.ws.model.InstancesLogs logs " + "WHERE logs.instancesId=?1 "
			+ "AND upper(coalesce(logs.businessProcess, '')) LIKE upper(concat('%', ?2, '%')) ")
	Page<InstancesLogs> historyTaskFilterWithInstancesId(Long instancesId, String businessProcess, Pageable pageable);

	@Query(value = "SELECT logs FROM com.mega.ws.model.InstancesLogs logs WHERE logs.requesterId=?1 "
			+ "AND upper(coalesce(logs.businessProcess, '')) LIKE upper(concat('%', ?2, '%')) "
			+ "AND logs.startDate BETWEEN ?3 AND ?4 " )
	Page<InstancesLogs> historyTaskFilterWithLeaveStartDate(String username, String businessProcess,
			String paramStartDate, String paramEndDate, Pageable pageable);

	@Query(value = "SELECT logs FROM com.mega.ws.model.InstancesLogs logs WHERE logs.requesterId=?1 "
			+ "AND upper(coalesce(logs.businessProcess, '')) LIKE upper(concat('%', ?2, '%')) "
			+ "AND logs.endDate BETWEEN ?3 AND ?4 " )
	Page<InstancesLogs> historyTaskFilterWithLeaveEndDate(String username, String businessProcess,
			String paramStartDate, String paramEndDate, Pageable pageable);

	@Query(value = "SELECT logs FROM com.mega.ws.model.InstancesLogs logs "
			+ "WHERE upper(coalesce(logs.subject, '')) LIKE upper(concat('%', ?1, '%')) "
			+ "ORDER BY logs.instancesId DESC")
	List<InstancesLogs> findListInstancesFilterBySubject(String subject);

	@Query(value = "SELECT logs FROM com.mega.ws.model.InstancesLogs logs "
			+ "WHERE upper(coalesce(logs.requesterName, '')) LIKE upper(concat('%', ?1, '%')) "
			+ "ORDER BY logs.instancesId DESC")
	List<InstancesLogs> findListInstancesFilterByRequesterName(String requesterName);

	@Query(value = "SELECT logs FROM com.mega.ws.model.InstancesLogs logs " + "WHERE logs.requesterId = ?1 "
			+ "ORDER BY logs.instancesId DESC")
	List<InstancesLogs> findListInstancesFilterByRequesterID(String requesterId);

	@Query(value = "SELECT logs FROM com.mega.ws.model.InstancesLogs logs " + "WHERE logs.instancesId=?1 ")
	InstancesLogs findListInstancesFilterByNotifInstancesId(Long instancesId);

	@Query(value = "SELECT COUNT(logs) FROM com.mega.ws.model.InstancesLogs logs " + "WHERE logs.assigner=?1 "
			+ "AND logs.assigner != '' " + "AND logs.assigner != null")
	Long countInstancesLogsByAssigner(String username);

	@Query(value = "SELECT COUNT(logs) FROM com.mega.ws.model.InstancesLogs logs " + "WHERE logs.requesterId=?1 "
			+ "AND (logs.assigner != '' OR logs.assigner != null) ")
	Long countInstancesLogsByReqIdOutbox(String username);

	@Query(value = "SELECT COUNT(logs) FROM com.mega.ws.model.InstancesLogs logs " + "WHERE logs.requesterId=?1 "
			+ "AND upper(coalesce(logs.businessProcess, '')) LIKE upper(concat('%', ?2, '%')) " 
			+ "AND (logs.assigner = null OR logs.assigner = '') ")
	Long countInstancesLogsByRequesterId(String username, String businessProcess);
	
	@Query(value = "SELECT COUNT(logs) FROM com.mega.ws.model.InstancesLogs logs " + "WHERE logs.requesterId=?1 "
			+ " AND (logs.assigner = null OR logs.assigner = '') ")
	Long countInstancesLogsByReqIdHistory(String username);

	@Query(value = "SELECT COUNT(logs) FROM com.mega.ws.model.InstancesLogs logs " + "WHERE logs.requesterId=?1 ")
	Long countInstancesLogsByRequesterIdAll(String username);

	@Query(value = "SELECT logs FROM com.mega.ws.model.InstancesLogs logs " + "WHERE logs.requesterId=?1 "
			+ "AND upper(coalesce(logs.absencesDesc, '')) LIKE upper(concat('%', ?2, '%')) "
			+ "AND upper(coalesce(logs.requesterId, '')) LIKE upper(concat('%', ?3, '%')) "
			+ "AND upper(coalesce(logs.requesterName, '')) LIKE upper(concat('%', ?4, '%')) "
			+ "AND logs.assigner = null " + "ORDER BY logs.instancesId DESC" )
	Page<InstancesLogs> historyTaskAll(String username, String paramAbsencesDesc, String paramRequesterId,
			String paramRequesterName, Pageable pageable);

	@Query(value = "SELECT logs FROM com.mega.ws.model.InstancesLogs logs " + "WHERE logs.instancesId=?1 "
			+ "AND upper(coalesce(logs.businessProcess, '')) LIKE upper(concat('%', ?2, '%')) "
			+ "AND logs.assigner = null " 
			+ "ORDER BY logs.instancesId DESC")
	Page<InstancesLogs> historyTaskFilterWithInstancesIdForMobile(Long instancesId, String businessProcess,
			Pageable pageable);

	@Query(value = "SELECT logs FROM com.mega.ws.model.InstancesLogs logs "
			+ "WHERE upper(coalesce(logs.businessProcess, '')) LIKE upper(concat('%', ?1, '%')) "
			+ "AND logs.requesterId=?2 " 
			+ "AND logs.assigner = null " 
			+ "ORDER BY logs.instancesId DESC")
	Page<InstancesLogs> historyTaskFilterWithRequesterIdForMobile(String businessProcess, String paramRequesterId,
			Pageable pageable);

	@Query(value = "SELECT logs FROM com.mega.ws.model.InstancesLogs logs WHERE logs.requesterId=?1 "
			+ "AND upper(coalesce(logs.businessProcess, '')) LIKE upper(concat('%', ?2, '%')) "
			+ "AND logs.startDate BETWEEN ?3 AND ?4 " 
			+ "AND logs.assigner = null " 
			+ "ORDER BY logs.instancesId DESC")
	Page<InstancesLogs> historyTaskFilterWithLeaveStartDateForMobile(String username, String businessProcess,
			String paramStartDate, String paramEndDate, Pageable pageable);

	@Query(value = "SELECT logs FROM com.mega.ws.model.InstancesLogs logs WHERE logs.requesterId=?1 "
			+ "AND upper(coalesce(logs.businessProcess, '')) LIKE upper(concat('%', ?2, '%')) "
			+ "AND logs.endDate BETWEEN ?3 AND ?4 " 
			+ "AND logs.assigner = null " 
			+ "ORDER BY logs.instancesId DESC")
	Page<InstancesLogs> historyTaskFilterWithLeaveEndDateForMobile(String username, String businessProcess,
			String paramStartDate, String paramEndDate, Pageable pageable);

	@Query(value = "SELECT logs FROM com.mega.ws.model.InstancesLogs logs WHERE logs.requesterId=?1 "
			+ "AND upper(coalesce(logs.businessProcess, '')) LIKE upper(concat('%', ?2, '%')) "
			+ "AND upper(coalesce(logs.absencesDesc, '')) LIKE upper(concat('%', ?3, '%')) "
			+ "AND upper(coalesce(logs.requesterName, '')) LIKE upper(concat('%', ?4, '%')) "
			+ "AND logs.assigner = null " 
			+ "ORDER BY logs.instancesId DESC")
	Page<InstancesLogs> historyTaskForMobile(String username, String businessProcess, String paramAbsencesDesc,
			String paramRequesterName, Pageable pageable);
	
	@Query(value = "SELECT logs FROM com.mega.ws.model.InstancesLogs logs " + "WHERE logs.instancesId=?1 "
			+ "AND logs.assigner != null " + "ORDER BY ?2")
	Page<InstancesLogs> findByUsernameForAssignFilterWithInstancesIdOrderByDynamic(Long paramInstancesId, String orderBy, Pageable pageable);
	
	@Query(value = "SELECT logs FROM com.mega.ws.model.InstancesLogs logs " + "WHERE logs.assigner=?1 "
			+ "AND logs.assigner != null " + "ORDER BY ?2")
	Page<InstancesLogs> findByUsernameForAssignFilterWithAssignerOrderByDinamic(String paramAssigner, String orderBy, Pageable pageable);
	
	@Query(value = "SELECT logs FROM com.mega.ws.model.InstancesLogs logs " + "WHERE logs.requesterId=?1 "
			+ "AND logs.assigner != null " + "ORDER BY ?2")
	Page<InstancesLogs> findByUsernameForAssignFilterWithRequesterIdOrderByDynamic(String paramRequesterId, String orderBy, Pageable pageable);
	
	@Query(value = "SELECT logs FROM com.mega.ws.model.InstancesLogs logs " + "WHERE logs.absencesType=?1 "
			+ "AND logs.assigner != null " + "ORDER BY ?2")
	Page<InstancesLogs> findByUsernameForAssignFilterWithAbsencesTypeOrderByDynamic(String paramAbsencesType, String orderBy, Pageable pageable);
	
	@Query(value = "SELECT logs FROM com.mega.ws.model.InstancesLogs logs " + "WHERE logs.assigner != null "
			+ "AND ((logs.assigner = logs.approvalDs1 AND upper(coalesce(logs.nameApprovalDs1, '')) LIKE upper(concat('%', ?1, '%'))) "
			+ "OR (logs.assigner = logs.approvalDs2 AND upper(coalesce(logs.nameApprovalDs2, '')) LIKE upper(concat('%', ?1, '%')))) "
			+ "ORDER BY ?2")
	Page<InstancesLogs> findByUsernameForAssignFilterWithAssignerNameOrderByDynamic(String paramAssignerName, String orderBy, Pageable pageable);
	
	@Query(value = "SELECT logs FROM com.mega.ws.model.InstancesLogs logs "
			+ "WHERE upper(coalesce(logs.assigner, '')) LIKE upper(concat('%', ?1, '%')) "
			+ "AND upper(coalesce(logs.requesterName, '')) LIKE upper(concat('%', ?2, '%')) "
			+ "AND upper(coalesce(logs.businessProcess, '')) LIKE upper(concat('%', ?3, '%')) "
			+ "AND upper(coalesce(logs.absencesDesc, '')) LIKE upper(concat('%', ?4, '%')) "
			+ "AND logs.assigner != null " + "ORDER BY ?5")
	Page<InstancesLogs> findByUsernameForAssignOrderByDynamic(String assigner, String paramRequesterName, String paramBusinessProcess,
			String paramAbsencesDesc, String orderBy, Pageable pageable);
	
	@Query(value = "SELECT logs FROM com.mega.ws.model.InstancesLogs logs "
			+ "WHERE logs.requesterId=?1 "
			+ "AND logs.assigner != '' "
			+ "AND (logs.absencesType = '1000' "
			+ "OR logs.absencesType = '3100') ")
	List<InstancesLogs> getListInstanceLogsDataDate(String requesterId);
	
	@Query(value = "SELECT logs FROM com.mega.ws.model.InstancesLogs logs "
			+ "WHERE logs.instancesId=?1 "
			+ "AND logs.assigner = ?2 ")
	List<InstancesLogs> checkAssignerTask(Long instancesId, String assigner);
	
	@Query(value = "SELECT logs FROM com.mega.ws.model.InstancesLogs logs "
			+ "WHERE logs.requesterId=?1 "
			+ "AND logs.startDate >= ?2 "
			+ "AND logs.endDate <= ?3 "
			+ "AND (logs.assigner != '') "
			+ "AND (logs.absencesType = '1000' or logs.absencesType = '3100' ) ")
	List<InstancesLogs> findDateDeduction(String requesterId, String startDt, String endDt);
	
}
