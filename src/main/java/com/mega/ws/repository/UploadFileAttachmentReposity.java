package com.mega.ws.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.mega.ws.model.FIleAttachment;

public interface UploadFileAttachmentReposity extends JpaRepository<FIleAttachment, Long> {

	@Query(value = "SELECT file FROM com.mega.ws.model.FIleAttachment file "
			+ "WHERE file.fileName=?1 "
			+ "AND file.id=(SELECT MAX(id) FROM com.mega.ws.model.FIleAttachment WHERE fileName=?1)")
	FIleAttachment findByFileName(String filename);
	
}
