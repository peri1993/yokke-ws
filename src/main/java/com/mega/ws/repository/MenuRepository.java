package com.mega.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.mega.ws.model.Menu;
import com.mega.ws.model.Role;

public interface MenuRepository extends JpaRepository<Menu, Long>{
	
	@Query(name = "SELECT menu FROM com.mega.ws.model.Menu menu WHERE menu.role=?1")
	List<Menu> findAllMenuByRole(Role role);

}
