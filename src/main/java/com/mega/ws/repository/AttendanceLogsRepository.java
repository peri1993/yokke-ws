package com.mega.ws.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.mega.ws.identity.AttendanceLogsID;
import com.mega.ws.model.AttendanceLogs;

public interface AttendanceLogsRepository extends JpaRepository<AttendanceLogs, AttendanceLogsID> {

	@Query(value = " SELECT logs FROM com.mega.ws.model.AttendanceLogs logs ")
	AttendanceLogs findListLogsAttendance();

	@Query(value = " SELECT logs FROM com.mega.ws.model.AttendanceLogs logs " + "WHERE logs.id.empId = ?1 "
			+ "AND logs.id.startDate = ?2 " + "AND logs.id.endDate = ?2 ")
	AttendanceLogs validationTime(String empId, String startDate);

	@Query(value = " SELECT logs FROM com.mega.ws.model.AttendanceLogs logs " + "WHERE logs.id.empId = ?1 ")
	Page<AttendanceLogs> listEmpAttendance(String username, Pageable pageable);

	@Query(value = " SELECT logs FROM com.mega.ws.model.AttendanceLogs logs " + "WHERE logs.id.empId = ?1 "
			+ "AND logs.id.startDate = ?2 ")
	AttendanceLogs listEmpAttendanceByDate(String username, String startDate);

	@Query(value = " SELECT logs FROM com.mega.ws.model.AttendanceLogs logs " + "WHERE logs.id.empId = ?1 "
			+ "AND logs.id.startDate >= ?2 " + "AND logs.id.endDate <= ?3 ")
	Page<AttendanceLogs> listHistoryAttendanceEmployee(String username, String startDt, String endDt,
			Pageable pageable);
}
