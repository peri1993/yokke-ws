package com.mega.ws.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.mega.ws.model.Role;

public interface RoleRepository extends JpaRepository<Role, Long>{

	@Query(value = "SELECT role FROM com.mega.ws.model.Role role "
			+ "WHERE upper(coalesce(role.roleName, '')) LIKE upper(concat('%', ?1, '%')) "
			+ "AND upper(coalesce(role.createdBy, '')) LIKE upper(concat('%', ?2, '%')) "
			+ "ORDER BY role.role_id ASC")
	Page<Role> findById(String roleName, String createdBy, Pageable pageable);
	
	Role findByRoleName(String roleName);
	
	@Query(value = "UPDATE com.mega.ws.model.Role SET roleName=?2 "
			+ "WHERE role_id=?1")
	void update(Long roleId, String rolename);
	
	@Query(value = "SELECT role FROM com.mega.ws.model.Role role "
			+ "WHERE upper(coalesce(role.roleName, '')) LIKE upper(concat('%', ?1, '%')) "
			+ "ORDER BY role.role_id DESC")
	List<Role> listByRoleName(String roleName);
}
