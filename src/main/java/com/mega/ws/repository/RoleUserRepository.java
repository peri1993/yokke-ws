package com.mega.ws.repository;

import java.sql.Timestamp;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.mega.ws.model.RoleUser;

public interface RoleUserRepository extends JpaRepository<RoleUser, Long> {

	@Query(value = "SELECT user FROM com.mega.ws.model.RoleUser user "
			+ "WHERE upper(coalesce(user.username, '')) LIKE upper(concat('%', ?1, '%')) "
			+ "AND upper(coalesce(user.fullname, '')) LIKE upper(concat('%', ?2, '%')) "
			+ "AND upper(coalesce(user.createdBy, '')) LIKE upper(concat('%', ?3, '%')) "
			+ "AND upper(coalesce(user.updatedBy, '')) LIKE upper(concat('%', ?4, '%')) " + "ORDER BY user.id DESC")
	Page<RoleUser> findById(String username, String fullname, String createdBy, String updatedBy, Pageable pageable);

	@Query(value = "SELECT user FROM com.mega.ws.model.RoleUser user " + "WHERE user.createdDt BETWEEN ?1 AND ?2 "
			+ "ORDER BY user.id DESC")
	Page<RoleUser> findByCreatedDt(Timestamp startDate, Timestamp endDate, Pageable pageable);

	@Query(value = "SELECT user FROM com.mega.ws.model.RoleUser user " + "WHERE user.updatedDt BETWEEN ?1 AND ?2 "
			+ "ORDER BY user.id DESC")
	Page<RoleUser> findByUpdatedDt(Timestamp startDate, Timestamp endDate, Pageable pageable);

	@Query(value = "SELECT user FROM com.mega.ws.model.RoleUser user "
			+ "WHERE upper(coalesce(user.role.roleName, '')) LIKE upper(concat('%', ?1, '%')) "
			+ "ORDER BY user.id DESC")
	Page<RoleUser> listByRoleName(String roleName, Pageable pageable);

	RoleUser findByUsername(String username);

	RoleUser findById(Long userRoleId);

}
