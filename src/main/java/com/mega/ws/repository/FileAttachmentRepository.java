package com.mega.ws.repository;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.mega.ws.model.FIleAttachment;

public interface FileAttachmentRepository  extends JpaRepository<FIleAttachment, Long>{
	
	@Query(value = "SELECT file FROM com.mega.ws.model.FIleAttachment file "
			+ "WHERE file.moduleName = 'jadwal' "
			+ "AND file.createdBy = ?1 "
			+ "AND file.createdDt BETWEEN ?2 AND ?3 ")
	List<FIleAttachment> listFileFromChange(String empId, Timestamp startDt, Timestamp endDt);
	
	@Query(value = "SELECT file.fileType FROM com.mega.ws.model.FIleAttachment file "
			+ "WHERE file.fileName = ?1 "
			+ "AND file.status = ?2 ")
	String findFileType(String filename, String status);
	

}
