package com.mega.ws.controller;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.mega.ws.constants.ModuleConstants;
import com.mega.ws.jbpm.MappingURL;
import com.mega.ws.request.HistoryDateRequest;
import com.mega.ws.request.LeaveRequest;
import com.mega.ws.request.LeaveStatusRequest;
import com.mega.ws.response.JBPM_MappingResponse;
import com.mega.ws.util.HeaderSettings;

@RestController
public class LeaveController extends BaseController {

	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(LeaveController.class);

	@Autowired
	private MappingURL mappingURL;

	@Autowired
	private HeaderSettings headerSettings;

	private RestTemplate restTemplate = new RestTemplate();

	private HistoryDateRequest setModel(LeaveRequest request) {
		HistoryDateRequest model = new HistoryDateRequest();
		model.setUsername(request.getEmployee_id());
		model.setAbsencesType(request.getAbsences_type());
		model.setStartDate(request.getLeave_start_date());
		model.setEndDate(request.getLeave_end_date());
		return model;
	}
	
	private Boolean validationApprovalNotEmpty(LeaveRequest request) {

		Boolean flag = false;

		try {
			if ("".equals(request.getApproval_ds_1()) || null == request.getApproval_ds_1()) {
				return true;
			}
			
			if("".equals(request.getApproval_ds_1()) || null == request.getApproval_ds_1()) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}

	@SuppressWarnings({ "unchecked", "rawtypes", "unused" })
	@PostMapping("/create-leave")
	@ResponseBody
	public Map<String, Object> createLeave(@RequestBody LeaveRequest request) {
		request.setModuleName(ModuleConstants.LEAVE_MODULE);
		
		Map<String, Object> map = new HashMap<String, Object>();
		Map<String, Object> resp = new HashMap<String, Object>();
		Map<String, Object> validationAbsenceType = new HashMap<String, Object>();
		
		// validation approval
		if (validationApprovalNotEmpty(request)) {
			map.put("message", "Anda Tidak Bisa mengajukan cuti karena Approval Anda Tidak ditemukan !!!");
			map.put("status", 400);

			return map;			
		}
		
		Long remain = new Long(0);
		if ("".equals(request.getRemaining_quota())) {
			remain = new Long(0);
		}else {
			remain = new Long(request.getRemaining_quota());
		}
		
		Long quotaTaken = new Long(request.getQuota_taken());
		
		
		// validation for date request
		if ("1000".equals(request.getAbsences_type()) || "3100".equals(request.getAbsences_type())) {
			Map<String, Object> mapDate = new HashMap<String, Object>();
			mapDate = validationDateRequest(request.getEmployee_id(), cekTotalDate(request.getLeave_start_date(), request.getLeave_end_date()));
			if ("400".equals(String.valueOf(mapDate.get("status")))) {
				mapDate.remove("flagRequest");
				return mapDate;
			}
		}

		// validationDeduction
		if (validationCheckDeduction(request) == false) {
				map.put("message", "Username anda berada diluar deduction, hubungi admin untuk info lebih lanjut");
				map.put("status", 400);

				return map;
		}

		/*
		 * // validation -quota if (remain < quotaTaken) { map.put("message",
		 * "Anda tidak memiliki quota yang cukup untuk mengambil Cuti");
		 * map.put("status", 400);
		 * 
		 * return map; }
		 */

		// validation history
		HistoryDateRequest historyDateRequest = setModel(request);
		boolean validateDate = validationFindDateHistory(historyDateRequest);
		if (validateDate) {
			map.put("message", "Anda sudah mengambil Cuti pada tanggal ini");
			map.put("status", 400);

			return map;
		}

		validationAbsenceType = validateAbsencesType(request.getAbsences_type(), request.getTotal_taken_quota(),
				request.getEmployee_id());
		boolean flag = (boolean) validationAbsenceType.get("flagAbsence");
		if (flag && !"Y".equals(request.getCarryOverFlag())) {
			validationAbsenceType.remove("flagAbsence");
			return validationAbsenceType;
		}

		if ("1000".equals(request.getAbsences_type()) && !"Y".equals(request.getCarryOverFlag())) {
			resp = validationBlockLeave(request.getEmployee_id(), request.getRemaining_quota(),
					request.getTotal_taken_quota());
			boolean checkBlockLeave = (boolean) resp.get("blockLeave");
			if (checkBlockLeave) {
				resp.remove("blockLeave");
				return resp;
			}
		}

		// validation absencetype 3150
		if (validationAbsencesType(request) == true) {
			map.put("message", request.getAbsenceDescription() + " hanya bisa di ambil satu kali");
			map.put("status", 400);

			return map;
		}
		
		JBPM_MappingResponse mapping = mappingURL.initiateLeaveRequest();

		HttpEntity<LeaveRequest> requestEntity = new HttpEntity<>(request, headerSettings.setupHeader());
		logger.info("BEGIN CALL JBPM");
		logger.info(mapping.getUrl());
		
		try {
			Object response = restTemplate.exchange(mapping.getUrl(), mapping.getMethod(), requestEntity, Object.class);
			ResponseEntity<Object> responseEntity = (ResponseEntity<Object>) response;
			map.put("processId", responseEntity.getBody());

			Integer processId = (Integer) map.get("processId");
			JBPM_MappingResponse activeTask = mappingURL.showActiveTaskByProcessId(processId);

			ResponseEntity<HashMap> responseActiveTask = restTemplate.exchange(activeTask.getUrl(),
					activeTask.getMethod(), null, HashMap.class);
			Integer activeTaskId = findTaskId(responseActiveTask);
			map.put("taskId", activeTaskId);

			LeaveStatusRequest status = new LeaveStatusRequest();
			status.setLeave_status("approve");

			HttpEntity<LeaveStatusRequest> completeEntity = new HttpEntity<>(status,
					headerSettings.setupHeaderContentType());
			JBPM_MappingResponse completeTask = mappingURL.completedTask(activeTaskId);

			UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(completeTask.getUrl())
					.queryParam("user", request.getEmployee_id()).queryParam("auto-progress", "true");

			restTemplate.exchange(builder.buildAndExpand(completeTask.getUrl()).toUri(), completeTask.getMethod(),
					completeEntity, Object.class);
			logsSave(request, processId, activeTaskId);

			map.put("message", "Success");
			map.put("status", 200);

		} catch (Exception e) {
			map.put("error", e.getMessage());
			map.put("status", 400);
		}
		return map;
	}

}
