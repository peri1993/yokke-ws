package com.mega.ws.controller;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.mega.ws.model.AttendanceLogs;
import com.mega.ws.model.ScheduleAttendance;
import com.mega.ws.request.AttendanceLogsIdRequest;
import com.mega.ws.request.AttendanceRequest;
import com.mega.ws.request.FileNameRequest;
import com.mega.ws.request.ScheduleAttendanceLogsIdRequest;
import com.mega.ws.request.ScheduleAttendanceLogsRequest;
import com.mega.ws.request.UploadFileAttendanceRequest;
import com.mega.ws.response.AttendanceLogsResponse;
import com.mega.ws.response.AttendanceResponse;
import com.mega.ws.response.JBPM_MappingResponse;
import com.mega.ws.response.PageResponse;
import com.mega.ws.response.PropertiesResponse;
import com.mega.ws.service.FileAttachmentService;
import com.mega.ws.service.UploadFileService;
import com.mega.ws.service.dto.AttendanceLogsDTO;
import com.mega.ws.service.dto.ScheduleAttendanceDTO;
import com.mega.ws.service.mapper.BaseMapper;
import com.mega.ws.util.QrHelperSettings;

@RestController
public class AttendanceController extends BaseController {

	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(AttendanceController.class);

	@Autowired
	private UploadFileService service;
	
	@Autowired
	private FileAttachmentService fileAttachmentService;

	@Autowired
	private BaseMapper baseMapper;

	@Autowired
	private BranchConfController brnchController;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@GetMapping("/find-list-under/{username}")
	@ResponseBody
	private Map<String, Object> findListUnderEmp(@PathVariable String username,
			@RequestParam(name = "fullName", defaultValue = "") String paramFullName,
			@RequestParam(name = "empId", defaultValue = "") String paramEmpId,
			@RequestParam(name = "ktp", defaultValue = "") String paramKtp,
			@RequestParam(name = "birthPlace", defaultValue = "") String paramBirthPlace,
			@RequestParam(name = "nationality", defaultValue = "") String paramNationality) {
		List<AttendanceResponse> listResponse = new ArrayList<AttendanceResponse>();

		Map<String, Object> resp = new HashMap<String, Object>();

		HttpEntity<Object> entity = new HttpEntity<>(headerSettings.setupHeaderAccept());
		JBPM_MappingResponse mapping = mappingURL.findListUnder(username);

		try {
			Map<String, Object> map = flagParam(paramFullName, paramEmpId, paramKtp, paramBirthPlace, paramNationality);
			Object response = null;
			if ("Y".equals(map.get("flag"))) {
				UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(mapping.getUrl())
						.queryParam("key", String.valueOf(map.get("key")))
						.queryParam("value", String.valueOf(map.get("value")));
				response = restTemplate.exchange(builder.buildAndExpand(mapping.getUrl()).toUri(), mapping.getMethod(),
						entity, Object.class);
			} else {
				response = restTemplate.exchange(mapping.getUrl(), mapping.getMethod(), entity, Object.class);
			}

			ResponseEntity<HashMap> responseEntity = (ResponseEntity<HashMap>) response;
			List<Object> listUnderEmployee = (List<Object>) responseEntity.getBody().get("listResponse");
			for (int i = 0; i < listUnderEmployee.size(); i++) {
				LinkedHashMap<String, Object> linkModel = (LinkedHashMap<String, Object>) listUnderEmployee.get(i);
				LinkedHashMap<String, Object> linkId = (LinkedHashMap<String, Object>) linkModel.get("id");

				String fullname = String.valueOf(linkModel.get("fullName"));
				String startDate = String.valueOf(linkId.get("startDate"));
				String empId = String.valueOf(linkId.get("empId"));
				String birthPlace = String.valueOf(linkModel.get("birthPlace"));
				String nationality = String.valueOf(linkModel.get("nationality"));
				String ktp = String.valueOf(linkModel.get("ktp"));

				AttendanceResponse model = new AttendanceResponse();
				model.setEmpId(empId);
				model.setFullName(fullname);
				model.setStartDate(startDate);
				model.setKtp(ktp);
				model.setNationality(nationality);
				model.setBirthPlace(birthPlace);
				listResponse.add(model);
			}

			resp.put("status", 200);
			resp.put("listResponse", listResponse);
			resp.put("message", "Success");

		} catch (Exception e) {
			e.printStackTrace();
			resp.put("error", 400);
			resp.put("message", e.getMessage());
		}

		return resp;
	}

	private Map<String, Object> flagParam(String paramFullName, String paramEmpId, String paramKtp,
			String paramBirthPlace, String paramNationality) {

		Map<String, Object> map = new HashMap<String, Object>();
		if (paramFullName != null && !"".equals(paramFullName)) {
			map.put("key", "fullname");
			map.put("value", paramFullName);
			map.put("flag", "Y");
		} else if (paramEmpId != null && !"".equals(paramEmpId)) {
			map.put("key", "empId");
			map.put("value", paramEmpId);
			map.put("flag", "Y");
		} else if (paramKtp != null && !"".equals(paramKtp)) {
			map.put("key", "ktp");
			map.put("value", paramKtp);
			map.put("flag", "Y");
		} else if (paramBirthPlace != null && !"".equals(paramBirthPlace)) {
			map.put("key", "birthPlace");
			map.put("value", paramBirthPlace);
			map.put("flag", "Y");
		} else if (paramNationality != null && !"".equals(paramNationality)) {
			map.put("key", "nationality");
			map.put("value", paramNationality);
			map.put("flag", "Y");
		} else {
			map.put("flag", "N");
		}
		return map;
	}

	@GetMapping(value = "/QR-Code/{empArea}/{empSubArea}/{landscape}/{longitude}/{latitude}/v1")
	@ResponseBody
	public Map<String, Object> qrcode(@PathVariable String empArea, @PathVariable String empSubArea,
			@PathVariable String landscape, @PathVariable String longitude, @PathVariable String latitude,
			HttpServletResponse response) {
		Map<String, Object> map = new HashMap<String, Object>();
		String pattern = "HHmmss";
		String today = null;
		try {
			Calendar date = Calendar.getInstance();
			DateFormat format = new SimpleDateFormat(pattern);
			today = format.format(date.getTime());

			response.setContentType("image/png");
			OutputStream outputStream = response.getOutputStream();
			outputStream.write(QrHelperSettings.getQrCodeImage(
					empArea + '|' + empSubArea + '|' + landscape + '|' + longitude + '|' + latitude + '|' + today, 200,
					200));
			outputStream.flush();
			outputStream.close();
			map.put("qrcode", outputStream);
		} catch (Exception e) {
			e.printStackTrace();
		}

		map.put("status", 200);
		map.put("message", "SUCCESS");
		map.put("codeQR",
				empArea + '|' + empSubArea + '|' + landscape + '|' + longitude + '|' + latitude + '|' + today);
		return map;
	}

	@PostMapping(value = "/create-attendance")
	@ResponseBody
	public Map<String, Object> createAttendance(@RequestBody AttendanceRequest request) {
		Map<String, Object> map = new HashMap<String, Object>();
		
		if(request.getBarcodeTime() != null) {
			if(validateById(request) == false) {
				map.put("message", "ID anda tidak terdaftar di cabang ini");
				map.put("status", 400);
				return map;
			}
			
			if (validationBarcode(request) == false) {
				map.put("message", "Barcode tidak valid !");
				map.put("status", 400);
				return map;
			}

			if (validateDistance(request) == false) {
				map.put("message", "Lokasi anda berada di luar jangkauan");
				map.put("status", 400);
				return map;
			}
		}

		try {
			if (null != request) {
				AttendanceLogs model = attendanceLogsService.validateById(request.getId().getEmpId(), getDate());
				update(request);
				if (model == null) {
					if (request.getPathImages() != null) {
						FileNameRequest file = new FileNameRequest();
						String filename = request.getPathImages().getCreatedBy().concat("-")
								.concat(baseMapper.formateTimestamp(getNow()));
						file.setFilename(filename);
						request.getPathImages().setCreatedBy(request.getId().getEmpId());
						request.getPathImages().setCreatedDt(getNow());
						request.getPathImages().setDirectory(getPaths());
						request.getPathImages().setFileName(file.getFilename());
						request.getPathImages().setStatus("In");
						uploadfileMobile(request.getPathImages());
						//uploadfileSelfieRezice(request.getPathImages());
						uploadPaths(request.getPathImages());
					}
					AttendanceLogsIdRequest req = new AttendanceLogsIdRequest();
					req.setEmpId(request.getId().getEmpId());
					req.setStartDate(getDate());
					req.setEndDate(getDate());
					req.setTime(getNow());
					req.setFullName(findName(request.getId().getEmpId()));
					attendanceLogsService.createAttendances(request, req);
					brnchController.findListBranchById(req.getEmpId());
					map.put("message", "Absen anda berhasil, Selamat Datang !");
					map.put("status", 200);
				} else {
					update(request);
					if (request.getPathImages() != null) {
						FileNameRequest file = new FileNameRequest();
						String filename = request.getPathImages().getCreatedBy().concat("-")
								.concat(baseMapper.formateTimestamp(getNow()));
						file.setFilename(filename);
						request.getPathImages().setCreatedBy(request.getId().getEmpId());
						request.getPathImages().setCreatedDt(getNow());
						request.getPathImages().setDirectory(getPaths());
						request.getPathImages().setFileName(file.getFilename());
						request.getPathImages().setStatus("Out");
						uploadfileMobile(request.getPathImages());
						//uploadfileSelfieRezice(request.getPathImages());
						uploadPaths(request.getPathImages());
					}
					AttendanceLogsIdRequest req = new AttendanceLogsIdRequest();
					req.setEmpId(request.getId().getEmpId());
					req.setStartDate(getDate());
					req.setEndDate(getDate());
					req.setTime(getNow());
					req.setFullName(findName(request.getId().getEmpId()));
					attendanceLogsService.createAttendances(request, req);
					map.put("message", "Absen anda berhasil, Hati-hati dijalan !");
					map.put("status", 200);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			map.put("status", 400);
			map.put("message", e.getMessage());
		}

		return map;
	}

	private void update(AttendanceRequest request) {

		JBPM_MappingResponse mapping = mappingURL.createAttendance();
		HttpEntity<AttendanceRequest> requestEntity = new HttpEntity<>(request, headerSettings.setupHeaderAccept());
		restTemplate.exchange(mapping.getUrl(), mapping.getMethod(), requestEntity, Object.class);

	}

	@PostMapping(value = "/download-files-attendance")
	@ResponseBody
	public Map<String, Object> downloadFiles(@RequestBody UploadFileAttendanceRequest file) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String path = null;
			String a[] = file.getDirectory().split(uploadpathconfig.getLocation_document());
			if (a[1] != null) {
				path = uploadpathconfig.getPath_file().concat(a[1]).concat("/")
						.concat(file.getFileName().concat("-").concat(file.getStatus()));
			}
			map.put("status", 200);
			map.put("path", path);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("status", 400);
			map.put("message", e.getMessage());
		}
		return map;
	}

	@GetMapping("/download-images-attendance")
	@ResponseBody
	public ResponseEntity<byte[]> downloadImages(@RequestParam String directory, @RequestParam String filename,
			@RequestParam String type) {

		ResponseEntity<byte[]> rsp = new ResponseEntity<byte[]>(HttpStatus.BAD_REQUEST);

		try {
			// download file in temp folder
			Path pat = Paths.get(String.valueOf(directory + File.separator + filename));
			byte[] contents = Files.readAllBytes(pat);

			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.setContentType(MediaType.parseMediaType(type));
			responseHeaders.set("Content-Disposition", "attachment");
			responseHeaders.setContentDispositionFormData(filename, filename);
			responseHeaders.setCacheControl("must-revalidate, post-check=0, pre-check=0");
			rsp = new ResponseEntity<byte[]>(contents, responseHeaders, HttpStatus.OK);

		} catch (Exception e) {
			e.printStackTrace();
			e.getMessage();
		}
		return rsp;
	}

	@PostMapping(value = "/upload-mobile-file")
	@ResponseBody
	public Map<String, Object> uploadfileMobile(@RequestBody UploadFileAttendanceRequest request)
			throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {

		Map<String, Object> map = new HashMap<String, Object>();
		List<String> listFilename = new ArrayList<String>();
		try {
			byte[] bytePhoto = DatatypeConverter.parseBase64Binary(request.getContent());
			BufferedImage image = ImageIO.read(new ByteArrayInputStream(bytePhoto));

			String filename = request.getCreatedBy().concat("-").concat(formateTimestamp(getNow())).concat("-")
					.concat(request.getStatus());
			Path path = Paths.get(getPaths() + File.separator + filename);
			if (!Files.exists(path)) {
				Files.createDirectories(path);
				File file = new File(getPaths() + File.separator + filename);
				ImageIO.write(image, "PNG", file);
				request.setFileSize(String.valueOf(file.length()));
			} else {
				Files.deleteIfExists(path);
				Files.createDirectories(path);
				File file = new File(getPaths() + File.separator + filename);
				ImageIO.write(image, "PNG", file);
				request.setFileSize(String.valueOf(file.length()));
			}
			listFilename.add(filename);
			map.put("status", 200);
			map.put("message", "Upload success");
			map.put("filename", listFilename);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("status", 400);
			map.put("message", e.getMessage());
		}
		return map;
	}

	@PostMapping(value = "/create-schedule-attendance")
	@ResponseBody
	public Map<String, Object> createScheduleAttendance(@RequestBody List<ScheduleAttendanceLogsRequest> request) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			for (int i = 0; i < request.size(); i++) {
				ScheduleAttendanceLogsIdRequest req = new ScheduleAttendanceLogsIdRequest();
				req.setEmpId(request.get(i).getId().getEmpId());
				req.setStartDate(request.get(i).getId().getStartDate());
				req.setEndDate(request.get(i).getId().getEndDate());
				attendanceLogsService.createScheduleAttendance(request.get(i), req);
			}
			map.put("status", 200);
			map.put("message", "Success insert jadwal karyawan");
		} catch (Exception e) {
			e.printStackTrace();
			map.put("status", 400);
			map.put("message", e.getMessage());
		}
		return map;
	}

	@GetMapping(value = "/list-schedule-attendance/{empId}")
	@ResponseBody
	public PageResponse<ScheduleAttendanceDTO> listAttendance(Pageable pageable, @PathVariable String empId) {

		try {
			Page<ScheduleAttendanceDTO> results = attendanceLogsService.listAttendance(empId, pageable);
			return new PageResponse<>(PropertiesResponse.STATUS_MESSAGE_OK, PropertiesResponse.STATUS_CODE_OK,
					results.getContent(), results.getNumber(), results.getSize(), results.getTotalPages(),
					results.isLast(), new Long(0));
		} catch (Exception e) {
			e.printStackTrace();

			List<ScheduleAttendanceDTO> list = new ArrayList<ScheduleAttendanceDTO>();
			Page<ScheduleAttendanceDTO> model = new PageImpl<ScheduleAttendanceDTO>(list, pageable, 0);

			return new PageResponse<>(PropertiesResponse.INPUT_ERROR_NUMERIC,
					PropertiesResponse.STATUS_CODE_CLIENT_ERROR, model.getContent(), model.getNumber(), model.getSize(),
					model.getTotalPages(), model.isLast(), new Long(0));
		}
	}

	@PostMapping(value = "/delete-schedule-attendance")
	@ResponseBody
	public Map<String, Object> deleteScheduleEmployee(@RequestBody ScheduleAttendanceLogsRequest request) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			if (request != null) {
				attendanceLogsService.deleteScheduleAttendance(request);
				map.put("status", 200);
				map.put("message", "Sukses delete data pegawai");
			} else {
				map.put("status", 200);
				map.put("message", "Tidak ada data yang berubah");
			}
		} catch (Exception e) {
			e.printStackTrace();
			map.put("status", 400);
			map.put("message", e.getMessage());
		}
		return map;
	}

	@GetMapping(value = "/list-history-attendance/{username}")
	@ResponseBody
	public PageResponse<AttendanceLogsDTO> listEmpAttendance(@PathVariable String username, Pageable pageable) {
		try {
			Page<AttendanceLogsDTO> results = attendanceLogsService.listEmpAttendance(username, pageable);
			return new PageResponse<>(PropertiesResponse.STATUS_MESSAGE_OK, PropertiesResponse.STATUS_CODE_OK,
					results.getContent(), results.getNumber(), results.getSize(), results.getTotalPages(),
					results.isLast(), new Long(0));
		} catch (Exception e) {
			e.printStackTrace();

			List<AttendanceLogsDTO> list = new ArrayList<AttendanceLogsDTO>();
			Page<AttendanceLogsDTO> model = new PageImpl<AttendanceLogsDTO>(list, pageable, 0);
			return new PageResponse<>(PropertiesResponse.INPUT_ERROR_NUMERIC,
					PropertiesResponse.STATUS_CODE_CLIENT_ERROR, model.getContent(), model.getNumber(), model.getSize(),
					model.getTotalPages(), model.isLast(), new Long(0));

		}
	}
	
	@GetMapping(value = "/list-history-attendance-employee/{username}/start-date/{startDt}/end-date/{endDt}")
	@ResponseBody
	public PageResponse<AttendanceLogsDTO> listHistoryAttendanceEmployee(@PathVariable String username, @PathVariable String startDt, @PathVariable String endDt, Pageable pageable) {
		try {
			Page<AttendanceLogsDTO> results = attendanceLogsService.listHistoryAttendanceEmployee(username, startDt, endDt, pageable);
			return new PageResponse<>(PropertiesResponse.STATUS_MESSAGE_OK, PropertiesResponse.STATUS_CODE_OK,
					results.getContent(), results.getNumber(), results.getSize(), results.getTotalPages(),
					results.isLast(), new Long(0));
		} catch (Exception e) {
			e.printStackTrace();

			List<AttendanceLogsDTO> list = new ArrayList<AttendanceLogsDTO>();
			Page<AttendanceLogsDTO> model = new PageImpl<AttendanceLogsDTO>(list, pageable, 0);
			return new PageResponse<>(PropertiesResponse.INPUT_ERROR_NUMERIC,
					PropertiesResponse.STATUS_CODE_CLIENT_ERROR, model.getContent(), model.getNumber(), model.getSize(),
					model.getTotalPages(), model.isLast(), new Long(0));

		}
	}

	@GetMapping(value = "/flag-attendance/{username}")
	@ResponseBody
	public Map<String, Object> flagAttendance(@PathVariable String username, Pageable pageable) {
		Map<String, Object> map = new HashMap<String, Object>();
		List<ScheduleAttendance> model = new ArrayList<ScheduleAttendance>();
		try {
			model = attendanceLogsService.flagAttendance(username, getDate());
			if (model.size() > 0) {
				map.put("flagAttendance", true);
				map.put("status", 200);
			} else {
				map.put("flagAttendance", false);
				map.put("status", 200);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}

	@GetMapping(value = "/list-data-absence/{username}")
	@ResponseBody
	public AttendanceLogsResponse listDataAbsence(@PathVariable String username) {
		AttendanceLogsResponse data = new AttendanceLogsResponse();
		try {
			AttendanceLogs model = attendanceLogsService.validateById(username, getDate());
			
			if(model != null) {
				data.setEmpArea(model.getEmpArea());
				data.setEmpId(model.getId().getEmpId());
				data.setEmpSubArea(model.getEmpSubArea());
				data.setLandscape(model.getId().getLandscape());
				data.setStartDate(model.getId().getStartDate());
				data.setEndDate(model.getId().getEndDate());
				data.setEmpSubAreaDescription(model.getEmpSubAreaDescription());
				data.setLatitudeCode(model.getLatitudeCode());
				data.setLongitudeCode(model.getLongitudeCode());
				data.setTimeStart(model.getTimeStart());
				data.setTimeEnd(model.getTimeEnd());
				data.setRadius(model.getRadius());
				data.setFullName(model.getId().getFullName());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	@GetMapping(value = "/list-history-attendance-byDate/{username}")
	@ResponseBody
	public Map<String, Object> listHistoryByNow(@PathVariable String username) {
		Map<String, Object> map = new HashMap<String, Object>();
		AttendanceLogs model = new AttendanceLogs();
		List<Object> dummy = new ArrayList<Object>();
		SimpleDateFormat timeNow = new SimpleDateFormat("yyyyMMdd");
		String startDate = timeNow.format(getNow());
		try {
			model = attendanceLogsService.listEmpAttedanceByDate(username, startDate);
			if (model != null) {
				if (model.getTimeEnd() != null) {
					map.put("listResponse", model);
					map.put("status", 300);
					map.put("message", "SUCCESS");
				} else {
					map.put("listResponse", model);
					map.put("status", 200);
					map.put("message", "SUCCESS");
				}
			} else {
				map.put("listResponse", dummy);
				map.put("status", 200);
				map.put("message", "Data tidak ditemukan");
			}
			return map;
		} catch (Exception e) {
			e.printStackTrace();
			map.put("message", e.getMessage());
			map.put("status", 400);
		}
		return map;
	}

	public void uploadPaths(UploadFileAttendanceRequest listFile) {
		try {
			service.saveAtt(listFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@GetMapping(value = "/find-extention-file")
	@ResponseBody
	public Map<String, Object> findFileType(@RequestParam String filename, @RequestParam String status) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String fileType = fileAttachmentService.findFileType(filename, status);
			map.put("status", 200);
			map.put("message", "SUCCESS");
			map.put("fileType", fileType);
			
			return map;
		} catch (Exception e) {
			e.printStackTrace();
			map.put("message", e.getMessage());
			map.put("status", 400);
		}
		return map;
	}

}
