package com.mega.ws.controller;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.mega.ws.constants.ModuleConstants;
import com.mega.ws.request.ChangeWorkHoursRequest;
import com.mega.ws.response.JBPM_MappingResponse;
import com.mega.ws.service.InformationWorkHoursService;

@RestController
public class InformationWorkHoursController extends BaseController {

	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(InformationWorkHoursController.class);

	@Autowired
	private InformationWorkHoursService informationWorkHoursService;

	@GetMapping("/information-work-hours/{username}")
	@ResponseBody
	public Map<String, Object> informastionWorkHours(@PathVariable String username) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map.put("status", 200);
			map.put("objectResponse", informationWorkHoursService.detailWorkHours(username));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			map.put("status", 400);
			map.put("error", e.getMessage());
		}

		return map;
	}

	@GetMapping("/list-work-hours")
	@ResponseBody
	public Map<String, Object> listWorkHours() {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map.put("status", 200);
			map.put("listResponse", informationWorkHoursService.listWorkHours());
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			map.put("status", 400);
			map.put("error", e.getMessage());
		}

		return map;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PostMapping("/update-work-hours")
	@ResponseBody
	public Map<String, Object> createChange(@RequestBody ChangeWorkHoursRequest request) {
		request.setModuleName(ModuleConstants.CHANGE_MODULE);

		Map<String, Object> map = new HashMap<String, Object>();

		JBPM_MappingResponse mapping = mappingURL.initiateLeaveRequest();

		HttpEntity<ChangeWorkHoursRequest> requestEntity = new HttpEntity<>(request, headerSettings.setupHeader());
		Object response = restTemplate.exchange(mapping.getUrl(), mapping.getMethod(), requestEntity, Object.class);
		
		if(validationEmployeeOffice(request.getEmployee_id()) == false) {
			map.put("message",
					"Menu ini hanya tersedia untuk akun Kantor Pusat Bank Mega ");
			map.put("status", 400);
			return map;
		}
		
		try {
			ResponseEntity<Object> responseEntity = (ResponseEntity<Object>) response;
			map.put("instancesId", responseEntity.getBody());

			Integer processId = (Integer) map.get("instancesId");
			JBPM_MappingResponse activeTask = mappingURL.showActiveTaskByProcessId(processId);

			ResponseEntity<HashMap> responseActiveTask = restTemplate.exchange(activeTask.getUrl(),
					activeTask.getMethod(), null, HashMap.class);
			Integer activeTaskId = findTaskId(responseActiveTask);
			map.put("taskId", activeTaskId);

			ChangeWorkHoursRequest status = new ChangeWorkHoursRequest();
			status.setStatus("approve");

			HttpEntity<ChangeWorkHoursRequest> completeEntity = new HttpEntity<>(status,
					headerSettings.setupHeaderContentType());
			JBPM_MappingResponse completeTask = mappingURL.completedTask(activeTaskId);

			UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(completeTask.getUrl())
					.queryParam("user", request.getEmployee_id()).queryParam("auto-progress", "true");

			restTemplate.exchange(builder.buildAndExpand(completeTask.getUrl()).toUri(), completeTask.getMethod(),
					completeEntity, Object.class);
			logsSave(request, processId, activeTaskId);

			map.put("message", "Request Update Jadwal Berhasil !");
			map.put("status", 200);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			map.put("error", e.getMessage());
			map.put("status", 400);
		}

		return map;
	}

}
