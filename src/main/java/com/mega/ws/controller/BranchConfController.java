package com.mega.ws.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mega.ws.request.BranchIdRequest;
import com.mega.ws.request.BranchRequest;
import com.mega.ws.response.JBPM_MappingResponse;
import com.mega.ws.response.PageResponse;
import com.mega.ws.response.PropertiesResponse;
import com.mega.ws.service.dto.BranchConfDTO;

@RestController
public class BranchConfController extends BaseController {

	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(BranchConfController.class);

	@GetMapping("/list-branch")
	@ResponseBody
	public PageResponse<BranchConfDTO> listBranch(Pageable pageable,
			@RequestParam(name = "empArea", defaultValue = "") String paramEmpArea,
			@RequestParam(name = "empSubArea", defaultValue = "") String paramEmpSubArea,
			@RequestParam(name = "empSubAreaDescription", defaultValue = "") String paramEmpSubAreaDescription,
			@RequestParam(name = "address", defaultValue = "") String paramAddress,
			@RequestParam(name = "empAreaDesc", defaultValue = "") String paramEmpAreaDesc,
			@RequestParam(name = "latitudeCode", defaultValue = "") String paramLatitudeCode,
			@RequestParam(name = "longitudeCode", defaultValue = "") String paramLongitudeCode,
			@RequestParam(name = "npwp", defaultValue = "") String paramNpwp,
			@RequestParam(name = "radius", defaultValue = "")String paramRadius) {

		try {
			Page<BranchConfDTO> results = branchConfService.listBranch(paramEmpArea, paramEmpSubArea, paramAddress,
					paramEmpSubAreaDescription, paramNpwp, paramLatitudeCode, paramLongitudeCode, paramRadius, pageable);
			return new PageResponse<>(PropertiesResponse.STATUS_MESSAGE_OK, PropertiesResponse.STATUS_CODE_OK,
					results.getContent(), results.getNumber(), results.getSize(), results.getTotalPages(),
					results.isLast(), new Long(0));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			List<BranchConfDTO> list = new ArrayList<BranchConfDTO>();
			Page<BranchConfDTO> model = new PageImpl<BranchConfDTO>(list, pageable, 0);

			return new PageResponse<>(PropertiesResponse.INPUT_ERROR_NUMERIC,
					PropertiesResponse.STATUS_CODE_CLIENT_ERROR, model.getContent(), model.getNumber(), model.getSize(),
					model.getTotalPages(), model.isLast(), new Long(0));
		}
	}

	@PostMapping("/update-branch-maps")
	@ResponseBody
	public Map<String, Object> updateBranchMaps(@RequestBody List<BranchRequest> requests) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			if (!requests.isEmpty()) {
				for (int i = 0; i < requests.size(); i++) {
					branchConfService.updateBranch(requests.get(i));
				}
				map.put("status", 200);
				map.put("message", "Success Update ");
			}else {
				map.put("status", 200);
				map.put("message", "No Data Change !!! ");
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			map.put("status", 400);
			map.put("error", e.getMessage());
		}

		return map;
	}

	@PostMapping("/find-branch")
	@ResponseBody
	public Map<String, Object> findBranch(@RequestBody BranchIdRequest request) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			if(branchConfService.findBranchByID(request) == null) {
				map.put("status", 400);
				map.put("message", "Data tidak ditemukan");
			}else {
				map.put("objectResponse", branchConfService.findBranchByID(request));
				map.put("status", 200);
			}
		} catch (Exception e) {
			// TODO: handle exception
			map.put("status", 400);
			map.put("error", e.getMessage());
		}
		return map;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@GetMapping("/find-branch-byid/{username}")
	@ResponseBody
	public Map<String, Object> findListBranchById(@PathVariable String username){
		Map<String, Object> map = new HashMap<String, Object>();
		Object response = null ;
		BranchIdRequest model = new BranchIdRequest();
		List<Object> listResponse = new ArrayList<Object>();
		
		HttpEntity<Object> requestEntity = new HttpEntity<>(headerSettings.setupHeaderAccept());
		JBPM_MappingResponse mapping = mappingURL.findListBranchById(username);
		response = restTemplate.exchange(mapping.getUrl(), mapping.getMethod(), requestEntity, Object.class);
		try {
			ResponseEntity<HashMap> responseEntity = (ResponseEntity<HashMap>) response;
			Object listBranch = responseEntity.getBody().get("listResponse");
				LinkedHashMap<String, Object> linkModel = (LinkedHashMap<String, Object>) listBranch;
				LinkedHashMap<String, Object> linkId	= (LinkedHashMap<String, Object>) linkModel.get("id");
				
				String empArea 		= String.valueOf(linkId.get("empArea"));
				String empSubArea	= String.valueOf(linkId.get("empSubArea"));
				String landscape	= "100";
				
				model.setEmpArea(empArea);
				model.setEmpSubArea(empSubArea);
				model.setLandscape(landscape);
				listResponse.add(branchConfService.findBranchByID(model));
			map.put("listResponse", listResponse);
			map.put("status", 200);
			map.put("message", "SUCCESS");
			
		} catch (Exception e) {
			e.printStackTrace();
			map.put("message", e.getMessage());
			map.put("status", 400);
		}
		return map;
	}
}
