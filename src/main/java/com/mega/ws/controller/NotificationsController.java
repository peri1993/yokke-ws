package com.mega.ws.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mega.ws.model.FIleAttachment;
import com.mega.ws.model.InstancesLogs;
import com.mega.ws.model.Notifications;
import com.mega.ws.request.LeaveRequest;
import com.mega.ws.response.DataDateResponse;
import com.mega.ws.response.JBPM_MappingResponse;
import com.mega.ws.response.NotifResponse;
import com.mega.ws.response.PageResponse;
import com.mega.ws.response.PropertiesResponse;
import com.mega.ws.response.QuotaInformationResponse;
import com.mega.ws.service.InstancesLogsService;
import com.mega.ws.service.NotificationService;
import com.mega.ws.service.dto.NotificationDTO;


@RestController
public class NotificationsController extends BaseController {
	
	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(NotificationsController.class);
	
	@Autowired
	private NotificationService notificationService;
	
	@Autowired
	private InstancesLogsService instancesService;
	
	@GetMapping("/list-notif-user/{username}")
	@ResponseBody
	private Map<String, Object> listNotifUser(@PathVariable String username) {
		Map<String, Object> map = new HashMap<String, Object>();
		List<NotifResponse> response = new ArrayList<NotifResponse>();
		try {
			List<Notifications> listModel = notificationService.listNotifUser(username);
			for (int i = 0; i < listModel.size(); i++) {
				Notifications model = listModel.get(i);
				NotifResponse resp = new NotifResponse();
				resp.setId(String.valueOf(model.getId()));
				resp.setStages(model.getStages());
				resp.setInstancesId(String.valueOf(model.getInstancesId().getInstancesId()));
				resp.setDescription(model.getDescriptionNotif());
				resp.setBusinessProcess(model.getBusinesProcess());
				response.add(resp);
			}
			map.put("status", 200);
			map.put("messages", "SUCCESS");
			map.put("listResponse", response);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			map.put("status", 400);
			map.put("error", e.getMessage());
			
		}
		return map;
	}
	
	@GetMapping("/list-notif/{username}")
	@ResponseBody
	public PageResponse<NotificationDTO> listNotif(Pageable pageable, @PathVariable String username,
			@RequestParam(name = "instancesId", defaultValue = "") String paramInstancesId,
			@RequestParam(name = "businessProcess", defaultValue = "") String paramBusinessProcess,
			@RequestParam(name = "subject", defaultValue = "") String paramSubject,
			@RequestParam(name = "status", defaultValue = "") String paramStatus,
			@RequestParam(name = "requesterId", defaultValue = "") String paramRequesterId,
			@RequestParam(name = "requesterName", defaultValue = "") String paramRequesterName,
			@RequestParam(name = "startDate", defaultValue = "") String paramStartDate,
			@RequestParam(name = "endDate", defaultValue = "") String paramEndDate,
			@RequestParam(name = "description", defaultValue = "") String paramDescription)  {
		try {
			Page<NotificationDTO> results = notificationService.listNotifUser(username, paramInstancesId, paramBusinessProcess,
					paramSubject, paramStatus,  paramRequesterId, paramRequesterName, paramStartDate, paramEndDate, paramDescription, pageable);
			return new PageResponse<>(PropertiesResponse.STATUS_MESSAGE_OK, PropertiesResponse.STATUS_CODE_OK,
					results.getContent(), results.getNumber(), results.getSize(), results.getTotalPages(),
					results.isLast(), new Long(0));
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			List<NotificationDTO> list = new ArrayList<NotificationDTO>();
			Page<NotificationDTO> model = new PageImpl<NotificationDTO>(list, pageable, 0);

			return new PageResponse<>(PropertiesResponse.STATUS_MESSAGE_OK, PropertiesResponse.STATUS_CODE_OK,
					model.getContent(), model.getNumber(), model.getSize(), model.getTotalPages(), model.isLast(), new Long(0));
		}
	}

	@GetMapping("/update-notif")
	@ResponseBody
	private Map<String, Object> updateNotif(@RequestParam String id, @RequestParam String username, @RequestParam String instancesId) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			InstancesLogs instances = instancesService.getInstancesLogById(new Long(instancesId));
			if (instances != null) {
				notificationService.update(id, username, instancesId);
				
				map.put("status", 200);
				map.put("messages", "SUCCESS");
			}else {
				map.put("error", 401);
				map.put("messages", "instances ID can't find !");
			}
			
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			map.put("status", 400);
			map.put("error", e.getMessage());
			
		}
		
		return map;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@GetMapping("/task-detail-notification")
	@ResponseBody
	private Map<Object, Object> detailTask(@RequestParam String instancesId, @RequestParam String username, @RequestParam String id) {
		Map<Object, Object> map = new HashMap<Object, Object>();

		JBPM_MappingResponse mapping = mappingURL.instancesVariables(Integer.valueOf(instancesId));
		HttpEntity<LeaveRequest> requestEntity = new HttpEntity<>(headerSettings.setupHeaderAccept());

		Object response = restTemplate.exchange(mapping.getUrl(), mapping.getMethod(), requestEntity, Object.class);

		ResponseEntity<HashMap> responseEntity = (ResponseEntity<HashMap>) response;

		List<Object> instancesList = (List<Object>) responseEntity.getBody().get("variable-instance");
		List<FIleAttachment> listFileUpload = new ArrayList<FIleAttachment>();
		for (int i = 0; i < instancesList.size(); i++) {
			FIleAttachment model = new FIleAttachment();
			Map<String, Object> instancesValues = (Map<String, Object>) instancesList.get(i);
			if ("filename1".equals(instancesValues.get("name"))) {
				model = uploadService.getFileByFilename(String.valueOf(instancesValues.get("value")));
				listFileUpload.add(model);
			} else if ("filename2".equals(instancesValues.get("name"))) {
				model = uploadService.getFileByFilename(String.valueOf(instancesValues.get("value")));
				listFileUpload.add(model);
			} else if ("filename3".equals(instancesValues.get("name"))) {
				model = uploadService.getFileByFilename(String.valueOf(instancesValues.get("value")));
				listFileUpload.add(model);
			} else if ("filename4".equals(instancesValues.get("name"))) {
				model = uploadService.getFileByFilename(String.valueOf(instancesValues.get("value")));
				listFileUpload.add(model);
			} else if ("filename5".equals(instancesValues.get("name"))) {
				model = uploadService.getFileByFilename(String.valueOf(instancesValues.get("value")));
				listFileUpload.add(model);
			} else {
				map.put(instancesValues.get("name"),
						(instancesValues.get("value") == "" ? instancesValues.get("old-value")
								: instancesValues.get("value")));
			}
		}

		map.put("listFileUpload", listFileUpload);

		JBPM_MappingResponse activeTask = mappingURL.showActiveTaskByProcessId(Integer.valueOf(instancesId));
		ResponseEntity<HashMap> responseActiveTask = restTemplate.exchange(activeTask.getUrl(), activeTask.getMethod(),
				null, HashMap.class);
		try {
			Integer activeTaskId = findTaskId(responseActiveTask);
			map.put("taskId", activeTaskId);
			map.put("instancesId", instancesId);
			notificationService.update(id, username, instancesId);
		} catch (Exception e) {
			// TODO: handle exception
			map.put("error", 400);
			map.put("message", "Data Not Found");
		}

		return map;
	}
	
	@SuppressWarnings("rawtypes")
	@GetMapping("/find-list-date/{requesterId}")
	@ResponseBody
	public Map<String, Object> getDataDate(@PathVariable String requesterId){
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			
			JBPM_MappingResponse activeTask = mappingURL.findDateDeduction(requesterId);
			ResponseEntity<HashMap> responseActiveTask = restTemplate.exchange(activeTask.getUrl(), activeTask.getMethod(),null, HashMap.class);
			String startDate = String.valueOf(responseActiveTask.getBody().get("startDate"));
			String endDate = String.valueOf(responseActiveTask.getBody().get("endDate"));
			
			List<DataDateResponse> listDate = new ArrayList<DataDateResponse>();
			List<InstancesLogs> list = instancesService.findDateDeduction(requesterId, startDate, endDate);
			if (!list.isEmpty()) {
				for (int i = 0; i < list.size(); i++) {
					DataDateResponse data = new DataDateResponse();
					data.setStartDate(list.get(i).getStartDate());
					data.setEndDate(list.get(i).getEndDate());
					listDate.add(data);
				}
			}
			map.put("status", 200);
			map.put("listResponse", listDate);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			map.put("status", 400);
			map.put("error", e.getMessage());
		}
		return map;
		
	}
	
	@SuppressWarnings("rawtypes")
	@GetMapping("/validation-list-date/{requesterId}")
	@ResponseBody
	public Map<String, Object> validationListDate(@PathVariable String requesterId, @PathVariable Integer totDaysRequest){
		Map<String, Object> map = new HashMap<String, Object>();
		Integer total = 0;
		try {
			List<DataDateResponse> listDate = new ArrayList<DataDateResponse>();
			List<InstancesLogs> list = instancesService.listDataDate(requesterId);
			if (!list.isEmpty()) {
				for (int i = 0; i < list.size(); i++) {
					DataDateResponse data = new DataDateResponse();
					data.setStartDate(list.get(i).getStartDate());
					data.setEndDate(list.get(i).getEndDate());
					listDate.add(data);
				}
			}
			
			if (!listDate.isEmpty()) {
				for (int i = 0; i < listDate.size(); i++) {
					JBPM_MappingResponse activeTask = mappingURL.getHolidayBetween(listDate.get(i).getStartDate(), listDate.get(i).getEndDate());
					ResponseEntity<HashMap> responseActiveTask = restTemplate.exchange(activeTask.getUrl(), activeTask.getMethod(),null, HashMap.class);
					String count = String.valueOf(responseActiveTask.getBody().get("total"));
					total = total + Integer.parseInt(count);
				}
			}
			
			JBPM_MappingResponse activeTask = mappingURL.findLastRemaining(requesterId);
			ResponseEntity<HashMap> responseActiveTask = restTemplate.exchange(activeTask.getUrl(), activeTask.getMethod(),null, HashMap.class);
			String remaining = String.valueOf(responseActiveTask.getBody().get("remaining"));
			
			if (!"".equals(remaining) && null != remaining) {
				if (total > Integer.parseInt(remaining)) {
					map.put("status", 400);
					map.put("flagRequest", true);
					map.put("message", "Anda tidak memiliki cukup kuota untuk melakukan Request, silahkan cek kembali task anda yang masih berjalan !");
				} else if(0 == (Integer.parseInt(remaining)-total)){
					map.put("status", 400);
					map.put("flagRequest", true);
					map.put("message", "Anda sudah tidak memiliki kuota untuk melakukan Request, silahkan cek kembali task anda yang masih berjalan !");
				} else if(totDaysRequest > (Integer.parseInt(remaining)-total)){
					map.put("status", 400);
					map.put("flagRequest", true);
					map.put("message", "Total hari Request yang Anda ambil lebih besar dari sisa kuota Anda !");
				} else {
					map.put("status", 200);
					map.put("flagRequest", false);
				}
			} else {
				map.put("status", 200);
				map.put("flagRequest", false);
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			map.put("status", 400);
			map.put("error", e.getMessage());
		}
		return map;
		
	}
	
	
	@SuppressWarnings("rawtypes")
	@GetMapping("/information-remaining-quota/{requesterId}")
	@ResponseBody
	public Map<String, Object> informationRemainingQuota(@PathVariable String requesterId){
		Map<String, Object> map = new HashMap<String, Object>();
		QuotaInformationResponse response = new QuotaInformationResponse();
		
		Integer total = 0;
		try {
			List<DataDateResponse> listDate = new ArrayList<DataDateResponse>();
			List<InstancesLogs> list = instancesService.listDataDate(requesterId);
			if (!list.isEmpty()) {
				for (int i = 0; i < list.size(); i++) {
					DataDateResponse data = new DataDateResponse();
					data.setStartDate(list.get(i).getStartDate());
					data.setEndDate(list.get(i).getEndDate());
					listDate.add(data);
				}
			}
			
			if (!listDate.isEmpty()) {
				for (int i = 0; i < listDate.size(); i++) {
					JBPM_MappingResponse activeTask = mappingURL.getHolidayBetween(listDate.get(i).getStartDate(), listDate.get(i).getEndDate());
					ResponseEntity<HashMap> responseActiveTask = restTemplate.exchange(activeTask.getUrl(), activeTask.getMethod(),null, HashMap.class);
					String count = String.valueOf(responseActiveTask.getBody().get("total"));
					total = total + Integer.parseInt(count);
				}
				
				response.setProcessRemaining(total);
			}else {
				response.setProcessRemaining(0);
			}
			
			JBPM_MappingResponse activeTask = mappingURL.findLastRemaining(requesterId);
			ResponseEntity<HashMap> responseActiveTask = restTemplate.exchange(activeTask.getUrl(), activeTask.getMethod(),null, HashMap.class);
			String remaining = String.valueOf(responseActiveTask.getBody().get("remaining"));
			if (!"".equals(remaining) && null != remaining) {
				response.setLastRemaining(Integer.parseInt(remaining));
			}else {
				response.setLastRemaining(0);
			}
			
			if (response.getProcessRemaining() < response.getLastRemaining()) {
				response.setTotal(response.getLastRemaining()-response.getProcessRemaining());
			}else {
				response.setTotal(0);
			}
			
			map.put("status", 200);
			map.put("objectResponse", response);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			map.put("status", 400);
			map.put("error", e.getMessage());
		}
		return map;
		
	}
}
