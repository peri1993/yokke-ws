package com.mega.ws.controller;

import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.DatatypeConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.util.UriComponentsBuilder;

import com.mega.ws.config.JbpmConfig;
import com.mega.ws.constants.ModuleConstants;
import com.mega.ws.jbpm.MappingPermitURL;
import com.mega.ws.request.FileNameRequest;
import com.mega.ws.request.HistoryDateRequest;
import com.mega.ws.request.LeaveStatusRequest;
import com.mega.ws.request.PermitRequest;
import com.mega.ws.request.UploadFileRequest;
import com.mega.ws.response.JBPM_MappingResponse;
import com.mega.ws.response.UploadFileResponse;
import com.mega.ws.service.UploadFileService;
import com.mega.ws.util.HeaderSettings;

@RestController
public class PermitController extends BaseController {

	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(LeaveController.class);

	@Autowired
	private MappingPermitURL mappingURL;

	@Autowired
	private HeaderSettings headerSettings;

	private RestTemplate restTemplate = new RestTemplate();

	@Autowired
	JbpmConfig jbpmConfig;

	@Autowired
	private UploadFileService service;

	private HistoryDateRequest setModel(PermitRequest request) {
		HistoryDateRequest model = new HistoryDateRequest();
		model.setUsername(request.getEmployee_id());
		model.setAbsencesType(request.getAbsences_type());
		model.setStartDate(request.getLeave_start_date());
		model.setEndDate(request.getLeave_end_date());
		return model;
	}

	private Boolean validationApprovalNotEmpty(PermitRequest request) {

		Boolean flag = false;

		try {
			if ("".equals(request.getApproval_ds_1()) || null == request.getApproval_ds_1()) {
				return true;
			}
			
			if("".equals(request.getApproval_ds_1()) || null == request.getApproval_ds_1()) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PostMapping("/create-permit")
	@ResponseBody
	public Map<String, Object> createPermit(@RequestBody PermitRequest request) {
		request.setModuleName(ModuleConstants.PERMIT_MODULE);

		Map<String, Object> map = new HashMap<String, Object>();
		Map<String, Object> validationAbsenceType = new HashMap<String, Object>();
		
		// validation approval
		if (validationApprovalNotEmpty(request)) {
			map.put("message", "Anda Tidak Bisa mengajukan cuti karena Approval Anda Tidak ditemukan !!!");
			map.put("status", 400);

			return map;	
		}

		// validation for date request
		if ("1000".equals(request.getAbsences_type()) || "3100".equals(request.getAbsences_type())) {
			Map<String, Object> mapDate = new HashMap<String, Object>();
			mapDate = validationDateRequest(request.getEmployee_id(), cekTotalDate(request.getLeave_start_date(), request.getLeave_end_date()));
			if ("400".equals(String.valueOf(mapDate.get("status")))) {
				mapDate.remove("flagRequest");
				return mapDate;
			}
		}

		// validationDeduction
		if (validationCheckDeduction(request) == false && "3100".equals(request.getAbsences_type())) {
			map.put("message", "Username anda berada diluar deduction, hubungi admin untuk info lebih lanjut");
			map.put("status", 400);

			return map;
		}

		// validation history
		HistoryDateRequest historyDateRequest = setModel(request);
		boolean validateDate = validationFindDateHistory(historyDateRequest);
		if (validateDate) {
			map.put("message", "Anda sudah mengambil Cuti pada tanggal ini");
			map.put("status", 400);

			return map;
		}

		validationAbsenceType = validateAbsencesType(request.getAbsences_type(), request.getTotal_taken_quota(),
				request.getEmployee_id());
		boolean flag = (boolean) validationAbsenceType.get("flagAbsence");
		if (flag) {
			validationAbsenceType.remove("flagAbsence");
			return validationAbsenceType;
		}

		// validation absencetype 2101
		if (validationAbsencesType(request)) {
			map.put("status", 400);
			map.put("error", "Anda Sudah " + request.getAbsenceDescription() + " bulan ini !!!");
			map.put("message", "Anda Sudah " + request.getAbsenceDescription() + " bulan ini !!!");
			return map;
		}

		// set filename for upload files
		if (request.getListUpload() != null) {
			if (request.getListUpload().size() > 0) {
				for (int i = 0; i < request.getListUpload().size(); i++) {
					FileNameRequest file = new FileNameRequest();
					String filename = request.getListUpload().get(i).getFileName();
					file.setFilename(filename);
					request.getListUpload().get(i).setCreatedBy(request.getEmployee_id());
					request.getListUpload().get(i).setCreatedDt(getNow());
					request.getListUpload().get(i).setDirectory(getPaths());
					if (i == 0) {
						request.setFilename1(filename);
					} else if (i == 1) {
						request.setFilename2(filename);
					} else if (i == 2) {
						request.setFilename3(filename);
					} else if (i == 3) {
						request.setFilename4(filename);
					} else if (i == 4) {
						request.setFilename5(filename);
					}
				}
				uploadPaths(request.getListUpload());
			}
		}

		JBPM_MappingResponse mapping = mappingURL.initiateLeaveRequest();
		HttpEntity<PermitRequest> requestEntity = new HttpEntity<>(request, headerSettings.setupHeader());
		Object response = restTemplate.exchange(mapping.getUrl(), mapping.getMethod(), requestEntity, Object.class);

		try {
			ResponseEntity<Object> responseEntity = (ResponseEntity<Object>) response;
			map.put("processId", responseEntity.getBody());

			Integer processId = (Integer) map.get("processId");
			JBPM_MappingResponse activeTask = mappingURL.showActiveTaskByProcessId(processId);

			ResponseEntity<HashMap> responseActiveTask = restTemplate.exchange(activeTask.getUrl(),
					activeTask.getMethod(), null, HashMap.class);
			Integer activeTaskId = findTaskId(responseActiveTask);
			map.put("taskId", activeTaskId);

			LeaveStatusRequest status = new LeaveStatusRequest();
			status.setLeave_status("approve");

			HttpEntity<LeaveStatusRequest> completeEntity = new HttpEntity<>(status,
					headerSettings.setupHeaderContentType());
			JBPM_MappingResponse completeTask = mappingURL.completedTask(activeTaskId);

			UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(completeTask.getUrl())
					.queryParam("user", request.getEmployee_id()).queryParam("auto-progress", "true");

			restTemplate.exchange(builder.buildAndExpand(completeTask.getUrl()).toUri(), completeTask.getMethod(),
					completeEntity, Object.class);
			logsSave(request, processId, activeTaskId);

			map.put("message", "Success");
			map.put("status", 200);

		} catch (Exception e) {
			map.put("error", e.getMessage());
			map.put("status", 400);
		}
		return map;
	}

	@PostMapping(value = "/upload-m-file")
	@ResponseBody
	public Map<String, Object> uploadfileMobile(HttpServletRequest request)
			throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		Map<String, Object> map = new HashMap<String, Object>();
		try {

			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			Iterator<String> it = multipartRequest.getFileNames();
			while (it.hasNext()) {
				UploadFileResponse response = new UploadFileResponse();
				MultipartFile multipart = multipartRequest.getFile(it.next());
				String file = multipart.getOriginalFilename();
				String fileSize = String.valueOf(multipart.getSize());
				String fileType = multipart.getContentType();
				String fileName = UUID.randomUUID() + "-" + file;
				byte[] bytes = multipart.getBytes();
				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(jbpmConfig.getLocation_document() + fileName));
				stream.write(bytes);
				stream.close();

				response.setFileName(fileName);
				response.setFileSize(fileSize);
				response.setFileType(fileType);
				map.put("data", response);
			}
			map.put("message", "success Upload");
			map.put("status", 200);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			map.put("status", 400);
			map.put("message", e.getMessage());
		}
		return map;
	}

	@PostMapping(value = "/upload-m-file2")
	@ResponseBody
	public Map<String, Object> uploadfileMobile2(@RequestBody List<UploadFileRequest> listUpload)
			throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		Map<String, Object> map = new HashMap<String, Object>();
		List<String> listFilename = new ArrayList<String>();
		try {
			for (int i = 0; i < listUpload.size(); i++) {
				UploadFileRequest request = listUpload.get(i);

				String filename = request.getCreatedBy().concat("-").concat("file" + request.getFileNo()).concat("-")
						.concat(request.getFileDescription());
				String fileNameResponse = ("file" + request.getFileNo()).concat("-")
						.concat(request.getFileDescription());
				if (request.getFileName() != null) {
					filename = filename.concat("-").concat(request.getFileName());
					fileNameResponse = fileNameResponse.concat("-").concat(request.getFileName());
				}

				Path path = Paths.get(getPaths() + File.separator + filename);

				byte[] bytePhoto = DatatypeConverter.parseBase64Binary(request.getContent());
				if ("application/pdf".equals(request.getFileType())) {
					Path folder = Paths.get(getPaths());
					if (!Files.exists(folder)) {
						Files.createDirectories(path);
					}
					FileOutputStream fos = new FileOutputStream(getPaths() + File.separator + filename);
					fos.write(bytePhoto);
					fos.flush();
					fos.close();
				} else {
					BufferedImage image = ImageIO.read(new ByteArrayInputStream(bytePhoto));
					if (!Files.exists(path)) {
						Files.createDirectories(path);
						File file = new File(getPaths() + File.separator + filename);
						ImageIO.write(image, "PNG", file);
					} else {
						Files.delete(path);
						Files.createDirectories(path);
						File file = new File(getPaths() + File.separator + filename);
						ImageIO.write(image, "PNG", file);
					}
				}
				listFilename.add(fileNameResponse);
			}
			map.put("status", 200);
			map.put("message", "upload success");
			map.put("filename", listFilename);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			map.put("status", 400);
			map.put("message", e.getMessage());
		}
		return map;
	}

	@PostMapping(value = "/upload-m-file3")
	@ResponseBody
	public Map<String, Object> uploadfileMobile3(@RequestBody UploadFileRequest request)
			throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {

		Map<String, Object> map = new HashMap<String, Object>();
		List<String> listFilename = new ArrayList<String>();
		try {
			byte[] bytePhoto = DatatypeConverter.parseBase64Binary(request.getContent());
			BufferedImage image = ImageIO.read(new ByteArrayInputStream(bytePhoto));

			String filename = request.getCreatedBy().concat("-").concat("file" + request.getFileNo()).concat("-")
					.concat(request.getFileDescription());
			if (request.getFileName() != null) {
				filename = filename.concat("-").concat(request.getFileName());
			}
			Path path = Paths.get(getPaths() + File.separator + filename);
			if (!Files.exists(path)) {
				Files.createDirectories(path);
				File file = new File(getPaths() + File.separator + filename);
				ImageIO.write(image, "PNG", file);
				// Files.deleteIfExists(path);
			}
			listFilename.add(filename);
			map.put("status", 200);
			map.put("message", "upload success");
			map.put("filename", listFilename);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			map.put("status", 400);
			map.put("message", e.getMessage());
		}
		return map;
	}

	@PostMapping(value = "/download-files")
	@ResponseBody
	public Map<String, Object> downloadFiles(@RequestBody UploadFileRequest file) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String path = null;
			String a[] = file.getDirectory().split(jbpmConfig.getLocation_document());
			if (a[1] != null) {
				path = jbpmConfig.getPath_file().concat(a[1]).concat("/").concat(file.getCreatedBy()).concat("-")
						.concat(file.getFileName());
			}
			map.put("status", 200);
			map.put("path", path);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			map.put("status", 400);
			map.put("message", e.getMessage());
		}
		return map;
	}

	@GetMapping("/download-images")
	@ResponseBody
	public ResponseEntity<byte[]> downloadImages(@RequestParam String directory, @RequestParam String filename,
			@RequestParam String type) {

		ResponseEntity<byte[]> rsp = new ResponseEntity<byte[]>(HttpStatus.BAD_REQUEST);

		try {
			// download file in temp folder
			Path pat = Paths.get(String.valueOf(directory + File.separator + filename));
			byte[] contents = Files.readAllBytes(pat);

			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.setContentType(MediaType.parseMediaType(type));
			responseHeaders.set("Content-Disposition", "attachment");
			responseHeaders.setContentDispositionFormData(filename, filename);
			responseHeaders.setCacheControl("must-revalidate, post-check=0, pre-check=0");
			rsp = new ResponseEntity<byte[]>(contents, responseHeaders, HttpStatus.OK);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return rsp;
	}

	public void uploadPaths(List<UploadFileRequest> listFile) {
		try {
			service.save(listFile);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
}
