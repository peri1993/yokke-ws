package com.mega.ws.controller;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.imageio.ImageIO;
import javax.xml.bind.DatatypeConverter;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.client.RestTemplate;

import com.mega.ws.config.BusinessProcessConfig;
import com.mega.ws.config.StagesConfig;
import com.mega.ws.config.UploadPathConfig;
import com.mega.ws.jbpm.MappingURL;
import com.mega.ws.model.BranchConf;
import com.mega.ws.model.InstancesLogs;
import com.mega.ws.model.Notifications;
import com.mega.ws.model.RoleUser;
import com.mega.ws.model.TaskLogs;
import com.mega.ws.request.ApproveRequest;
import com.mega.ws.request.AttendanceRequest;
import com.mega.ws.request.BranchIdRequest;
import com.mega.ws.request.ChangeWorkHoursRequest;
import com.mega.ws.request.HistoryDateRequest;
import com.mega.ws.request.LeaveRequest;
import com.mega.ws.request.PermitRequest;
import com.mega.ws.request.UploadFileAttendanceRequest;
import com.mega.ws.response.JBPM_MappingResponse;
import com.mega.ws.service.AttendanceLogsService;
import com.mega.ws.service.BranchConfService;
import com.mega.ws.service.InformationWorkHoursService;
import com.mega.ws.service.InstancesLogsService;
import com.mega.ws.service.NotificationService;
import com.mega.ws.service.RoleUserService;
import com.mega.ws.service.TaskLogsService;
import com.mega.ws.service.UploadFileService;
import com.mega.ws.util.HeaderSettings;

@Controller
public class BaseController {

	@Autowired
	private TaskLogsService taskService;

	@Autowired
	private InstancesLogsService instancesService;

	@Autowired
	private NotificationService notificationService;

	@Autowired
	private RoleUserService roleService;

	@Autowired
	public BusinessProcessConfig businessProcessConfig;

	@Autowired
	public StagesConfig stagesConfig;

	@Autowired
	public MappingURL mappingURL;

	@Autowired
	public HeaderSettings headerSettings;

	@Autowired
	public UploadFileService uploadService;

	@Autowired
	public BranchConfService branchConfService;

	@Autowired
	public AttendanceLogsService attendanceLogsService;

	@Autowired
	private NotificationsController notificationsController;

	@Autowired
	public InformationWorkHoursService informationWorkHoursService;

	@Autowired
	public UploadPathConfig uploadpathconfig;

	public RestTemplate restTemplate = new RestTemplate();
	public SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public SimpleDateFormat format2 = new SimpleDateFormat("yyyyMMdd");
	public SimpleDateFormat formatTime = new SimpleDateFormat("HH:mm:ss");
	public SimpleDateFormat formatDisplay = new SimpleDateFormat("dd-MM-yyyy");
	private static ZipOutputStream zout;

	protected <T> T parseJson(String response, Class<T> clazz)
			throws IOException, JsonParseException, JsonMappingException {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.readValue(response, clazz);
	}

	protected <T> List<T> parseJsonList(String response, Class<T> clazz)
			throws IOException, JsonParseException, JsonMappingException {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.readValue(response, new TypeReference<List<T>>() {
		});
	}

	public void logsSave(Object object, Integer instancesId, Integer taskId) {
		try {
			if (object.getClass().equals(LeaveRequest.class)) {
				saveInstancesLeave(object, instancesId, taskId);
				saveTaskLeave(object, instancesId, taskId);
			} else if (object.getClass().equals(PermitRequest.class)) {
				saveInstancesPermit(object, instancesId, taskId);
				saveTaskPermit(object, instancesId, taskId);
			} else if (object.getClass().equals(ChangeWorkHoursRequest.class)) {
				saveInstancesWorkHours(object, instancesId, taskId);
				saveTaskWorkHours(object, instancesId, taskId);
			}

			saveNotifForAssign(object, instancesId, taskId);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public void saveNotifForAssign(Object object, Integer instancesId, Integer taskId) {
		try {
			if (object.getClass().equals(LeaveRequest.class)) {
				saveNotifLeave(object, instancesId, taskId);
			} else if (object.getClass().equals(PermitRequest.class)) {
				saveNotifPermit(object, instancesId, taskId);
			} else if (object.getClass().equals(ChangeWorkHoursRequest.class)) {
				saveNotifWorkHours(object, instancesId, taskId);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	private void saveNotifWorkHours(Object object, Integer instancesId, Integer taskId) {
		ChangeWorkHoursRequest request = (ChangeWorkHoursRequest) object;
		try {
			Notifications model = new Notifications();
			model.setAssigner(trackingTaskApproval(String.valueOf(instancesId)));
			model.setCreatedBy(request.getEmployee_id());
			model.setCreatedDt(getNow());
			model.setStages(stagesConfig.getWaiting());
			model.setDescription("Anda memiliki Task " + stagesConfig.getWaiting() + " Dari " + request.getEmployee_id()
					+ " - " + request.getEmployee_name());
			model.setDescriptionNotif("Task Dari " + request.getEmployee_id() + "-" + request.getEmployee_name());
			model.setInstancesId(instancesService.getInstancesLogById(new Long(instancesId)));
			model.setFlagRead("N");
			model.setBusinesProcess(businessProcessConfig.getChangeHours());

			notificationService.saveNotif(model);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	private void saveNotifLeave(Object object, Integer instancesId, Integer taskId) {
		LeaveRequest request = (LeaveRequest) object;
		try {
			Notifications model = new Notifications();
			model.setAssigner(trackingTaskApproval(String.valueOf(instancesId)));
			model.setCreatedBy(request.getEmployee_id());
			model.setCreatedDt(getNow());
			model.setStages(stagesConfig.getWaiting());
			model.setDescription("Anda memiliki Task " + stagesConfig.getWaiting() + " Dari " + request.getEmployee_id()
					+ " - " + request.getEmployee_name());
			model.setDescriptionNotif("Task Dari " + request.getEmployee_id() + "-" + request.getEmployee_name());
			model.setInstancesId(instancesService.getInstancesLogById(new Long(instancesId)));
			model.setFlagRead("N");
			if ("Y".equals(request.getCarryOverFlag())) {
				model.setBusinesProcess(businessProcessConfig.getCarry());
			} else {
				model.setBusinesProcess(businessProcessConfig.getLeave());
			}
			notificationService.saveNotif(model);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	private void saveNotifPermit(Object object, Integer instancesId, Integer taskId) {
		PermitRequest request = (PermitRequest) object;
		try {
			Notifications model = new Notifications();
			model.setAssigner(trackingTaskApproval(String.valueOf(instancesId)));
			model.setCreatedBy(request.getEmployee_id());
			model.setCreatedDt(getNow());
			model.setStages(stagesConfig.getWaiting());
			model.setDescription("Anda memiliki Task " + stagesConfig.getWaiting() + " Dari " + request.getEmployee_id()
					+ " - " + request.getEmployee_name());
			model.setDescriptionNotif("Task Dari " + request.getEmployee_id() + "-" + request.getEmployee_name());
			model.setInstancesId(instancesService.getInstancesLogById(new Long(instancesId)));
			model.setFlagRead("N");
			model.setBusinesProcess(businessProcessConfig.getPermit());
			notificationService.saveNotif(model);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public void saveLogsStages(String instancesId, String taskId, String stages, String username) {

		try {
			String createBy = getCreateByTaskByInstancesId(instancesId);
			if (null == createBy) {
				createBy = instancesService.getRequesterId(new Long(instancesId));
			}
			TaskLogs model = taskService.findData(createBy, instancesService.getInstancesLogById(new Long(instancesId)),
					new Long(taskId));
			if ("approve".equals(stages)) {
				saveLogsApprove(model, username);
			} else if ("reject".equals(stages)) {
				saveLogsReject(model, username);
			}
			updateIntancesLogs(instancesId);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public void saveNotifRemaining(Map<Object, Object> detailTask, ApproveRequest request) {
		try {
			InstancesLogs logs = instancesService.findByInstancesId(new Long(request.getInstancesId()));
			Notifications model = new Notifications();
			model.setAssigner(String.valueOf(detailTask.get("employee_id")));
			model.setBusinesProcess(logs.getBusinessProcess());
			model.setCreatedBy(request.getUsername());
			model.setCreatedDt(getNow());
			model.setDescription("Task Anda tidak bisa di approve karena total remaining tidak mencukupi !!!");
			model.setStages(stagesConfig.getWaiting());
			model.setFlagRead("N");
			model.setInstancesId(logs);
			notificationService.save(model);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	private void updateIntancesLogs(String instancesId) {
		Long taskId = getTaskIdByInstancesId(Integer.valueOf(instancesId));
		String trackingTaskApproval = trackingTaskApproval(instancesId);
		instancesService.updateApprove(trackingTaskApproval, getNow(), taskId, new Long(instancesId));
	}

	private void saveLogsApprove(TaskLogs param, String username) {
		try {
			TaskLogs model = new TaskLogs();
			String instancesId = String.valueOf(param.getInstanceId().getInstancesId());
			model.setCreatedBy(param.getCreatedBy());
			model.setCreatedDt(getNow());
			model.setInstanceId(param.getInstanceId());
			model.setUserStages(username);
			model.setStages(stagesConfig.getApprove());
			model.setTaskId(getTaskIdByInstancesId(Integer.valueOf(instancesId)));
			if (model.getTaskId() == 0) {
				model.setFlagTaskComplete("Y");
			} else {
				model.setFlagTaskComplete("N");
			}
			taskService.save(model);

			saveNotifApprove(model);
			if (getTaskIdByInstancesId(Integer.valueOf(instancesId)) > 0) {
				setWaitingApproved(model);
				saveNotifWaiting(model);
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	private void saveNotifApprove(TaskLogs logs) {
		try {
			String assigner = null;
			if (null == logs.getInstanceId().getAssigner()) {
				assigner = logs.getInstanceId().getRequesterId();
			} else {
				assigner = logs.getInstanceId().getAssigner();
			}
			Notifications notif = new Notifications();
			notif.setAssigner(logs.getInstanceId().getRequesterId());
			notif.setBusinesProcess(logs.getInstanceId().getBusinessProcess());
			notif.setCreatedBy(assigner);
			notif.setCreatedDt(getNow());
			notif.setDescription(
					"Task Anda Telah di " + logs.getStages() + " Oleh " + assigner + " - " + findName(assigner));
			notif.setDescriptionNotif(logs.getStages() + " Oleh " + assigner + "-" + findName(assigner));
			notif.setFlagRead("N");
			notif.setStages(logs.getStages());
			notif.setInstancesId(logs.getInstanceId());
			notificationService.saveNotif(notif);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public String findName(String username) {
		String fullName = null;

		try {
			HttpEntity<Object> entity = new HttpEntity<>(headerSettings.setupHeaderAccept());
			JBPM_MappingResponse task = mappingURL.findName(username);

			Object response = restTemplate.exchange(task.getUrl(), task.getMethod(), entity, Object.class);

			ResponseEntity<HashMap> responseEntity = (ResponseEntity<HashMap>) response;
			fullName = (String) responseEntity.getBody().get("fullname");

		} catch (Exception e) {
			e.printStackTrace();
		}

		return fullName;
	}

	private void saveNotifReject(TaskLogs logs) {
		try {
			String assigner = null;
			if (null == logs.getInstanceId().getAssigner()) {
				assigner = logs.getInstanceId().getRequesterId();
			} else {
				assigner = logs.getInstanceId().getAssigner();
			}
			Notifications notif = new Notifications();
			notif.setAssigner(logs.getInstanceId().getRequesterId());
			notif.setBusinesProcess(logs.getInstanceId().getBusinessProcess());
			notif.setCreatedBy(assigner);
			notif.setCreatedDt(getNow());
			notif.setDescription(
					"Task Anda Telah di " + logs.getStages() + " Oleh " + assigner + " - " + findName(assigner));
			notif.setDescriptionNotif(logs.getStages() + " Oleh " + assigner + "-" + findName(assigner));
			notif.setFlagRead("N");
			notif.setStages(stagesConfig.getReject());
			notif.setInstancesId(logs.getInstanceId());
			notificationService.saveNotif(notif);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	private void saveLogsReject(TaskLogs param, String username) {
		try {
			TaskLogs model = new TaskLogs();
			String instancesId = String.valueOf(param.getInstanceId().getInstancesId());
			model.setCreatedBy(param.getCreatedBy());
			model.setCreatedDt(getNow());
			model.setInstanceId(param.getInstanceId());
			model.setUserStages(username);
			model.setStages(stagesConfig.getReject());
			model.setTaskId(getTaskIdByInstancesId(Integer.valueOf(instancesId)));
			if (model.getTaskId() == 0) {
				model.setFlagTaskComplete("Y");
			} else {
				model.setFlagTaskComplete("N");
			}
			taskService.save(model);
			saveNotifReject(model);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	private void saveTaskPermit(Object object, Integer instancesId, Integer taskId) {
		try {
			PermitRequest request = (PermitRequest) object;
			TaskLogs model = new TaskLogs();
			model.setTaskId(Long.valueOf(taskId));
			model.setInstanceId(instancesService.getInstancesLogById(new Long(instancesId)));
			model.setCreatedBy(request.getEmployee_id());
			model.setCreatedDt(getNow());
			model.setUserStages(request.getEmployee_id());
			model.setStages(stagesConfig.getCreate());
			model.setFlagTaskComplete("N");
			taskService.save(model);

			setWaitingApprovalPermit(object, instancesId);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	private void setWaitingApprovalPermit(Object object, Integer instancesId) {
		try {
			PermitRequest request = (PermitRequest) object;
			TaskLogs model = new TaskLogs();
			model.setTaskId(getTaskIdByInstancesId(Integer.valueOf(instancesId)));
			model.setInstanceId(instancesService.getInstancesLogById(new Long(instancesId)));
			model.setCreatedBy(request.getEmployee_id());
			model.setCreatedDt(getNow());
			model.setUserStages(request.getEmployee_id());
			model.setStages(stagesConfig.getWaiting());
			model.setFlagTaskComplete("N");
			taskService.save(model);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	private void setWaitingApprovalLeave(Object object, Integer instancesId) {
		try {
			LeaveRequest request = (LeaveRequest) object;
			TaskLogs model = new TaskLogs();
			model.setTaskId(getTaskIdByInstancesId(Integer.valueOf(instancesId)));
			model.setInstanceId(instancesService.getInstancesLogById(new Long(instancesId)));
			model.setCreatedBy(request.getEmployee_id());
			model.setCreatedDt(getNow());
			model.setUserStages(request.getEmployee_id());
			model.setStages(stagesConfig.getWaiting());
			model.setFlagTaskComplete("N");
			taskService.save(model);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	private void setWaitingApprovalWorkHours(Object object, Integer instancesId) {
		try {
			ChangeWorkHoursRequest request = (ChangeWorkHoursRequest) object;
			TaskLogs model = new TaskLogs();
			model.setTaskId(getTaskIdByInstancesId(Integer.valueOf(instancesId)));
			model.setInstanceId(instancesService.getInstancesLogById(new Long(instancesId)));
			model.setCreatedBy(request.getEmployee_id());
			model.setCreatedDt(getNow());
			model.setUserStages(request.getEmployee_id());
			model.setStages(stagesConfig.getWaiting());
			model.setFlagTaskComplete("N");
			taskService.save(model);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	private void setWaitingApproved(TaskLogs object) {
		try {
			TaskLogs model = new TaskLogs();
			String instancesId = String.valueOf(object.getInstanceId().getInstancesId());
			model.setTaskId(getTaskIdByInstancesId(Integer.valueOf(instancesId)));
			model.setInstanceId(instancesService.getInstancesLogById(new Long(instancesId)));
			model.setCreatedBy(object.getCreatedBy());
			model.setCreatedDt(getNow());
			model.setUserStages(object.getCreatedBy());
			model.setStages(stagesConfig.getWaiting());
			model.setFlagTaskComplete("N");
			taskService.save(model);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	private void saveNotifWaiting(TaskLogs logs) {
		try {

			Notifications notif = new Notifications();
			notif.setAssigner(trackingTaskApproval(String.valueOf(logs.getInstanceId().getInstancesId())));
			notif.setBusinesProcess(logs.getInstanceId().getBusinessProcess());
			notif.setCreatedBy(logs.getInstanceId().getRequesterId());
			notif.setCreatedDt(getNow());
			notif.setDescription("Anda memiliki Task " + stagesConfig.getWaiting() + " Dari "
					+ logs.getInstanceId().getRequesterId() + " - " + logs.getInstanceId().getRequesterName());
			notif.setDescriptionNotif("Task Dari " + logs.getInstanceId().getRequesterId() + "-"
					+ logs.getInstanceId().getRequesterName());
			notif.setFlagRead("N");
			notif.setStages(stagesConfig.getWaiting());
			notif.setInstancesId(logs.getInstanceId());
			notificationService.saveNotif(notif);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public void saveInstancesWorkHours(Object object, Integer instancesId, Integer taskId) {
		try {
			ChangeWorkHoursRequest request = (ChangeWorkHoursRequest) object;
			InstancesLogs model = new InstancesLogs();
			model.setInstancesId(Long.valueOf(instancesId));
			model.setRequesterId(request.getEmployee_id());
			model.setRequesterName(request.getEmployee_name());
			model.setAssigner(trackingTaskApproval(String.valueOf(instancesId)));
			model.setSubject(null);
			model.setCreatedDt(getNow());
			model.setBusinessProcess(businessProcessConfig.getChangeHours());
			model.setAbsencesType(null);
			model.setAbsencesDesc(null);
			model.setStartDate(null);
			model.setEndDate(null);
			model.setReason(request.getReason());
			model.setApprovalDs1(request.getApproval_ds_1());
			model.setNameApprovalDs1(request.getApproval_ds_1_name());
			model.setApprovalDs2(request.getApproval_ds_2());
			model.setNameApprovalDs2(request.getApproval_ds_2_name());
			instancesService.save(model);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public void saveTaskWorkHours(Object object, Integer instancesId, Integer taskId) {

		try {
			ChangeWorkHoursRequest request = (ChangeWorkHoursRequest) object;
			TaskLogs model = new TaskLogs();
			model.setTaskId(Long.valueOf(taskId));
			model.setInstanceId(instancesService.getInstancesLogById(new Long(instancesId)));
			model.setCreatedBy(request.getEmployee_id());
			model.setCreatedDt(getNow());
			model.setUserStages(request.getEmployee_id());
			model.setStages(stagesConfig.getCreate());
			model.setFlagTaskComplete("N");
			taskService.save(model);

			setWaitingApprovalWorkHours(object, instancesId);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	private void saveInstancesPermit(Object object, Integer instancesId, Integer taskId) {
		try {
			PermitRequest request = (PermitRequest) object;
			InstancesLogs model = new InstancesLogs();
			model.setInstancesId(Long.valueOf(instancesId));
			model.setRequesterId(request.getEmployee_id());
			model.setRequesterName(request.getEmployee_name());
			model.setAssigner(trackingTaskApproval(String.valueOf(instancesId)));
			model.setSubject(request.getSubject());
			model.setCreatedDt(getNow());
			model.setBusinessProcess(businessProcessConfig.getPermit());
			model.setAbsencesType(request.getAbsences_type());
			model.setAbsencesDesc(request.getAbsenceDescription());
			model.setStartDate(request.getLeave_start_date());
			model.setEndDate(request.getLeave_end_date());
			model.setApprovalDs1(request.getApproval_ds_1());
			model.setNameApprovalDs1(request.getDs1_id());
			model.setApprovalDs2(request.getApproval_ds_2());
			model.setNameApprovalDs2(request.getDs2_id());
			instancesService.save(model);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	private void saveTaskLeave(Object object, Integer instancesId, Integer taskId) {
		try {
			LeaveRequest request = (LeaveRequest) object;
			TaskLogs model = new TaskLogs();
			model.setTaskId(Long.valueOf(taskId));
			model.setInstanceId(instancesService.getInstancesLogById(new Long(instancesId)));
			model.setCreatedBy(request.getEmployee_id());
			model.setCreatedDt(getNow());
			model.setUserStages(request.getEmployee_id());
			model.setStages(stagesConfig.getCreate());
			model.setFlagTaskComplete("N");
			taskService.save(model);

			setWaitingApprovalLeave(object, instancesId);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	private void saveInstancesLeave(Object object, Integer instancesId, Integer taskId) {
		try {
			LeaveRequest request = (LeaveRequest) object;
			InstancesLogs model = new InstancesLogs();
			model.setInstancesId(Long.valueOf(instancesId));
			model.setRequesterId(request.getEmployee_id());
			model.setRequesterName(request.getEmployee_name());
			model.setAssigner(trackingTaskApproval(String.valueOf(instancesId)));
			model.setSubject(request.getSubject());
			model.setCreatedDt(getNow());
			if ("Y".equals(request.getCarryOverFlag())) {
				model.setBusinessProcess(businessProcessConfig.getCarry());
			} else {
				model.setBusinessProcess(businessProcessConfig.getLeave());
			}
			model.setAbsencesType(request.getAbsences_type());
			model.setAbsencesDesc(request.getAbsenceDescription());
			model.setStartDate(request.getLeave_start_date());
			model.setEndDate(request.getLeave_end_date());
			model.setApprovalDs1(request.getApproval_ds_1());
			model.setNameApprovalDs1(request.getDs1_id());
			model.setApprovalDs2(request.getApproval_ds_2());
			model.setNameApprovalDs2(request.getDs2_id());
			instancesService.save(model);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Integer findTaskId(ResponseEntity<HashMap> responseActiveTask) {
		Map<String, Object> task = (Map<String, Object>) responseActiveTask.getBody().get("active-user-tasks");
		if (task != null) {
			List<Object> taskList = (List<Object>) task.get("task-summary");
			Map<String, Object> mapTask = (Map<String, Object>) taskList.get(0);
			Integer taskId = (Integer) mapTask.get("task-id");
			return taskId;
		} else {
			return 0;
		}
	}

	@SuppressWarnings({ "unused", "rawtypes" })
	private Long getTaskIdByInstancesId(Integer instancesId) {
		try {
			Map<Object, Object> map = new HashMap<Object, Object>();

			JBPM_MappingResponse activeTask = mappingURL.showActiveTaskByProcessId(Integer.valueOf(instancesId));
			ResponseEntity<HashMap> responseActiveTask = restTemplate.exchange(activeTask.getUrl(),
					activeTask.getMethod(), null, HashMap.class);
			Integer responseTaskId = findTaskId(responseActiveTask);
			return new Long(responseTaskId);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	private String getCreateByTaskByInstancesId(String instancesId) {
		try {
			JBPM_MappingResponse mapping = mappingURL.getValueCreateTaskByInstancesId(new Long(instancesId));
			HttpEntity<String> requestEntity = new HttpEntity<>(headerSettings.setupHeaderAccept());

			Object response = restTemplate.exchange(mapping.getUrl(), mapping.getMethod(), requestEntity, Object.class);

			ResponseEntity<String> responseEntity = (ResponseEntity<String>) response;
			String createBy = responseEntity.getBody();
			return createBy;
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}

	public String getTracktingTaskApproval(String instancesId) {
		return trackingTaskApproval(instancesId);
	}

	@SuppressWarnings({ "unused", "rawtypes", "unchecked" })
	private String trackingTaskApproval(String instancesId) {
		try {
			Map<Object, Object> map = new HashMap<Object, Object>();

			JBPM_MappingResponse activeTask = mappingURL.getTrackingTaskByInstancesId(new Long(instancesId));
			ResponseEntity<HashMap> responseActiveTask = restTemplate.exchange(activeTask.getUrl(),
					activeTask.getMethod(), null, HashMap.class);
			List<Object> trackingTask = (List<Object>) responseActiveTask.getBody().get("node-instance");
			if (trackingTask.size() == 0) {
				return null;
			}
			Map<String, Object> mapTask = (Map<String, Object>) trackingTask.get(0);
			String trackingApproval = (String) mapTask.get("node-name");

			return getValueVariable(instancesId, trackingApproval);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
	}

	@SuppressWarnings({ "unused", "unchecked" })
	private String getValueVariable(String instancesId, String variable) {
		try {
			Map<Object, Object> map = new HashMap<Object, Object>();

			JBPM_MappingResponse mapping = mappingURL.getValueApproval(new Long(instancesId), variable);
			HttpEntity<String> requestEntity = new HttpEntity<>(headerSettings.setupHeaderAccept());

			Object response = restTemplate.exchange(mapping.getUrl(), mapping.getMethod(), requestEntity, Object.class);

			ResponseEntity<String> responseEntity = (ResponseEntity<String>) response;
			String valueVariable = responseEntity.getBody();
			return valueVariable;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private boolean checkBlockLeave(String username) {
		boolean resp = false;
		try {
			HttpEntity<Object> entity = new HttpEntity<>(headerSettings.setupHeaderAccept());
			JBPM_MappingResponse task = mappingURL.checkBlockLeave(username);

			Object response = restTemplate.exchange(task.getUrl(), task.getMethod(), entity, Object.class);

			ResponseEntity<HashMap> responseEntity = (ResponseEntity<HashMap>) response;
			resp = (boolean) responseEntity.getBody().get("flag");

			return resp;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return resp;
	}

	@SuppressWarnings({ "unchecked", "rawtypes", "deprecation" })
	public Boolean validationAbsencesType(PermitRequest request) {

		Boolean flag = false;
		if ("2101".equals(request.getAbsences_type())) {
			JBPM_MappingResponse mapping = mappingURL.findByAbsenceType(request.getEmployee_id(),
					request.getAbsences_type());
			HttpEntity<String> requestEntity = new HttpEntity<>(headerSettings.setupHeader());
			Object response = restTemplate.exchange(mapping.getUrl(), mapping.getMethod(), requestEntity, Object.class);
			try {
				ResponseEntity<HashMap> responseEntity = (ResponseEntity<HashMap>) response;
				List<Object> list = (List<Object>) responseEntity.getBody().get("listResponse");
				List<String> listCreatedDate = new ArrayList<String>();
				List<String> listMonth = new ArrayList<String>();
				for (int i = 0; i < list.size(); i++) {
					LinkedHashMap<String, Object> obj = (LinkedHashMap<String, Object>) list.get(i);
					LinkedHashMap<String, String> objId = (LinkedHashMap<String, String>) obj.get("id");
					String createdDate = objId.get("startDate");
					listCreatedDate.add(parseDate(createdDate));
					String dated = parseDate(createdDate);
					SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
					Date date = format.parse(dated);
					listMonth.add(String.valueOf(date.getMonth() + 1));
				}
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
				Date date = format.parse(parseDate(request.getLeave_start_date()));
				// validation 1 month tanpa surat dokter
				if (!listMonth.isEmpty()) {
					for (int i = 0; i < listMonth.size(); i++) {
						Long minova = new Long(listMonth.get(i));
						Long req = new Long(date.getMonth() + 1);
						if (minova == req || minova.equals(req)) {
							flag = true;
						}
					}
				}
				if (list.size() == 7) {
					flag = true;
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return flag;
	}

	@SuppressWarnings({ "unchecked", "rawtypes", "unused" })
	public Boolean validationAbsencesType(LeaveRequest request) {

		Boolean flag = false;
		String absenceType = null;

		if ("3150".equals(request.getAbsences_type())) {
			JBPM_MappingResponse mapping = mappingURL.findByAbsenceType(request.getEmployee_id(),
					request.getAbsences_type());
			HttpEntity<String> requestEntity = new HttpEntity<>(headerSettings.setupHeader());
			Object response = restTemplate.exchange(mapping.getUrl(), mapping.getMethod(), requestEntity, Object.class);

			try {
				ResponseEntity<HashMap> responseEntity = (ResponseEntity<HashMap>) response;
				List<HashMap<String, Object>> listModel = (List<HashMap<String, Object>>) responseEntity.getBody()
						.get("listResponse");
				for (Iterator iterator = listModel.iterator(); iterator.hasNext();) {
					HashMap<String, Object> model = (HashMap<String, Object>) iterator.next();
					absenceType = String.valueOf(model.get("absenceType"));
				}
				if (absenceType != null) {
					flag = true;
				} else {
					flag = false;
				}

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}

		}
		return flag;
	}

	@SuppressWarnings({ "unchecked", "rawtypes", "deprecation" })
	public Boolean validationAbsencesType(String employeeId, String absenceType, String leaveStartDate) {

		Boolean flag = false;
		if ("2101".equals(absenceType)) {
			JBPM_MappingResponse mapping = mappingURL.findByAbsenceType(employeeId, absenceType);
			HttpEntity<String> requestEntity = new HttpEntity<>(headerSettings.setupHeader());
			Object response = restTemplate.exchange(mapping.getUrl(), mapping.getMethod(), requestEntity, Object.class);
			try {
				ResponseEntity<HashMap> responseEntity = (ResponseEntity<HashMap>) response;
				List<Object> list = (List<Object>) responseEntity.getBody().get("listResponse");
				List<String> listCreatedDate = new ArrayList<String>();
				List<String> listMonth = new ArrayList<String>();
				for (int i = 0; i < list.size(); i++) {
					LinkedHashMap<String, String> obj = (LinkedHashMap<String, String>) list.get(i);
					String createdDate = obj.get("createdDate");
					listCreatedDate.add(parseDate(createdDate));
					String dated = parseDate(createdDate);
					SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
					Date date = format.parse(dated);
					listMonth.add(String.valueOf(date.getMonth() + 1));
				}
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
				Date date = format.parse(parseDate(leaveStartDate));
				// validation 1 month tanpa surat dokter
				if (!listMonth.isEmpty()) {
					for (int i = 0; i < listMonth.size(); i++) {
						Long minova = new Long(listMonth.get(i));
						Long req = new Long(date.getMonth() + 1);
						if (minova == req || minova.equals(req)) {
							flag = true;
						}
					}
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return flag;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Boolean validationCheckDeduction(LeaveRequest request) {

		Boolean flag = false;

		try {
			JBPM_MappingResponse mapping = mappingURL.checkDeduction(request.getEmployee_id(),
					request.getLeave_start_date(), request.getLeave_end_date());
			HttpEntity<String> requestEntity = new HttpEntity<String>(headerSettings.setupHeader());
			Object response = restTemplate.exchange(mapping.getUrl(), mapping.getMethod(), requestEntity, Object.class);
			ResponseEntity<HashMap> responseEntity = (ResponseEntity<HashMap>) response;
			flag = (Boolean) responseEntity.getBody().get("message");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Boolean validationCheckDeduction(PermitRequest request) {

		Boolean flag = false;

		try {
			JBPM_MappingResponse mapping = mappingURL.checkDeduction(request.getEmployee_id(),
					request.getLeave_start_date(), request.getLeave_end_date());
			HttpEntity<String> requestEntity = new HttpEntity<String>(headerSettings.setupHeader());
			Object response = restTemplate.exchange(mapping.getUrl(), mapping.getMethod(), requestEntity, Object.class);
			ResponseEntity<HashMap> responseEntity = (ResponseEntity<HashMap>) response;
			flag = (Boolean) responseEntity.getBody().get("message");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Boolean validationFindDateHistory(HistoryDateRequest request) {

		Boolean flag = false;

		try {
			JBPM_MappingResponse mapping = mappingURL.findDateHistory();
			HttpEntity<HistoryDateRequest> requestEntity = new HttpEntity<>(request, headerSettings.setupHeader());
			Object response = restTemplate.exchange(mapping.getUrl(), mapping.getMethod(), requestEntity, Object.class);
			ResponseEntity<HashMap> responseEntity = (ResponseEntity<HashMap>) response;
			flag = (Boolean) responseEntity.getBody().get("flagHistory");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Map<String, Object> validateAbsencesType(String absenceType, String quotaTaken, String username) {
		Map<String, Object> resp = new HashMap<String, Object>();
		try {
			HttpEntity<Object> entity = new HttpEntity<>(headerSettings.setupHeaderAccept());
			JBPM_MappingResponse task = mappingURL.getLeaveQuota(absenceType);

			Object response = restTemplate.exchange(task.getUrl(), task.getMethod(), entity, Object.class);

			ResponseEntity<HashMap> responseEntity = (ResponseEntity<HashMap>) response;
			Integer status = (Integer) responseEntity.getBody().get("status");
			String leaveQuota = (String) responseEntity.getBody().get("leaveQuota");
			String leaveDesc = (String) responseEntity.getBody().get("leaveDesc");
			String quotaDeduction = (String) responseEntity.getBody().get("quotadeduction");
			Long quotaTakeMinova = new Long(quotaTaken);

			if (200 == status && !"Y".equals(quotaDeduction)) {
				Long leaveQuotaMinova = null;
				if (leaveQuota == null || "".equals(leaveQuota)) {
					leaveQuotaMinova = new Long("0");
				} else {
					leaveQuotaMinova = new Long(leaveQuota);
				}
				quotaTakeMinova = new Long(quotaTaken);
				if (quotaTakeMinova <= leaveQuotaMinova) {
					resp.put("flagAbsence", false);
				} else {
					resp.put("flagAbsence", true);
					resp.put("message", "Quota yang diambil harus lebih kecil atau sama dengan quota " + leaveDesc
							+ " yakni : " + leaveQuota + " Hari !!!");
					resp.put("status", 400);
				}
			} else {
				HttpEntity<Object> entity2 = new HttpEntity<>(headerSettings.setupHeaderAccept());
				JBPM_MappingResponse task2 = mappingURL.findLastRemaining(username);

				Object response2 = restTemplate.exchange(task2.getUrl(), task2.getMethod(), entity2, Object.class);

				ResponseEntity<HashMap> responseEntity2 = (ResponseEntity<HashMap>) response2;
				String remain = (String) responseEntity2.getBody().get("remaining");

				Long leaveQuotaMinova = null;
				if (remain == null || "".equals(remain)) {
					leaveQuotaMinova = new Long("0");
				} else {
					leaveQuotaMinova = new Long(remain);
				}
				quotaTakeMinova = new Long(quotaTaken);
				if (quotaTakeMinova <= leaveQuotaMinova) {
					resp.put("flagAbsence", false);
				} else {
					resp.put("flagAbsence", true);
					resp.put("message", "Quota yang diambil harus lebih kecil atau sama dengan quota " + leaveDesc
							+ " yakni : " + remain + " Hari !!!");
					resp.put("status", 400);
				}

			}

			return resp;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return resp;
	}

	public Map<String, Object> validationBlockLeave(String username, String remainingQuota, String quotaTaken) {
		Map<String, Object> map = new HashMap<String, Object>();
		if (false == checkBlockLeave(username)) {
			Integer block = Integer.valueOf(remainingQuota) - Integer.valueOf(quotaTaken);
			Integer days = Integer.valueOf(remainingQuota) - 5;
			if (block < 5 && block >= 1) {
				map.put("message", "Anda Harus mengambil block leave atau " + days + " hari");
				map.put("status", 400);
				map.put("blockLeave", true);
			} else {
				map.put("blockLeave", false);
			}
		} else {
			map.put("blockLeave", false);
		}
		return map;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Map<String, Object> InformationLogin(Map<String, Object> map, String username) {
		try {
			HttpEntity<Object> entity = new HttpEntity<>(headerSettings.setupHeaderAccept());
			JBPM_MappingResponse task = mappingURL.getInformationLogin(username);

			Object response = restTemplate.exchange(task.getUrl(), task.getMethod(), entity, Object.class);

			ResponseEntity<HashMap> responseEntity = ((ResponseEntity<HashMap>) response);
			String fullname = (String) responseEntity.getBody().get("fullname");
			String jobDesc = (String) responseEntity.getBody().get("jobDes");

			map.put("fullname", fullname);
			map.put("jobDesc", jobDesc);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return map;
	}

	public Map<String, Object> listMenuAccess(Map<String, Object> map, String username) {
		try {
			RoleUser role = roleService.findUserByUsername(username);
			if (role == null) {
				RoleUser defaultUser = roleService.findUserByUsername("default");
				map.put("listMenuAccess", defaultUser.getRole().getListMenu());
			} else {
				map.put("listMenuAccess", role.getRole().getListMenu());
			}

		} catch (Exception e) {
			map.put("error", e.getMessage());
		}

		return map;
	}

	public String parseDate(String strDate) {
		String formateDateStr = "";
		try {
			if (strDate != null && strDate != "") {
				String formateYear = String.valueOf(strDate.subSequence(0, 4));
				String formateMonth = String.valueOf(strDate.subSequence(4, 6));
				String formateDate = String.valueOf(strDate.subSequence(6, 8));
				formateDateStr = formateYear.concat("-").concat(formateMonth).concat("-").concat(formateDate);
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return formateDateStr;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Map<String, Object> findUsernameByExternalId(Map<String, Object> map, String externalId) {
		try {
			HttpEntity<Object> entity = new HttpEntity<>(headerSettings.setupHeaderAccept());
			JBPM_MappingResponse task = mappingURL.findUsernameByExternalId(externalId);

			Object response = restTemplate.exchange(task.getUrl(), task.getMethod(), entity, Object.class);

			ResponseEntity<HashMap> responseEntity = ((ResponseEntity<HashMap>) response);
			String username = (String) responseEntity.getBody().get("username");
			if (null != username) {
				map.put("externalId", externalId);
				map.put("username", username);
			} else {
				map.put("externalId", null);
				map.put("username", externalId);
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return map;
	}

	public Timestamp getNow() {
		Timestamp now = new Timestamp(System.currentTimeMillis());
		return now;
	}

	public String formateTimestamp(String timestamp) {
		String dates = null;
		try {
			if (timestamp != null && !"null".equals(timestamp)) {
				Date date = format.parse(timestamp);
				dates = format.format(date);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return dates;
	}

	public String formateTimestamp(Timestamp timestamp) {
		if (timestamp == null) {
			return null;
		} else {
			Date date = new Date(timestamp.getTime());
			return format2.format(date);
		}
	}

	public String formatDisplay(String strDate) {
		String formatDt = null;
		try {
			formatDt = formatDisplay(strDate);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return formatDt;
	}

	public Pageable validatePageable(Pageable pageable, String sortDefault) {
		Pageable page = null;
		if (null != pageable.getSort()) {
			String dataPage = String.valueOf(pageable.getSort());
			String[] split = dataPage.split(":");
			if (split.length == 3) {
				if (" ASC".equals(split[1])) {
					PageRequest request = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(),
							new Sort(Direction.ASC, split[0]));
					page = request;
				} else {
					PageRequest request = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(),
							new Sort(Direction.DESC, split[0]));
					page = request;
				}
			} else {
				page = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(), pageable.getSort());
			}
		} else {
			page = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(),
					new Sort(Direction.DESC, sortDefault));
		}
		return page;
	}

	public Map<String, Object> validationDateRequest(String requesterId, Integer countDaysRequest) {
		Map<String, Object> map = notificationsController.validationListDate(requesterId, countDaysRequest);
		return map;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public boolean validationEmployeeOffice(String empId) {
		Object response = null;
		Boolean flag = false;

		HttpEntity<Object> requestEntity = new HttpEntity<>(headerSettings.setupHeaderAccept());
		JBPM_MappingResponse mapping = mappingURL.findListBranchById(empId);
		response = restTemplate.exchange(mapping.getUrl(), mapping.getMethod(), requestEntity, Object.class);
		try {
			ResponseEntity<HashMap> responseEntity = (ResponseEntity<HashMap>) response;
			Object listBranch = responseEntity.getBody().get("listResponse");
			LinkedHashMap<String, Object> linkModel = (LinkedHashMap<String, Object>) listBranch;
			LinkedHashMap<String, Object> linkId = (LinkedHashMap<String, Object>) linkModel.get("id");

			String empArea = String.valueOf(linkId.get("empArea"));
			String empSubArea = String.valueOf(linkId.get("empSubArea"));

			if (empArea.equals("0001") && empSubArea.equals("0888")) {
				flag = true;
			} else {
				flag = false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}

	public String getDate() {
		String formatDt = null;
		try {
			Date date = Calendar.getInstance().getTime();
			formatDt = format2.format(date);
		} catch (Exception e) {
			e.printStackTrace();
			e.getMessage();
		}
		return formatDt;
	}

	public String parseHoursInput() {
		String formateDateStr = null;
		try {
			Date date = Calendar.getInstance().getTime();
			formateDateStr = formatTime.format(date);
			String formateMinute = String.valueOf(formateDateStr.subSequence(0, 2));

			if (formateDateStr != null) {
				formateDateStr = formateMinute;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return formateDateStr;
	}

	public String parseTimeInput() {
		String formateDateStr = null;
		try {
			Date date = Calendar.getInstance().getTime();
			formateDateStr = formatTime.format(date);
			String formateMinute = String.valueOf(formateDateStr.subSequence(3, 5));

			if (formateDateStr != null) {
				formateDateStr = formateMinute;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return formateDateStr;
	}

	public String parseHoursBarcode(String strDate) {
		String formateDateStr = null;
		try {
			String formateMinute = String.valueOf(strDate.subSequence(8, 10));

			if (strDate != null && strDate != "") {
				formateDateStr = formateMinute;

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return formateDateStr;
	}

	public String parseTimeBarcode(String strDate) {
		String formateDateStr = null;
		try {
			String formateMinute = String.valueOf(strDate.subSequence(10, 12));

			if (strDate != null && strDate != "") {
				formateDateStr = formateMinute;

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return formateDateStr;
	}

	public boolean validationBarcode(AttendanceRequest request) {
		Boolean flag = false;

		if (request.getBarcodeTime() != null) {
			String barcode = parseHoursBarcode(request.getBarcodeTime());
			String jam = parseHoursInput();

			String inTime = parseTimeInput();
			String inBarcode = parseTimeBarcode(request.getBarcodeTime());
			Integer range = Integer.valueOf(inTime) - Integer.valueOf(inBarcode);
			Integer range2 = Integer.valueOf(inBarcode) - Integer.valueOf(inTime);

			if (barcode.equals(jam)) {
				if (range <= 1 && range >= 0 || range2 >= -1 && range2 <= 0) {
					flag = true;
				} else {
					flag = false;
				}
				return flag;
			}
		} else {
			flag = true;
		}
		return flag;
	}

	public boolean validateDistance(AttendanceRequest request) {
		Boolean flag = false;
		BranchConf model = new BranchConf();
		BranchIdRequest req = new BranchIdRequest();
		String reqRad = request.getRadius();

		try {
			req.setEmpArea(request.getEmpArea());
			req.setEmpSubArea(request.getEmpSubArea());
			req.setLandscape(request.getId().getLandscape());
			model = branchConfService.findBranchByID(req);

			Float radius = new Float(0);
			if (model.getRadius() != null) {
				radius = Float.valueOf(model.getRadius());
			} else {
				radius = Float.valueOf(reqRad);
			}
			String radReq = String.valueOf(reqRad.subSequence(0, 2));
			Float radiusReq = Float.valueOf(radReq);

			if (radiusReq <= radius) {
				flag = true;
			} else {
				flag = false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			e.getMessage();
		}
		return flag;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public boolean validateById(AttendanceRequest request) {
		Boolean flag = false;
		Object response = null;

		HttpEntity<Object> requestEntity = new HttpEntity<>(headerSettings.setupHeaderAccept());
		JBPM_MappingResponse mapping = mappingURL.findListBranchById(request.getId().getEmpId());
		response = restTemplate.exchange(mapping.getUrl(), mapping.getMethod(), requestEntity, Object.class);
		try {
			ResponseEntity<HashMap> responseEntity = (ResponseEntity<HashMap>) response;
			Object listBranch = responseEntity.getBody().get("listResponse");
			LinkedHashMap<String, Object> linkModel = (LinkedHashMap<String, Object>) listBranch;
			LinkedHashMap<String, Object> linkId = (LinkedHashMap<String, Object>) linkModel.get("id");

			String empArea = String.valueOf(linkId.get("empArea"));
			String empSubArea = String.valueOf(linkId.get("empSubArea"));

			if (empArea.equals(request.getEmpArea()) && empSubArea.equals(request.getEmpSubArea())) {
				flag = true;
			} else {
				flag = false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			e.getMessage();
		}
		return flag;
	}

	@SuppressWarnings("rawtypes")
	public Integer cekTotalDate(String startDt, String endDt) {
		try {
			JBPM_MappingResponse activeTask = mappingURL.getHolidayBetween(startDt, endDt);
			ResponseEntity<HashMap> responseActiveTask = restTemplate.exchange(activeTask.getUrl(),
					activeTask.getMethod(), null, HashMap.class);
			String count = String.valueOf(responseActiveTask.getBody().get("total"));
			return Integer.parseInt(count);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return 0;
	}

	public String getPaths() {
		try {
			Calendar calendar = new GregorianCalendar();
			TimeZone timeZone = calendar.getTimeZone();
			calendar.setTimeZone(timeZone);
			int years = calendar.get(Calendar.YEAR);
			int month = calendar.get(Calendar.MONTH) + 1;
			String months = null;
			if (month < 10) {
				months = "0".concat(String.valueOf(month));
			} else {
				months = String.valueOf(month);
			}
			String separator = String.valueOf(years).concat("/").concat(months).concat("/");

			File folderDin = new File(uploadpathconfig.getLocation_document() + separator);
			Path pathDin = Paths.get(String.valueOf(folderDin));
			return String.valueOf(pathDin);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public void uploadfileSelfieRezice(@RequestBody UploadFileAttendanceRequest request)
			throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {

		List<String> listFilename = new ArrayList<String>();
		try {
			byte[] bytePhoto = DatatypeConverter.parseBase64Binary(request.getContent());

			BufferedImage originalImage = ImageIO.read(new ByteArrayInputStream(bytePhoto));
			int type = originalImage.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : originalImage.getType();
			BufferedImage image = resizeImage(originalImage, type);

			String filename = request.getCreatedBy().concat("-").concat(formateTimestamp(getNow())).concat("-")
					.concat(request.getStatus());
			Path path = Paths.get(getPaths() + File.separator + filename);
			if (!Files.exists(path)) {
				Files.createDirectories(path);
			} else {
				Files.deleteIfExists(path);
				Files.createDirectories(path);
			}

			File file = new File(getPaths() + File.separator + filename);

			ImageIO.write(image, "PNG", file);
			request.setFileSize(String.valueOf(file.length()));
			listFilename.add(filename);

			zip(getPaths(), file, filename);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	public void removeImages(@RequestBody UploadFileAttendanceRequest request)
			throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {

		try {

			String filename = request.getCreatedBy().concat("-").concat(formateTimestamp(getNow())).concat("-").concat(request.getStatus());
			Path path = Paths.get(getPaths() + File.separator + filename);
			if (!Files.exists(path)) {
				Files.createDirectories(path);
			} else {
				Files.deleteIfExists(path);
				Files.createDirectories(path);
			}

			File file = new File(getPaths() + File.separator + filename);
			file.delete();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@SuppressWarnings("static-access")
	private void zip(String path, File files, String filename) throws IOException, FileNotFoundException {

		zout = new ZipOutputStream(
				new BufferedOutputStream(new FileOutputStream(path + files.separator + filename.concat(".zip"))));
		zout.setLevel(Deflater.BEST_COMPRESSION);

		File file = new File(path + files.separator + filename);
		if (!file.exists()) {
			System.out.println("File " + file.getAbsolutePath() + " not found ");
			System.out.println("Aborted.");
			return;
		}
		ZipEntry ze = new ZipEntry(path + files.separator + filename);
		zout.putNextEntry(ze);

		BufferedInputStream buffin = new BufferedInputStream(new FileInputStream(file));

		byte[] buffer = new byte[1024];
		int count = -1;
		while ((count = buffin.read(buffer)) != -1) {
			zout.write(buffer, 0, count);
		}
		buffin.close();

		zout.closeEntry();
		zout.close();
	}

	private static BufferedImage resizeImage(BufferedImage originalImage, int type) {
		int IMG_WIDTH = 700;
		int IMG_CLAHEIGHT = 600;
		BufferedImage resizedImage = new BufferedImage(IMG_WIDTH, IMG_CLAHEIGHT, type);
		Graphics2D g = resizedImage.createGraphics();
		g.drawImage(originalImage, 0, 0, IMG_WIDTH, IMG_CLAHEIGHT, null);
		g.dispose();
		return resizedImage;
	}

	public static byte[] compress(byte[] data, int compressionLevel, boolean format) throws IOException {

		Deflater deflater = new Deflater(compressionLevel, format);
		deflater.setInput(data);
		deflater.finish();

		ByteArrayOutputStream baout = new ByteArrayOutputStream();
		byte[] buff = new byte[1024];
		int count = 0;

		while (!deflater.finished()) {
			count = deflater.deflate(buff);
			if (count > 0)
				baout.write(buff, 0, count);
		}
		deflater.end();
		return baout.toByteArray();
	}
	
}
