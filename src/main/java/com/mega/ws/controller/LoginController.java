package com.mega.ws.controller;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.DatatypeConverter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.mega.ws.login.Output;
import com.mega.ws.login.Response;
import com.mega.ws.config.LdapConfig;
import com.mega.ws.request.LoginRequest;

@RestController
public class LoginController extends BaseController {

	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(LoginController.class);

	@Autowired
	LdapConfig ldapConfig;

	// login ldap baru

	@PostMapping(value = "/login")
	@ResponseBody
	public Map<String, Object> login(@RequestBody LoginRequest request)
			throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		Map<String, Object> map = new HashMap<String, Object>();

		String enc = encrypt(request.getPassword());

		Output response = callService(request.getUsername(), enc);

		if ("00".equals(response.ResponseCode) && "SUCCESS".equals(response.ResponseDescription)) {
			map.put("status", response.ResponseCode);
			map.put("message", response.ResponseDescription);
			findUsernameByExternalId(map, request.getUsername());
			boolean isFlag = false;
			if (null != map.get("username") && null != map.get("externalId")) {
				if (!map.get("username").equals(map.get("externalId"))) {
					isFlag = true;
				}
			}
			if (isFlag) {
				InformationLogin(map, String.valueOf(map.get("username")));
				listMenuAccess(map, String.valueOf(map.get("username")));
			}else {
				InformationLogin(map, request.getUsername());
				listMenuAccess(map, request.getUsername());
			}
		} else {
			map.put("status", response.ResponseCode);
			map.put("message", "Username or password is invalid !!!");
		}
		return map;
	}

	@PostMapping(value = "/login-bypass")
	@ResponseBody
	public Map<String, Object> loginByPass(@RequestBody LoginRequest request)
			throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		Map<String, Object> map = new HashMap<String, Object>();

		map.put("status", "00");
		map.put("message", "SUCCESS");
		findUsernameByExternalId(map, request.getUsername());
		
		boolean isFlag = false;
		if (null != map.get("username") && null != map.get("externalId")) {
			if (!map.get("username").equals(map.get("externalId"))) {
				isFlag = true;
			}
		}
		if (isFlag) {
			InformationLogin(map, String.valueOf(map.get("username")));
			listMenuAccess(map, String.valueOf(map.get("username")));
		}else {
			InformationLogin(map, request.getUsername());
			listMenuAccess(map, request.getUsername());
		}
		
		
		return map;
	}
	
	@PostMapping(value = "/login-portal")
	@ResponseBody
	public Map<String, Object> loginByPortal(@RequestBody LoginRequest request)
			throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		Map<String, Object> map = new HashMap<String, Object>();

		map.put("status", "00");
		map.put("message", "SUCCESS");
		findUsernameByExternalId(map, request.getUsername());
		InformationLogin(map, request.getUsername());
		listMenuAccess(map, request.getUsername());
		
		return map;
	}

	// encrypt password
	@SuppressWarnings({ "rawtypes", "unchecked", "unused" })
	private static String encrypt(String password) {
		String salt = "gek123";
		String secret = "iknowwhatyoudidlastnigth";

		try {
			if (salt.length() > 2) {
				salt = salt.substring(0, 2);
			}
			int[] bsalt = arraySliceUnpack("\0" + salt);
			int[] bpwd = arraySliceUnpack("\0" + password);
			int[] digest = unpackPack(md5(salt + secret));
			int len = salt.length() + (digest.length * (bpwd.length + 16) / 16);
			int i = 0;
			int p = 0;
			int j = 0;
			ArrayList result = new ArrayList();
			for (; i < bsalt.length; i++) {
				result.add(bsalt[i]);
			}
			for (; i < len; i++) {
				if (p < bpwd.length) {
					result.add(bpwd[p] ^ digest[(j % digest.length)]);
				} else {
					result.add(0 ^ digest[(j % digest.length)]);
				}
				j++;
				p++;
			}
			String hash = "";
			byte[] b = new byte[result.size()];
			for (int z = 0; z < result.size(); z++) {
				b[z] = (byte) ((int) result.get(z));
			}
			String base64Hash = DatatypeConverter.printBase64Binary(b);
			return new String(base64Hash);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private static int[] arraySliceUnpack(String data) {
		byte[] bytes = data.getBytes();
		int[] num = new int[bytes.length - 1];
		for (int i = 0; i < bytes.length; i++) {
			if (i < bytes.length - 1) {
				num[i] = bytes[i + 1];
			}
		}
		return num;
	}

	private static int[] unpackPack(String hex) {
		byte[] input = DatatypeConverter.parseHexBinary(hex);
		int[] num = new int[input.length];
		for (int i = 0; i < input.length; i++) {
			num[i] = input[i] & 0xFF;
		}
		return num;
	}

	private static String md5(String data) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] array = md.digest(data.getBytes());
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < array.length; ++i) {
				sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
			}
			return sb.toString();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	// call SOAP Service
	public static Output callService(String nip, String pass) {
		String ServiceName = "SERVICE_VERIFY";
		String ClientId = "47715538-aff8-4fc6-a8e4-ccf9531bbe28";
		String Signature = "Z2V2nmgXawzm+14H7lkbwzVbJds6QSJPo6QIQrwQXZo=";

		// Code to make a webservice HTTP request
		String responseString = "";
		String outputString = "";
		String wsEndPoint = "http://10.14.18.159:8080/middleware/services/ServicePortTypeBndPortServiceLdap";
		try {
			URL url = new URL(wsEndPoint);
			URLConnection connection = url.openConnection();
			HttpURLConnection httpConn = (HttpURLConnection) connection;
			ByteArrayOutputStream bout = new ByteArrayOutputStream();
			String xmlInput = "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ns1=\"http://ldap.service.ws.mega.com/ServiceLdap\"><SOAP-ENV:Body><ns1:getServiceLdap><input><ServiceName>"
					+ ServiceName + "</ServiceName><ClientId>" + ClientId + "</ClientId><Signature>" + Signature
					+ "</Signature><ArrRequest><item><Request>cn=" + nip
					+ " , o=megauser</Request></item><item><Request>" + pass
					+ "</Request></item></ArrRequest></input></ns1:getServiceLdap></SOAP-ENV:Body></SOAP-ENV:Envelope>";
			byte[] buffer = new byte[xmlInput.length()];
			buffer = xmlInput.getBytes();
			bout.write(buffer);
			byte[] b = bout.toByteArray();
			String SOAPAction = "getServiceLdap";
			httpConn.setRequestProperty("Content-Length", String.valueOf(b.length));
			httpConn.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
			httpConn.setRequestProperty("SOAPAction", SOAPAction);
			httpConn.setRequestMethod("POST");
			httpConn.setDoOutput(true);
			httpConn.setDoInput(true);

			OutputStream out = httpConn.getOutputStream();
			// Write the content of the request to the outputstream of the HTTP
			// Connection.
			out.write(b);
			out.close();
			// Ready with sending the request.
			// Read the response.
			InputStreamReader isr = new InputStreamReader(httpConn.getInputStream(), StandardCharsets.UTF_8);
			BufferedReader in = new BufferedReader(isr);
			// Write the SOAP message response to a String.
			while ((responseString = in.readLine()) != null) {
				outputString = outputString + responseString;
			}
			// Write the SOAP message formatted to the console.
			String formattedSOAPResponse = formatXML(outputString);

			SOAPMessage message = MessageFactory.newInstance().createMessage(null,
					new ByteArrayInputStream(formattedSOAPResponse.getBytes()));
			Unmarshaller unmarshaller = JAXBContext.newInstance(Response.class).createUnmarshaller();
			Response rs = (Response) unmarshaller.unmarshal(message.getSOAPBody().extractContentAsDocument());

			return rs.output;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	// format the XML in pretty String
	private static String formatXML(String unformattedXml) {
		try {
			Document document = parseXmlFile(unformattedXml);
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			transformerFactory.setAttribute("indent-number", 3);
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			DOMSource source = new DOMSource(document);
			StreamResult xmlOutput = new StreamResult(new StringWriter());
			transformer.transform(source, xmlOutput);
			return xmlOutput.getWriter().toString();
		} catch (TransformerException e) {
			throw new RuntimeException(e);
		}
	}

	// parse XML
	private static Document parseXmlFile(String in) {
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			InputSource is = new InputSource(new StringReader(in));
			return db.parse(is);
		} catch (IOException | ParserConfigurationException | SAXException e) {
			throw new RuntimeException(e);
		}
	}

	// login ldap lama

	/*
	 * @SuppressWarnings({ "unchecked", "unused" })
	 * 
	 * @PostMapping(value = "/login")
	 * 
	 * @ResponseBody public Map<String, Object> login(@RequestBody LoginRequest
	 * request) throws NoSuchFieldException, SecurityException,
	 * IllegalArgumentException, IllegalAccessException{ Map<String,Object> map =
	 * new HashMap<String,Object>();
	 * 
	 * RestTemplate restTemplate = new RestTemplate(); HttpHeaders headers = new
	 * HttpHeaders(); headers.setContentType(MediaType.APPLICATION_JSON);
	 * HttpEntity<?> requestEntity = new HttpEntity<>(request, headers);
	 * 
	 * List<NameValuePair> form = new ArrayList<>();
	 * 
	 * form.add(new BasicNameValuePair("operation",
	 * ldapConfig.getVerifyPassword())); form.add(new BasicNameValuePair("id",
	 * request.getUsername()));
	 * 
	 * String encPassword = ""; try { encPassword =
	 * LdapEncryptPassword.encrypt(ldapConfig.getSalt(), request.getPassword(),
	 * ldapConfig.getSecret()); } catch (Exception e) { map.put("status", 900);
	 * map.put("message","encrypt password error "+e.getMessage());
	 * e.printStackTrace(); }
	 * 
	 * form.add(new BasicNameValuePair("password", encPassword)); URI uri = null;
	 * try { uri = new URIBuilder(ldapConfig.getUrl()).addParameters(form).build();
	 * } catch (Exception e) { e.printStackTrace(); map.put("status", 800);
	 * map.put("message","uri builder error "+e.getMessage()); }
	 * 
	 * Object response =
	 * restTemplate.exchange(String.valueOf(uri),HttpMethod.GET,null,LdapResponse.
	 * class); ResponseEntity<LdapResponse> responseEntity =
	 * (ResponseEntity<LdapResponse>) response;
	 * 
	 * if(responseEntity.getBody().getResponseCode().equalsIgnoreCase("00")){
	 * map.put("status", responseEntity.getBody().getResponseCode());
	 * map.put("message",responseEntity.getBody().getResponseDescription()); }else {
	 * map.put("status", responseEntity.getBody().getResponseCode());
	 * map.put("message", responseEntity.getBody().getResponseDescription()); }
	 * return map; }
	 */

}
