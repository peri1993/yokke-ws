package com.mega.ws.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mega.ws.model.InstancesLogs;
import com.mega.ws.model.Notifications;
import com.mega.ws.request.ApprovalChangeRequest;
import com.mega.ws.response.JBPM_MappingResponse;
import com.mega.ws.service.InstancesLogsService;
import com.mega.ws.service.NotificationService;

@RestController
public class ApprovalController extends BaseController {

	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(ApprovalController.class);

	@Autowired
	private InstancesLogsService instancesService;

	@Autowired
	private NotificationService notifService;

	
	@PostMapping("/update-approval-list")
	@ResponseBody
	public Map<Object, Object> updateList(@RequestBody List<ApprovalChangeRequest> request){
		Map<Object, Object> map = new HashMap<Object, Object>();
		try {
			for (int i = 0; i < request.size(); i++) {
				InstancesLogs instancesLogs = instancesService.getInstancesLogById(new Long(request.get(i).getInstancesId()));
				Map<String, Object> validationMap = validationAssignTaskList(instancesLogs, request.get(i));
				Boolean flag = (Boolean) validationMap.get("validation");
				if(flag) {
					map.put("status", validationMap.get("status"));
					map.put("message", validationMap.get("message"));
				}else {

					JBPM_MappingResponse mapping = mappingURL.updateApproval();
					HttpEntity<ApprovalChangeRequest> requestEntity = new HttpEntity<>(request.get(i), headerSettings.setupHeaderAccept());
					restTemplate.exchange(mapping.getUrl(), mapping.getMethod(), requestEntity, Object.class);

					if (instancesLogs != null) {
						if(instancesLogs.getAssigner() != null) {
							if(instancesLogs.getAssigner().equals(instancesLogs.getApprovalDs1())) {
								instancesLogs.setAssigner(request.get(i).getDs1_id());
							}else if(instancesLogs.getAssigner().equals(instancesLogs.getApprovalDs2())) {
								instancesLogs.setAssigner(request.get(i).getDs2_id());
							}
						}
						
						if (instancesLogs.getApprovalDs1().equals(instancesLogs.getApprovalDs2())) {
							if(instancesLogs.getApprovalDs1().equals(request.get(i).getDs1_id())) {
								instancesLogs.setApprovalDs1(request.get(i).getDs2_id());
								instancesLogs.setNameApprovalDs1(findName(request.get(i).getDs2_id()));
								instancesLogs.setApprovalDs2(request.get(i).getDs2_id());
								instancesLogs.setNameApprovalDs2(findName(request.get(i).getDs2_id()));
								instancesLogs.setAssigner(request.get(i).getDs2_id());
							}else {
								instancesLogs.setApprovalDs1(request.get(i).getDs1_id());
								instancesLogs.setNameApprovalDs1(findName(request.get(i).getDs1_id()));
								instancesLogs.setApprovalDs2(request.get(i).getDs1_id());
								instancesLogs.setNameApprovalDs2(findName(request.get(i).getDs1_id()));
								instancesLogs.setAssigner(request.get(i).getDs1_id());
							}
						} else {
							instancesLogs.setApprovalDs1(request.get(i).getDs1_id());
							instancesLogs.setNameApprovalDs1(findName(request.get(i).getDs1_id()));
							instancesLogs.setApprovalDs2(request.get(i).getDs2_id());
							instancesLogs.setNameApprovalDs2(findName(request.get(i).getDs2_id()));
						}
						instancesLogs.setUpdatedBy(request.get(i).getUsername());
						instancesLogs.setUpdatedDt(getNow());
						
						instancesService.save(instancesLogs);
						
						Notifications notif = notifService.findByInstancesIdDesc(instancesLogs);
						if(notif != null) {
							notif.setAssigner(instancesLogs.getAssigner());
							notif.setFlagRead("N");
							
							notifService.save(notif);
						}
					}

					map.put("status", 200);
					map.put("message", "Update Approval Success Task Assign To "+validationMap.get("assigner")+" !!!");
					map.put("assigner", validationMap.get("assigner"));
				
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			map.put("error", e.getMessage() == null ? "Instances ID Not Found !!! " : e.getMessage());
			map.put("status", 400);
		}
		
		return map;
	}
	
	@PostMapping("/update-approval")
	@ResponseBody
	public Map<Object, Object> update(@RequestBody ApprovalChangeRequest request) {
		Map<Object, Object> map = new HashMap<Object, Object>();
		try {

			InstancesLogs instancesLogs = instancesService.getInstancesLogById(new Long(request.getInstancesId()));
			Map<String, Object> validationMap = validationAssignTask(instancesLogs, request);
			Boolean flag = (Boolean) validationMap.get("validation");
			if(flag) {
				map.put("status", validationMap.get("status"));
				map.put("message", validationMap.get("message"));
			}else {
				JBPM_MappingResponse mapping = mappingURL.updateApproval();
				HttpEntity<ApprovalChangeRequest> requestEntity = new HttpEntity<>(request,
						headerSettings.setupHeaderAccept());
				restTemplate.exchange(mapping.getUrl(), mapping.getMethod(), requestEntity, Object.class);

				if (instancesLogs != null) {
					if(instancesLogs.getAssigner() != null) {
						if(instancesLogs.getAssigner().equals(instancesLogs.getApprovalDs1())) {
							instancesLogs.setAssigner(request.getDs1_id());
						}else if(instancesLogs.getAssigner().equals(instancesLogs.getApprovalDs2())) {
							instancesLogs.setAssigner(request.getDs2_id());
						}
					}
					instancesLogs.setApprovalDs1(request.getDs1_id());
					instancesLogs.setNameApprovalDs1(findName(request.getDs1_id()));
					instancesLogs.setApprovalDs2(request.getDs2_id());
					instancesLogs.setNameApprovalDs2(findName(request.getDs2_id()));
					instancesLogs.setUpdatedBy(request.getUsername());
					instancesLogs.setUpdatedDt(getNow());
					
					instancesService.save(instancesLogs);
					
					Notifications notif = notifService.findByInstancesIdDesc(instancesLogs);
					if(notif != null) {
						notif.setAssigner(instancesLogs.getAssigner());
						notif.setFlagRead("N");
						
						notifService.save(notif);
					}
				}

				map.put("status", 200);
				map.put("message", "Update Approval Success Task Assign To "+validationMap.get("assigner")+" !!!");
				map.put("assigner", validationMap.get("assigner"));
			}

		} catch (Exception e) {
			map.put("error", e.getMessage());
			map.put("status", 400);
		}

		return map;
	}
	
	private Map<String, Object> validationAssignTask(InstancesLogs logs, ApprovalChangeRequest request) {
		Map<String, Object> map = new HashMap<String, Object>();
		if (logs.getAssigner().equals(request.getAssigner())) {
			if(logs.getApprovalDs1().contains(request.getDs1_id()) && logs.getApprovalDs2().contains(request.getDs2_id())){
				map.put("validation", true);
				map.put("status", 400);
				map.put("message", "Tidak ada perubahan dalam struktur approval !!!");
				return map;
			}
			
			/*
			 * if(logs.getAssigner().equals(logs.getApprovalDs1()) &&
			 * logs.getAssigner().equals(logs.getApprovalDs2())) {
			 * if(request.getDs1_id().equals(request.getDs2_id())) { map.put("validation",
			 * false); map.put("assigner", request.getDs1_id()); }else {
			 * map.put("validation", true); map.put("status", 400); map.put("message",
			 * "Approval 1 dan Approval 2 harus sama !!!"); } }else
			 */
			if (logs.getAssigner().equals(logs.getApprovalDs1()) && !request.getAssigner().equals(request.getDs1_id())) {
				map.put("validation", false);		
				map.put("assigner", request.getDs1_id());
			}else if(logs.getAssigner().equals(logs.getApprovalDs2()) && !request.getAssigner().equals(request.getDs2_id())) {
				map.put("validation", false);
				map.put("assigner", request.getDs2_id());
			}else {
				map.put("validation", true);
				map.put("status", 400);
				map.put("message", "Tidak bisa update approval karena user tersebut tidak memiliki task ini !!!");
			}
		}
		
		return map;
	}
	
	private Map<String, Object> validationAssignTaskList(InstancesLogs logs, ApprovalChangeRequest request) {
		Map<String, Object> map = new HashMap<String, Object>();
		if (logs.getAssigner().equals(request.getAssigner())) {
			if(logs.getApprovalDs1().contains(request.getDs1_id()) && logs.getApprovalDs2().contains(request.getDs2_id())){
				map.put("validation", true);
				map.put("status", 400);
				map.put("message", "Tidak ada perubahan dalam struktur approval !!!");
				return map;
			}
			
			if(logs.getAssigner().equals(logs.getApprovalDs1()) && logs.getAssigner().equals(logs.getApprovalDs2())) {
				if(request.getDs1_id().equals(request.getDs2_id())) {
					map.put("validation", false);		
					map.put("assigner", request.getDs1_id());
				}else if(!request.getAssigner().equals(request.getDs1_id())){
					map.put("validation", false);		
					map.put("assigner", request.getDs1_id());
				}else {
					map.put("validation", false);		
					map.put("assigner", request.getDs2_id());
				}
			}else if (logs.getAssigner().equals(logs.getApprovalDs1()) && !request.getAssigner().equals(request.getDs1_id())) {
				map.put("validation", false);		
				map.put("assigner", request.getDs1_id());
			}else if(logs.getAssigner().equals(logs.getApprovalDs2()) && !request.getAssigner().equals(request.getDs2_id())) {
				map.put("validation", false);
				map.put("assigner", request.getDs2_id());
			}else {
				map.put("validation", true);
				map.put("status", 400);
				map.put("message", "Tidak bisa update approval karena user tersebut tidak memiliki task ini !!!");
			}
		}
		
		return map;
	}
	
	@GetMapping("/detail-approval/{instancesId}")
	@ResponseBody
	public Map<Object, Object> detail(@PathVariable String instancesId) {
		Map<Object, Object> map = new HashMap<Object, Object>();
		try {
			InstancesLogs logs = instancesService.findByInstancesId(new Long(instancesId));
			if(logs != null) {
				map.put("objectResponse", logs);
				map.put("status", 200);
				map.put("message", "Success");
			}else {
				map.put("status", 400);
				map.put("message", "Instances ID not Found !!!");
			}

		} catch (Exception e) {
			map.put("error", e.getMessage());
			map.put("status", 400);
		}

		return map;
	}
	
	@GetMapping("/check-assigner")
	@ResponseBody
	public Map<Object, Object> checkAssigner(@RequestParam String instancesId, @RequestParam String username) {
		Map<Object, Object> map = new HashMap<Object, Object>();
		try {
			String user = username;
			List<InstancesLogs> logs = instancesService.checkAssigner(new Long(instancesId), username);
			if(!logs.isEmpty()) {
				map.put("checkAssignerFlag", true);
				map.put("status", 200);
				map.put("message", "User "+user+" Task Owner This Task !!!");
			}else {
				map.put("checkAssignerFlag", false);
				map.put("status", 400);
				map.put("message", "User "+user+" Not Task Owner This Task !!!");
			}

		} catch (Exception e) {
			map.put("error", e.getMessage());
			map.put("status", 400);
		}

		return map;
	}

}
