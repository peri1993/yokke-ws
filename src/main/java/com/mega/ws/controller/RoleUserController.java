package com.mega.ws.controller;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mega.ws.model.Menu;
import com.mega.ws.model.Role;
import com.mega.ws.model.RoleUser;
import com.mega.ws.request.MenuRequest;
import com.mega.ws.request.RoleRequest;
import com.mega.ws.request.RoleUserRequest;
import com.mega.ws.response.PageResponse;
import com.mega.ws.response.PropertiesResponse;
import com.mega.ws.service.RoleUserService;
import com.mega.ws.service.dto.RoleDTO;
import com.mega.ws.service.dto.UserDTO;

@RestController
public class RoleUserController extends BaseController {

	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(RoleUserController.class);

	@Autowired
	private RoleUserService roleService;

	@GetMapping("/list-user")
	@ResponseBody
	public PageResponse<UserDTO> listUser(Pageable pageable,
			@RequestParam(name = "username", defaultValue = "") String paramUsername,
			@RequestParam(name = "fullname", defaultValue = "") String paramFullname,
			@RequestParam(name = "roleName", defaultValue = "") String paramRoleName,
			@RequestParam(name = "createdBy", defaultValue = "") String paramCreatedBy,
			@RequestParam(name = "updatedBy", defaultValue = "") String paramUpdatedBy,
			@RequestParam(name = "createdStartDate", defaultValue = "") String createdStartDate,
			@RequestParam(name = "createdEndDate", defaultValue = "") String createdEndDate,
			@RequestParam(name = "updatedStartDate", defaultValue = "") String updatedStartDate,
			@RequestParam(name = "updatedEndDate", defaultValue = "") String updatedEndDate) {

		try {
			Page<UserDTO> results = roleService.getAllUser(paramUsername, paramFullname, paramCreatedBy, paramRoleName,
					createdStartDate, createdEndDate, paramUpdatedBy, updatedStartDate, updatedEndDate, pageable);
			return new PageResponse<>(PropertiesResponse.STATUS_MESSAGE_OK, PropertiesResponse.STATUS_CODE_OK,
					results.getContent(), results.getNumber(), results.getSize(), results.getTotalPages(),
					results.isLast(), new Long(0));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}

	@PostMapping("/create-user")
	@ResponseBody
	public Map<String, Object> createUser(@RequestBody RoleUserRequest request) {
		Map<String, Object> map = new HashMap<String, Object>();

		RoleUser userCheck = roleService.findUserByUsername(request.getUsername());
		if (userCheck != null) {
			map.put("error", "Username " + request.getUsername() + " Sudah Ada !!!");
			map.put("status", 200);
			return map;
		}

		try {
			Role role = roleService.detailRole(new Long(request.getRoleId()));
			if (role == null) {
				map.put("error", "Role ID tidak ditemukan");
				map.put("status", 200);
				return map;
			} else if ("default".equals(role.getRoleName())) {
				map.put("error", "Penambahan user ID di Role default tidak diperbolehkan !!!");
				map.put("status", 400);
				return map;
			} else {
				RoleUser user = setUserBody(request);
				user.setRole(role);
				roleService.saveUserRole(user);
				map.put("message", "Create User Role Success");
				map.put("status", 200);
				return map;
			}

		} catch (Exception e) {
			map.put("error", e.getMessage());
			map.put("status", 400);
		}

		return map;
	}

	@SuppressWarnings({ "unused" })
	@PostMapping("/update-user")
	@ResponseBody
	public Map<String, Object> updateUser(@RequestBody RoleUserRequest request) {
		Map<String, Object> map = new HashMap<String, Object>();

		try {
			RoleUser roleUser = roleService.findUserRoleByID(request.getId());
			String username = roleUser.getUsername();
			if ("default".equals(username)) {
				map.put("error", "User Default tidak boleh di Update !!!");
				map.put("status", 400);
				return map;
			}
			if (roleUser == null) {
				map.put("error", "ID tidak ditemukan");
				map.put("status", 200);
				return map;
			} else {
				Role role = roleService.detailRole(new Long(request.getRoleId()));
				if ("default".equals(role.getRoleName())) {
					map.put("error", "User Tidak bisa di update ke Role default, Silahkan delete username jika ingin mendapatkan Role Default  !!!");
					map.put("status", 400);
					return map;
				} else {
					RoleUser user = setBodyUserForUpdate(roleUser);
					user.setUpdatedBy(request.getUpdatedBy());
					user.setRole(role);
					roleService.saveUserRole(user);
					map.put("message", "Update User Role Success");
					map.put("status", 200);
					return map;
				}

			}

		} catch (Exception e) {
			map.put("error", e.getMessage());
			map.put("status", 400);
		}

		return map;
	}

	private RoleUser setBodyUserForUpdate(RoleUser request) {
		RoleUser user = new RoleUser();
		try {
			user = request;
			user.setUpdatedDt(getNow());
			user.setRole(request.getRole());

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return user;
	}

	@GetMapping("/delete-user/{userRoleId}")
	@ResponseBody
	public Map<String, Object> deleteUser(@PathVariable String userRoleId) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			RoleUser role = roleService.findUserRoleByID(new Long(userRoleId));
			if (role == null) {
				map.put("error", "User Role ID tidak ditemukan !!!");
				map.put("status", 400);
			} else {
				if ("default".equals(role.getUsername())) {
					map.put("error", "User " + role.getUsername() + " Tidak diperbolehkan di delete !!!");
					map.put("status", 400);
				} else {
					roleService.deleteUserRole(new Long(userRoleId));
					map.put("message", "Delete Success");
					map.put("status", 200);
				}
			}

		} catch (Exception e) {
			map.put("error", e.getMessage());
			map.put("status", 400);
		}

		return map;
	}

	private RoleUser setUserBody(RoleUserRequest request) {
		RoleUser user = new RoleUser();
		user.setUsername(request.getUsername());
		user.setFullname(request.getFullname());
		user.setCreatedBy(request.getCreatedBy());
		user.setCreatedDt(getNow());
		return user;
	}

	@PostMapping("/create-role")
	@ResponseBody
	public Map<String, Object> createRole(@RequestBody RoleRequest request) {
		Map<String, Object> map = new HashMap<String, Object>();
		Role roleCheck = roleService.findByRoleName(request.getRoleName());
		if (roleCheck != null) {
			map.put("error", "Role Name " + request.getRoleName() + " Sudah Ada !!!");
			map.put("status", 200);
			return map;
		}
		Role role = setRole(request);
		try {
			roleService.saveRole(role);
			map.put("message", "Success");
			map.put("status", 200);

		} catch (Exception e) {
			map.put("error", e.getMessage());
			map.put("status", 400);
		}

		return map;
	}

	@PostMapping("/update-role")
	@ResponseBody
	public Map<String, Object> updateRole(@RequestBody RoleRequest request) {
		Map<String, Object> map = new HashMap<String, Object>();

		try {
			Role checkRole = roleService.detailRole(request.getRole_id());
			if ("default".equals(checkRole.getRoleName())) {
				map.put("error", "role name default tidak bisa di update !!!");
				map.put("message", "role name default tidak bisa di update !!!");
				map.put("status", 400);
			} else {
				Role role = setRoleUpdate(request);
				roleService.updateRole(role);
				map.put("message", "Update Success");
				map.put("status", 200);
			}

		} catch (Exception e) {
			map.put("error", e.getMessage());
			map.put("status", 400);
		}

		return map;
	}

	@GetMapping("/delete-role")
	@ResponseBody
	public Map<String, Object> deleteRole(@RequestParam String roleId) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			Role role = roleService.detailRole(new Long(roleId));
			if ("default".equals(role.getRoleName())) {
				map.put("error", "Role " + role.getRoleName() + " Tidak bisa di delete");
				map.put("message", "Role " + role.getRoleName() + " Tidak bisa di delete");
				map.put("status", 400);
			} else {
				roleService.delete(new Long(roleId));
				map.put("message", "Delete Success");
				map.put("status", 200);
			}

		} catch (Exception e) {
			map.put("error", e.getMessage());
			map.put("status", 400);
			map.put("message", "Role tidak bisa di delete karena ada user yang memakai");
		}

		return map;
	}

	@GetMapping("/detail-role")
	@ResponseBody
	public Map<String, Object> detailRole(@RequestParam String roleId) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			Role role = roleService.detailRole(new Long(roleId));
			map.put("objectResponse", role);
			map.put("status", 200);

		} catch (Exception e) {
			map.put("error", e.getMessage());
			map.put("status", 400);
		}

		return map;
	}

	@GetMapping("/detail-user/{id}")
	@ResponseBody
	public Map<String, Object> detailUser(@PathVariable String id) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			RoleUser user = roleService.findUserRoleByID(new Long(id));
			map.put("objectResponse", user);
			map.put("status", 200);

		} catch (Exception e) {
			map.put("error", e.getMessage());
			map.put("status", 400);
		}

		return map;
	}

	@GetMapping("/all-role")
	@ResponseBody
	public PageResponse<RoleDTO> allRole(Pageable pageable,
			@RequestParam(name = "roleName", defaultValue = "") String paramRoleName,
			@RequestParam(name = "createdBy", defaultValue = "") String paramCreatedBy,
			@RequestParam(name = "startDate", defaultValue = "") String paramStartDate,
			@RequestParam(name = "endDate", defaultValue = "") String paramEndDate) {

		try {
			Page<RoleDTO> results = roleService.getAllRole(paramRoleName, paramCreatedBy, pageable);
			return new PageResponse<>(PropertiesResponse.STATUS_MESSAGE_OK, PropertiesResponse.STATUS_CODE_OK,
					results.getContent(), results.getNumber(), results.getSize(), results.getTotalPages(),
					results.isLast(), new Long(0));
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private Role setRole(RoleRequest request) {
		Role role = new Role();
		Set<Menu> listMenu = new HashSet<>();

		try {
			role.setCreatedBy(request.getCreatedBy());
			role.setCreatedDt(getNow());
			role.setRoleName(request.getRoleName());
			if (request.getUpdatedBy() != null) {
				role.setUpdatedBy(request.getUpdatedBy());
				role.setUpdatedDt(getNow());
			}
			if (request.getListMenu() != null) {
				if (!request.getListMenu().isEmpty() && request.getListMenu().size() > 0) {
					for (int i = 0; i < request.getListMenu().size(); i++) {
						MenuRequest model = request.getListMenu().get(i);
						Menu menu = new Menu();
						menu.setName(model.getName());
						menu.setRole(role);
						listMenu.add(menu);
					}
				}
				role.setListMenu(listMenu);
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return role;
	}

	private Role setRoleUpdate(RoleRequest request) {
		Role role = new Role();
		Set<Menu> listMenu = new HashSet<>();
		Role detail = roleService.detailRole(request.getRole_id());
		try {
			role.setRole_id(request.getRole_id());
			role.setCreatedBy(detail.getCreatedBy());
			role.setCreatedDt(detail.getCreatedDt());
			role.setRoleName(request.getRoleName());
			role.setUpdatedBy(request.getUpdatedBy());
			role.setUpdatedDt(getNow());

			if (request.getListMenu() != null) {
				if (!request.getListMenu().isEmpty() && request.getListMenu().size() > 0) {
					for (int i = 0; i < request.getListMenu().size(); i++) {
						MenuRequest model = request.getListMenu().get(i);
						Menu menu = new Menu();
						menu.setName(model.getName());
						menu.setRole(role);
						listMenu.add(menu);
					}
				}
				role.setListMenu(listMenu);
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return role;
	}
}
