package com.mega.ws.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.mega.ws.config.ConfigSortDefault;
import com.mega.ws.constants.ModuleConstants;
import com.mega.ws.model.FIleAttachment;
import com.mega.ws.model.OfficeHours;
import com.mega.ws.model.TaskLogs;
import com.mega.ws.request.ApproveRequest;
import com.mega.ws.request.ChangeWorkHoursRequest;
import com.mega.ws.request.HistoryDateRequest;
import com.mega.ws.request.LeaveRequest;
import com.mega.ws.response.ChangeWorkHoursDetailResponse;
import com.mega.ws.response.JBPM_MappingResponse;
import com.mega.ws.response.PageResponse;
import com.mega.ws.response.PropertiesResponse;
import com.mega.ws.response.TaskLogsResponse;
import com.mega.ws.response.TaskResponse;
import com.mega.ws.service.InstancesLogsService;
import com.mega.ws.service.NotificationService;
import com.mega.ws.service.TaskLogsService;
import com.mega.ws.service.dto.InstancesLogsDTO;
import com.mega.ws.service.dto.ReAssignerDTO;
import com.mega.ws.service.dto.TaskLogsDTO;

@RestController
public class TaskController extends BaseController {

	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(TaskController.class);

	@Autowired
	private InstancesLogsService instancesLogsService;

	@Autowired
	private TaskLogsService taskLogsService;

	@Autowired
	private NotificationService notifService;

	@SuppressWarnings({ "unchecked" })
	@PostMapping("/actived-task")
	private Map<String, Object> showActiveTaskByProcessId(Integer instanceId) {
		Map<String, Object> map = new HashMap<String, Object>();

		JBPM_MappingResponse mapping = mappingURL.showActiveTaskByProcessId(instanceId);
		HttpEntity<LeaveRequest> requestEntity = new HttpEntity<>(headerSettings.setupHeaderAccept());
		Object response = restTemplate.exchange(mapping.getUrl(), mapping.getMethod(), requestEntity, Object.class);

		ResponseEntity<Object> responseEntity = (ResponseEntity<Object>) response;
		map.put("data", responseEntity.getBody());
		map.put("status", 200);
		return map;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@GetMapping("/task-owner")
	@ResponseBody
	private List<TaskResponse> showActiveTaskByProcessId(@RequestParam String username) {
		List<TaskResponse> response = new ArrayList<TaskResponse>();

		try {
			JBPM_MappingResponse mapping = mappingURL.instancesOwner();
			HttpEntity<LeaveRequest> requestEntity = new HttpEntity<>(headerSettings.setupHeaderAccept());

			UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(mapping.getUrl())
					.queryParam("user", username).queryParam("page", "0").queryParam("pageSize", "500");

			Object resp = restTemplate.exchange(builder.buildAndExpand(mapping.getUrl()).toUri(), mapping.getMethod(),
					requestEntity, Object.class);

			ResponseEntity<HashMap> responseEntity = (ResponseEntity<HashMap>) resp;
			List<Object> taskList = (List<Object>) responseEntity.getBody().get("task-summary");
			for (int i = 0; i < taskList.size(); i++) {
				Map<String, Object> taskValue = (Map<String, Object>) taskList.get(i);
				response.add(setModel(taskValue));
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return response;
	}

	private TaskResponse setModel(Map<String, Object> taskValues) {
		TaskResponse model = new TaskResponse();
		try {

			model.setTaskId(String.valueOf(taskValues.get("task-id")));
			model.setTaskName(String.valueOf(taskValues.get("task-name")));
			model.setTaskSubject(String.valueOf(taskValues.get("task-subject")));
			model.setTaskDescription(String.valueOf(taskValues.get("task-description")));
			model.setTaskStatus(String.valueOf(taskValues.get("task-status")));
			model.setTaskCreatedBy(String.valueOf(taskValues.get("task-created-by")));
			model.setTaskInstancesId(String.valueOf(taskValues.get("task-proc-inst-id")));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return model;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@GetMapping("/task-detail")
	@ResponseBody
	private Map<Object, Object> detailTask(@RequestParam String instancesId, @RequestParam String username) {
		Map<Object, Object> map = new HashMap<Object, Object>();
		try {

			JBPM_MappingResponse mapping = mappingURL.instancesVariables(Integer.valueOf(instancesId));
			HttpEntity<LeaveRequest> requestEntity = new HttpEntity<>(headerSettings.setupHeaderAccept());
			
			Object response = restTemplate.exchange(mapping.getUrl(), mapping.getMethod(), requestEntity, Object.class);
			ResponseEntity<HashMap> responseEntity = (ResponseEntity<HashMap>) response;

			List<Object> instancesList = (List<Object>) responseEntity.getBody().get("variable-instance");
			List<FIleAttachment> listFileUpload = new ArrayList<FIleAttachment>();
			for (int i = 0; i < instancesList.size(); i++) {
				FIleAttachment model = new FIleAttachment();
				Map<String, Object> instancesValues = (Map<String, Object>) instancesList.get(i);
				if ("filename1".equals(instancesValues.get("name"))) {
					model = uploadService.getFileByFilename(String.valueOf(instancesValues.get("value")));
					listFileUpload.add(model);
				} else if ("filename2".equals(instancesValues.get("name"))) {
					model = uploadService.getFileByFilename(String.valueOf(instancesValues.get("value")));
					listFileUpload.add(model);
				} else if ("filename3".equals(instancesValues.get("name"))) {
					model = uploadService.getFileByFilename(String.valueOf(instancesValues.get("value")));
					listFileUpload.add(model);
				} else if ("filename4".equals(instancesValues.get("name"))) {
					model = uploadService.getFileByFilename(String.valueOf(instancesValues.get("value")));
					listFileUpload.add(model);
				} else if ("filename5".equals(instancesValues.get("name"))) {
					model = uploadService.getFileByFilename(String.valueOf(instancesValues.get("value")));
					listFileUpload.add(model);
				} else {
					map.put(instancesValues.get("name"),
							(instancesValues.get("value") == "" ? instancesValues.get("old-value")
									: instancesValues.get("value")));
				}
			}

			map.put("listFileUpload", listFileUpload);

			JBPM_MappingResponse activeTask = mappingURL.showActiveTaskByProcessId(Integer.valueOf(instancesId));
			ResponseEntity<HashMap> responseActiveTask = restTemplate.exchange(activeTask.getUrl(),
					activeTask.getMethod(), null, HashMap.class);
			Integer activeTaskId = findTaskId(responseActiveTask);
			map.put("taskId", activeTaskId);
			map.put("instancesId", instancesId);
			notifService.update(null, username, instancesId);
		} catch (Exception e) {
			// TODO: handle exception
			map.put("error", 400);
			map.put("message", "Data Not Found");
		}

		return map;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@GetMapping("/task-detail-work-hours")
	@ResponseBody
	private Map<Object, Object> detailTaskWorkHours(@RequestParam String instancesId, @RequestParam String username) {
		Map<Object, Object> map = new HashMap<Object, Object>();

		JBPM_MappingResponse mapping = mappingURL.instancesVariables(Integer.valueOf(instancesId));
		HttpEntity<LeaveRequest> requestEntity = new HttpEntity<>(headerSettings.setupHeaderAccept());

		Object response = restTemplate.exchange(mapping.getUrl(), mapping.getMethod(), requestEntity, Object.class);

		ResponseEntity<HashMap> responseEntity = (ResponseEntity<HashMap>) response;

		List<Object> instancesList = (List<Object>) responseEntity.getBody().get("variable-instance");

		// get instancesId and task ID
		JBPM_MappingResponse activeTask = mappingURL.showActiveTaskByProcessId(Integer.valueOf(instancesId));
		ResponseEntity<HashMap> responseActiveTask = restTemplate.exchange(activeTask.getUrl(), activeTask.getMethod(),
				null, HashMap.class);
		Integer activeTaskId = findTaskId(responseActiveTask);

		map = setResponseWorkHours(instancesList, String.valueOf(activeTaskId), instancesId);

		try {
			notifService.update(null, username, instancesId);
			map.put("status", 200);
		} catch (Exception e) {
			// TODO: handle exception
			map.put("error", 400);
			map.put("message", "Data Not Found");
		}

		return map;
	}

	@SuppressWarnings("unchecked")
	private Map<Object, Object> setResponseWorkHours(List<Object> list, String activeTaskId, String instancesId) {
		Map<Object, Object> map = new HashMap<Object, Object>();
		ChangeWorkHoursDetailResponse request = new ChangeWorkHoursDetailResponse();
		for (int i = 0; i < list.size(); i++) {
			Map<String, Object> instancesValues = (Map<String, Object>) list.get(i);
			if ("employee_id".equals(instancesValues.get("name"))) {
				request.setEmployee_id(String.valueOf(instancesValues.get("value")));
			} else if ("employee_name".equals(instancesValues.get("name"))) {
				request.setEmployee_name(String.valueOf(instancesValues.get("value")));
			} else if ("approval_ds_1".equals(instancesValues.get("name"))) {
				request.setApproval_ds_1(String.valueOf(instancesValues.get("value")));
			} else if ("approval_ds_1_name".equals(instancesValues.get("name"))) {
				request.setApproval_ds_1_name(String.valueOf(instancesValues.get("value")));
			} else if ("approval_ds_2".equals(instancesValues.get("name"))) {
				request.setApproval_ds_2(String.valueOf(instancesValues.get("value")));
			} else if ("approval_ds_2_name".equals(instancesValues.get("name"))) {
				request.setApproval_ds_2_name(String.valueOf(instancesValues.get("value")));
			} else if ("hours_id".equals(instancesValues.get("name"))) {
				request.setHours_id(String.valueOf(instancesValues.get("value")));
			}
		}

		OfficeHours hours = informationWorkHoursService.findOfficeHoursById(request.getHours_id());
		request.setStart_time(hours.getStartTime().concat(" ").concat(hours.getLocationExtend()));
		request.setEnd_time(hours.getEndtTime().concat(" ".concat(hours.getLocationExtend())));
		request.setReason(hours.getReason());
		request.setTaskId(activeTaskId);
		request.setInstancesId(instancesId);

		map.put("objectResponse", request);
		return map;
	}

	@PutMapping("/approve-task-list")
	@ResponseBody
	public Map<Object, Object> approveList(@RequestBody List<ApproveRequest> listRequest) {
		Map<Object, Object> map = new HashMap<Object, Object>();
		Map<String, Object> responseApprove = new HashMap<String, Object>();
		Integer countSuccess = 0, countFailed = 0;
		try {
			for (int i = 0; i < listRequest.size(); i++) {
				ApproveRequest request = listRequest.get(i);
				if (request.getInstancesId() != null && request.getTaskId() != null) {
					responseApprove = approve(request);
					Integer status = (Integer) responseApprove.get("status");
					if (status != 200) {
						countFailed = countFailed + 1;
					} else {
						countSuccess = countSuccess + 1;
					}
				} else {
					map.put("message", "InstancesId" + request.getInstancesId() + " not exist !");
					map.put("status", 200);
					return map;
				}
			}
			if (countFailed >= 1) {
				map.put("message",
						"Success approve " + countSuccess + " Task, And " + countFailed + " Task Approve Failed !!!");
			} else {
				map.put("message", "Success approve " + countSuccess + " Task ");
			}
			map.put("status", 200);

		} catch (Exception e) {
			map.put("message", e.getMessage() == null ? "Task ID cannot approved !" : e.getMessage());
			map.put("status", 400);
		}

		return map;
	}

	@PutMapping("/reject-task-list")
	@ResponseBody
	public Map<Object, Object> rejectList(@RequestBody List<ApproveRequest> listRequest) {
		Map<Object, Object> map = new HashMap<Object, Object>();
		try {
			for (int i = 0; i < listRequest.size(); i++) {
				ApproveRequest request = listRequest.get(i);
				if (request.getInstancesId() != null && request.getTaskId() != null) {
					reject(request);
				} else {
					map.put("message", "InstancesId" + request.getInstancesId() + " not exist !");
					map.put("status", 200);
					return map;
				}
			}
			map.put("message", "Reject Success");
			map.put("status", 200);

		} catch (Exception e) {
			map.put("message", e.getMessage() == null ? "Task ID cannot Reject !" : e.getMessage());
			map.put("status", 400);
		}

		return map;
	}

	@PutMapping("/approve-task")
	@ResponseBody
	public Map<String, Object> approve(@RequestBody ApproveRequest approveRequest) {
		Map<String, Object> map = new HashMap<String, Object>();
		approveRequest.setLeave_status("approve");
		try {
			if (approveRequest.getInstancesId() != null && approveRequest.getTaskId() != null) {
				String username = approveRequest.getUsername();
				String approval = getTracktingTaskApproval(approveRequest.getInstancesId());
				boolean isFlag = false;
				Map<Object, Object> detail = detailTask(approveRequest.getInstancesId(), approveRequest.getUsername());
				if (detail != null) {
					if (approveRequest.getUsername().equals(detail.get("approval_ds_2"))
							|| detail.get("approval_ds_2").equals(approval)) {
						isFlag = true;
					}
				}

				if (isFlag && !ModuleConstants.CHANGE_MODULE.equals(detail.get("moduleName"))) {
					if ("Y".equals(detail.get("carryOverFlag"))) {
						String remaining = findLastRemainingCarryOver(String.valueOf(detail.get("employee_id")));
						Long lastRemain = new Long(remaining);
						Long getRemain = new Long(String.valueOf(detail.get("quota_taken")));
						if (getRemain > lastRemain && lastRemain == 0) {
							map.put("error", "instances ID " + approveRequest.getInstancesId()
									+ " Silahkan hubungi admin untuk perpanjang Carry Over Anda");
							map.put("status", 400);
							return map;
						}
					} else {
						Map<String, Object> validationAbsenceType = new HashMap<String, Object>();
						validationAbsenceType = validateAbsencesType(String.valueOf(detail.get("absences_type")),
								String.valueOf(detail.get("total_taken_quota")),
								String.valueOf(detail.get("employee_id")));
						boolean flag = (boolean) validationAbsenceType.get("flagAbsence");
						if (flag) {
							validationAbsenceType.remove("flagAbsence");
							return validationAbsenceType;
						}

						// valdiation absencetype 2101
						if (validationAbsencesType(String.valueOf(detail.get("employee_id")),
								String.valueOf(detail.get("absences_type")),
								String.valueOf(detail.get("leave_start_date")))) {
							map.put("status", 400);
							map.put("error", "Anda Sudah " + detail.get("absenceDescription") + " bulan ini !!!");
							map.put("message", "Anda Sudah " + detail.get("absenceDescription") + " bulan ini !!!");
							return map;
						}
					}
				}
				if (approveRequest.getUsername().equals(approval)) {
					HttpEntity<ApproveRequest> completeEntity = new HttpEntity<>(approveRequest,
							headerSettings.setupHeaderContentType());
					JBPM_MappingResponse completeTask = mappingURL
							.completedTask(Integer.valueOf(approveRequest.getTaskId()));

					UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(completeTask.getUrl())
							.queryParam("user", approveRequest.getUsername()).queryParam("auto-progress", "true");

					restTemplate.exchange(builder.buildAndExpand(completeTask.getUrl()).toUri(),
							completeTask.getMethod(), completeEntity, Object.class);

				} else {
					approveRequest.setUsername(approval);
					HttpEntity<ApproveRequest> completeEntity = new HttpEntity<>(approveRequest,
							headerSettings.setupHeaderContentType());
					JBPM_MappingResponse completeTask = mappingURL
							.completedTask(Integer.valueOf(approveRequest.getTaskId()));

					UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(completeTask.getUrl())
							.queryParam("user", approveRequest.getUsername()).queryParam("auto-progress", "true");

					restTemplate.exchange(builder.buildAndExpand(completeTask.getUrl()).toUri(),
							completeTask.getMethod(), completeEntity, Object.class);

					approveRequest.setUsername(username);

				}

				saveLogsStages(approveRequest.getInstancesId(), approveRequest.getTaskId(),
						approveRequest.getLeave_status(), approveRequest.getUsername());

				if (approveRequest.getInstancesId() != null) {
					Map<Object, Object> obj = detailTask(approveRequest.getInstancesId(), approveRequest.getUsername());
					if (obj != null) {
						if (obj.get("approval_ds_1").equals(obj.get("approval_ds_2"))) {
							String taskId = String.valueOf(obj.get("taskId"));
							if (null != taskId && !"0".equals(taskId)) {
								ApproveRequest approval2 = approveRequest;
								approval2.setTaskId(taskId);
								if (!obj.get("approval_ds_2").equals(approval2.getUsername())) {
									approval2.setUsername(String.valueOf(obj.get("approval_ds_2")));
								}
								isFlag = false;
								approve(approval2);
							}
						}
					}
				}

				if (isFlag) {
					manipulationObject(detail);
				}

				map.put("message", "Data berhasil di Approve");
				map.put("status", 200);
			} else {
				map.put("error", "InstancesId or TaskId cannot empty !");
				map.put("status", 200);
			}

		} catch (Exception e) {
			map.put("error", e.getMessage() == null ? "Task ID cannot approved !" : e.getMessage());
			map.put("status", 400);
		}

		return map;
	}

	private void manipulationObject(Map<Object, Object> detail) {
		if (ModuleConstants.LEAVE_MODULE.equals(detail.get("moduleName"))
				|| ModuleConstants.PERMIT_MODULE.equals(detail.get("moduleName"))) {
			updateDataMinova(detail);
		} else if (ModuleConstants.CHANGE_MODULE.equals(detail.get("moduleName"))) {
			updateDataWorkHours(detail);
		}
	}

	private void updateDataWorkHours(Map<Object, Object> detail) {
		ChangeWorkHoursRequest change = setModelWorkHours(detail);
		informationWorkHoursService.udpateWorkHours(change);
	}

	private ChangeWorkHoursRequest setModelWorkHours(Map<Object, Object> detail) {
		ChangeWorkHoursRequest request = new ChangeWorkHoursRequest();
		try {
			request.setEmployee_id(String.valueOf(detail.get("employee_id")));
			request.setEmployee_name(String.valueOf(detail.get("employee_name")));
			request.setStart_time(String.valueOf(detail.get("start_time")));
			request.setEnd_time(String.valueOf(detail.get("end_time")));
			request.setReason(String.valueOf(detail.get("reason")));
			request.setApproval_ds_1(String.valueOf(detail.get("approval_ds_1")));
			request.setApproval_ds_1_name(String.valueOf(detail.get("approval_ds_1_name")));
			request.setApproval_ds_2(String.valueOf(detail.get("approval_ds_2")));
			request.setApproval_ds_2_name(String.valueOf(detail.get("approval_ds_2_name")));
			request.setHours_id(String.valueOf(detail.get("hours_id")));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return request;
	}

	private void updateDataMinova(Map<Object, Object> detail) {
		LeaveRequest leave = setDetail(detail);
		update(leave);
	}

	private LeaveRequest setDetail(Map<Object, Object> detail) {
		LeaveRequest model = new LeaveRequest();
		model.setEmployee_id(String.valueOf(detail.get("employee_id")));
		model.setLeave_start_date(String.valueOf(detail.get("leave_start_date")));
		model.setLeave_end_date(String.valueOf(detail.get("leave_end_date")));
		model.setAbsences_type(String.valueOf(detail.get("absences_type")));
		model.setAbsenceDescription(String.valueOf(detail.get("absenceDescription")));
		model.setDescription(String.valueOf(detail.get("absenceDescription")));
		model.setRemaining_quota(String.valueOf(detail.get("remaining_quota")));
		model.setTotal_taken_quota(String.valueOf(detail.get("total_taken_quota")));
		model.setQuota_taken(String.valueOf(detail.get("quota_taken")));
		model.setLeave_reason(String.valueOf(detail.get("leave_reason")));
		if ("Y".equals(detail.get("carryOverFlag"))) {
			model.setCarryOverFlag("Y");
		} else {
			model.setCarryOverFlag("N");
		}
		return model;
	}

	private void update(LeaveRequest request) {

		JBPM_MappingResponse mapping = mappingURL.updateData();
		HttpEntity<LeaveRequest> requestEntity = new HttpEntity<>(request, headerSettings.setupHeaderAccept());
		restTemplate.exchange(mapping.getUrl(), mapping.getMethod(), requestEntity, Object.class);

	}

	@SuppressWarnings({ "unchecked", "rawtypes", "unused" })
	private String findLastRemaining(String requesterId) {

		HttpEntity<Object> entity = new HttpEntity<>(headerSettings.setupHeaderAccept());
		JBPM_MappingResponse task = mappingURL.findLastRemaining(requesterId);

		Object response = restTemplate.exchange(task.getUrl(), task.getMethod(), entity, Object.class);

		ResponseEntity<HashMap> responseEntity = (ResponseEntity<HashMap>) response;
		String remaining = (String) responseEntity.getBody().get("remaining");

		return remaining;

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private String findLastRemainingCarryOver(String requesterId) {

		HttpEntity<Object> entity = new HttpEntity<>(headerSettings.setupHeaderAccept());
		JBPM_MappingResponse task = mappingURL.findLastRemainingCarryOver(requesterId);

		Object response = restTemplate.exchange(task.getUrl(), task.getMethod(), entity, Object.class);

		ResponseEntity<HashMap> responseEntity = (ResponseEntity<HashMap>) response;
		String remaining = (String) responseEntity.getBody().get("remaining");

		return remaining;

	}

	@PutMapping("/reject-task")
	@ResponseBody
	public Map<Object, Object> reject(@RequestBody ApproveRequest approveRequest) {
		Map<Object, Object> map = new HashMap<Object, Object>();

		approveRequest.setLeave_status("reject");
		try {

			if (approveRequest.getInstancesId() != null && approveRequest.getTaskId() != null) {
				String username = approveRequest.getUsername();
				String approval = getTracktingTaskApproval(approveRequest.getInstancesId());
				if (approval.equals(username)) {
					HttpEntity<ApproveRequest> completeEntity = new HttpEntity<>(approveRequest,
							headerSettings.setupHeaderContentType());
					JBPM_MappingResponse completeTask = mappingURL
							.completedTask(Integer.valueOf(approveRequest.getTaskId()));

					UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(completeTask.getUrl())
							.queryParam("user", approveRequest.getUsername()).queryParam("auto-progress", "true");

					restTemplate.exchange(builder.buildAndExpand(completeTask.getUrl()).toUri(),
							completeTask.getMethod(), completeEntity, Object.class);
				} else {
					approveRequest.setUsername(approval);
					HttpEntity<ApproveRequest> completeEntity = new HttpEntity<>(approveRequest,
							headerSettings.setupHeaderContentType());
					JBPM_MappingResponse completeTask = mappingURL
							.completedTask(Integer.valueOf(approveRequest.getTaskId()));

					UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(completeTask.getUrl())
							.queryParam("user", approveRequest.getUsername()).queryParam("auto-progress", "true");

					restTemplate.exchange(builder.buildAndExpand(completeTask.getUrl()).toUri(),
							completeTask.getMethod(), completeEntity, Object.class);

					approveRequest.setUsername(username);
				}

				saveLogsStages(approveRequest.getInstancesId(), approveRequest.getTaskId(),
						approveRequest.getLeave_status(), approveRequest.getUsername());

				map.put("message", "Data berhasil di Reject");
				map.put("status", 200);
			} else {
				map.put("error", "InstancesId or TaskId cannot empty !");
				map.put("status", 200);
			}

		} catch (Exception e) {
			map.put("error", e.getMessage());
			map.put("status", 400);
		}

		return map;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@GetMapping("/cek-data-active")
	@ResponseBody
	public Map<Object, Object> cekActiveData(@RequestParam String instancesId) {
		Map<Object, Object> map = new HashMap<Object, Object>();
		String resp = null;

		try {

			if (instancesId != null && instancesId != "") {
				HttpEntity<Object> entity = new HttpEntity<>(headerSettings.setupHeaderAccept());
				JBPM_MappingResponse task = mappingURL.cekActiveData(instancesId);

				Object response = restTemplate.exchange(task.getUrl(), task.getMethod(), entity, Object.class);

				ResponseEntity<HashMap> responseEntity = (ResponseEntity<HashMap>) response;

				if (null == responseEntity.getBody().get("active-user-tasks")) {
					resp = "completed";
				} else {
					resp = "uncompleted";
				}

				map.put("message", resp);
				map.put("status", 200);
			} else {
				map.put("error", "undefined instancesId");
				map.put("status", 404);
			}

		} catch (Exception e) {
			map.put("error", "undefined instancesId");
			map.put("status", 404);
		}

		return map;
	}

	@SuppressWarnings({ "unchecked", "rawtypes", "unused" })
	private String checkCompleteTask(String instancesId) {
		HttpEntity<Object> entity = new HttpEntity<>(headerSettings.setupHeaderAccept());
		JBPM_MappingResponse task = mappingURL.cekActiveData(instancesId);

		Object response = restTemplate.exchange(task.getUrl(), task.getMethod(), entity, Object.class);

		ResponseEntity<HashMap> responseEntity = (ResponseEntity<HashMap>) response;

		if (null == responseEntity.getBody().get("active-user-tasks")) {
			return "Y";
		} else {
			return "N";
		}
	}

	@SuppressWarnings("unused")
	private String validateChangeNameStatus(String status) {
		String returnName = null;
		if ("Completed".equals(status)) {
			returnName = "Approved";
		} else if ("Reserved".equals(status)) {
			returnName = "Waiting Approval";
		}
		return returnName;
	}

	@GetMapping("/list-inbox")
	@ResponseBody
	public PageResponse<InstancesLogsDTO> listInbox(Pageable pageable, String username,
			@RequestParam(name = "requesterId", defaultValue = "") String paramRequesterId,
			@RequestParam(name = "requesterName", defaultValue = "") String paramRequesterName,
			@RequestParam(name = "businessProcess", defaultValue = "") String paramBusinessProcess,
			@RequestParam(name = "absencesDesc", defaultValue = "") String paramAbsencesDesc,
			@RequestParam(name = "taskId", defaultValue = "") String paramTaskId,
			@RequestParam(name = "dateKey", defaultValue = "") String paramDateKey,
			@RequestParam(name = "startDate", defaultValue = "") String paramStartDate,
			@RequestParam(name = "endDate", defaultValue = "") String paramEndDate) {
		List<Long> listTaskId = new ArrayList<Long>();
		try {

			Page<InstancesLogsDTO> results = instancesLogsService.listInbox(username, listTaskId, paramRequesterId,
					paramRequesterName, paramBusinessProcess, paramAbsencesDesc, paramTaskId, paramDateKey,
					paramStartDate, paramEndDate, pageable);
			return new PageResponse<>(PropertiesResponse.STATUS_MESSAGE_OK, PropertiesResponse.STATUS_CODE_OK,
					results.getContent(), results.getNumber(), results.getSize(), results.getTotalPages(),
					results.isLast(), instancesLogsService.countInstancesLogByAssigner(username));

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@GetMapping("/list-outbox")
	@ResponseBody
	public PageResponse<InstancesLogsDTO> listOutbox(Pageable pageable, String username,
			@RequestParam(name = "requesterId", defaultValue = "") String paramRequesterId,
			@RequestParam(name = "requesterName", defaultValue = "") String paramRequesterName,
			@RequestParam(name = "businessProcess", defaultValue = "") String paramBusinessProcess,
			@RequestParam(name = "absencesType", defaultValue = "") String paramAbsencesType,
			@RequestParam(name = "taskId", defaultValue = "") String paramTaskId,
			@RequestParam(name = "dateKey", defaultValue = "") String paramDateKey,
			@RequestParam(name = "startDate", defaultValue = "") String paramStartDate,
			@RequestParam(name = "endDate", defaultValue = "") String paramEndDate) {
		List<Long> listTaskId = new ArrayList<Long>();
		try {

			Page<InstancesLogsDTO> results = instancesLogsService.listOutbox(username, listTaskId, paramRequesterId,
					paramRequesterName, paramBusinessProcess, paramAbsencesType, paramTaskId, paramDateKey,
					paramStartDate, paramEndDate, pageable);
			return new PageResponse<>(PropertiesResponse.STATUS_MESSAGE_OK, PropertiesResponse.STATUS_CODE_OK,
					results.getContent(), results.getNumber(), results.getSize(), results.getTotalPages(),
					results.isLast(), instancesLogsService.countInstancesLogByReqIdOutbox(username));

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@SuppressWarnings("unused")
	@GetMapping("/list-assign")
	@ResponseBody
	public PageResponse<ReAssignerDTO> listAssign(Pageable pageable,
			@RequestParam(name = "instancesId", defaultValue = "") String paramInstancesId,
			@RequestParam(name = "businessProcess", defaultValue = "") String paramBusinessProcess,
			@RequestParam(name = "absencesType", defaultValue = "") String paramAbsencesType,
			@RequestParam(name = "absencesDesc", defaultValue = "") String paramAbsencesDesc,
			@RequestParam(name = "requesterId", defaultValue = "") String paramRequesterId,
			@RequestParam(name = "requesterName", defaultValue = "") String paramRequesterName,
			@RequestParam(name = "assigner", defaultValue = "") String paramAssigner,
			@RequestParam(name = "assignerName", defaultValue = "") String paramAssignerName) {
		String message = null;
		try {

			if (!"".equals(paramAssigner) || !"".equals(paramRequesterId) || !"".equals(paramAbsencesType)) {
				if (!"".equals(paramAssigner)) {
					message = " filter Assigner !!!";
					Long var = new Long(paramAssigner);
				}
				if (!"".equals(paramRequesterId)) {
					message = " filter Requester !!!";
					Long var = new Long(paramRequesterId);
				}
				if (!"".equals(paramAbsencesType)) {
					message = " filter Absences Type !!!";
					Long var = new Long(paramAbsencesType);
				}
			}
			Page<ReAssignerDTO> results = instancesLogsService.listAssigner(paramInstancesId, paramBusinessProcess,
					paramAbsencesType, paramAbsencesDesc, paramRequesterId, paramRequesterName, paramAssigner,
					paramAssignerName, validatePageable(pageable, ConfigSortDefault.INSTANCES_ID));
			return new PageResponse<>(PropertiesResponse.STATUS_MESSAGE_OK, PropertiesResponse.STATUS_CODE_OK,
					results.getContent(), results.getNumber(), results.getSize(), results.getTotalPages(),
					results.isLast(), new Long(0));

		} catch (Exception e) {
			e.printStackTrace();
			List<ReAssignerDTO> list = new ArrayList<ReAssignerDTO>();
			Page<ReAssignerDTO> model = new PageImpl<ReAssignerDTO>(list, pageable, 0);

			return new PageResponse<>(PropertiesResponse.INPUT_ERROR_NUMERIC + message,
					PropertiesResponse.STATUS_CODE_CLIENT_ERROR, model.getContent(), model.getNumber(), model.getSize(),
					model.getTotalPages(), model.isLast(), new Long(0));
		}
	}

	@SuppressWarnings("unused")
	@GetMapping("/history-task/{module}")
	@ResponseBody
	public PageResponse<InstancesLogsDTO> historyTask(Pageable pageable, String username, @PathVariable String module,
			@RequestParam(name = "instancesId", defaultValue = "") String paramInstancesId,
			@RequestParam(name = "absencesDesc", defaultValue = "") String paramAbsencesDesc,
			@RequestParam(name = "requesterId", defaultValue = "") String paramRequesterId,
			@RequestParam(name = "requesterName", defaultValue = "") String paramRequesterName,
			@RequestParam(name = "dateKey", defaultValue = "") String paramDateKey,
			@RequestParam(name = "startDate", defaultValue = "") String paramStartDate,
			@RequestParam(name = "endDate", defaultValue = "") String paramEndDate) {

		String message = null;
		try {
			String businessProcess = null;
			if (!"".equals(paramInstancesId) || !"".equals(paramRequesterId)) {
				if (!"".equals(paramInstancesId)) {
					message = "Instances ID ";
					Long param = new Long(paramInstancesId);
				}

				if (!"".equals(paramRequesterId)) {
					message = "Requester ID ";
					Long param = new Long(paramRequesterId);
				}
			}

			if (module.equals("leave") || module.equals("permit")) {
				if (module.equals("leave")) {
					businessProcess = businessProcessConfig.getLeave();
				} else if (module.equals("permit")) {
					businessProcess = businessProcessConfig.getPermit();
				}
				Page<InstancesLogsDTO> results = instancesLogsService.historyTask(username, businessProcess,
						paramInstancesId, paramAbsencesDesc, paramRequesterId, paramRequesterName, paramDateKey,
						paramStartDate, paramEndDate, validatePageable(pageable, ConfigSortDefault.INSTANCES_ID));

				return new PageResponse<>(PropertiesResponse.STATUS_MESSAGE_OK, PropertiesResponse.STATUS_CODE_OK,
						results.getContent(), results.getNumber(), results.getSize(), results.getTotalPages(),
						results.isLast(),
						instancesLogsService.countInstancesLogByRequesterId(username, businessProcess));
			} else if (module.equals("all")) {
				Page<InstancesLogsDTO> results = instancesLogsService.historyTaskAll(username, paramAbsencesDesc,
						paramRequesterId, paramRequesterName, pageable);

				return new PageResponse<>(PropertiesResponse.STATUS_MESSAGE_OK, PropertiesResponse.STATUS_CODE_OK,
						results.getContent(), results.getNumber(), results.getSize(), results.getTotalPages(),
						results.isLast(), instancesLogsService.countInstancesLogByRequesterIdAll(username));
			} else {
				List<InstancesLogsDTO> list = new ArrayList<InstancesLogsDTO>();
				Page<InstancesLogsDTO> model = new PageImpl<InstancesLogsDTO>(list, pageable, 0);

				return new PageResponse<>(PropertiesResponse.STATUS_MESSAGE_OK, PropertiesResponse.STATUS_CODE_OK,
						model.getContent(), model.getNumber(), model.getSize(), model.getTotalPages(), model.isLast(),
						new Long(0));
			}
		} catch (Exception e) {
			e.printStackTrace();
			List<InstancesLogsDTO> list = new ArrayList<InstancesLogsDTO>();
			Page<InstancesLogsDTO> model = new PageImpl<InstancesLogsDTO>(list, pageable, 0);

			return new PageResponse<>(PropertiesResponse.INPUT_ERROR_NUMERIC + message,
					PropertiesResponse.STATUS_CODE_CLIENT_ERROR, model.getContent(), model.getNumber(), model.getSize(),
					model.getTotalPages(), model.isLast(), new Long(0));
		}
	}

	@SuppressWarnings("unused")
	@GetMapping("/history-task-mobile/{module}")
	@ResponseBody
	public PageResponse<InstancesLogsDTO> historyTaskForMobile(Pageable pageable, String username,
			@PathVariable String module, @RequestParam(name = "instancesId", defaultValue = "") String paramInstancesId,
			@RequestParam(name = "absencesDesc", defaultValue = "") String paramAbsencesDesc,
			@RequestParam(name = "requesterId", defaultValue = "") String paramRequesterId,
			@RequestParam(name = "requesterName", defaultValue = "") String paramRequesterName,
			@RequestParam(name = "dateKey", defaultValue = "") String paramDateKey,
			@RequestParam(name = "startDate", defaultValue = "") String paramStartDate,
			@RequestParam(name = "endDate", defaultValue = "") String paramEndDate) {

		String message = null;
		try {
			String businessProcess = null;
			if (!"".equals(paramInstancesId) || !"".equals(paramRequesterId)) {
				if (!"".equals(paramInstancesId)) {
					message = "Instances ID ";
					Long param = new Long(paramInstancesId);
				}

				if (!"".equals(paramRequesterId)) {
					message = "Requester ID ";
					Long param = new Long(paramRequesterId);
				}
			}

			if (module.equals("leave") || module.equals("permit")) {
				if (module.equals("leave")) {
					businessProcess = "Leave";
				} else if (module.equals("permit")) {
					businessProcess = businessProcessConfig.getPermit();
				}
				Page<InstancesLogsDTO> results = instancesLogsService.historyTaskForMobile(username, businessProcess,
						paramInstancesId, paramAbsencesDesc, paramRequesterId, paramRequesterName, paramDateKey,
						paramStartDate, paramEndDate, pageable);

				return new PageResponse<>(PropertiesResponse.STATUS_MESSAGE_OK, PropertiesResponse.STATUS_CODE_OK,
						results.getContent(), results.getNumber(), results.getSize(), results.getTotalPages(),
						results.isLast(),
						instancesLogsService.countInstancesLogByRequesterId(username, businessProcess));
			} else if (module.equals("all")) {
				Page<InstancesLogsDTO> results = instancesLogsService.historyTaskAll(username, paramAbsencesDesc,
						paramRequesterId, paramRequesterName, pageable);

				return new PageResponse<>(PropertiesResponse.STATUS_MESSAGE_OK, PropertiesResponse.STATUS_CODE_OK,
						results.getContent(), results.getNumber(), results.getSize(), results.getTotalPages(),
						results.isLast(), instancesLogsService.countInstancesLogsByReqIdHistory(username));
			} else {
				List<InstancesLogsDTO> list = new ArrayList<InstancesLogsDTO>();
				Page<InstancesLogsDTO> model = new PageImpl<InstancesLogsDTO>(list, pageable, 0);

				return new PageResponse<>(PropertiesResponse.STATUS_MESSAGE_OK, PropertiesResponse.STATUS_CODE_OK,
						model.getContent(), model.getNumber(), model.getSize(), model.getTotalPages(), model.isLast(),
						new Long(0));
			}
		} catch (Exception e) {
			e.printStackTrace();
			List<InstancesLogsDTO> list = new ArrayList<InstancesLogsDTO>();
			Page<InstancesLogsDTO> model = new PageImpl<InstancesLogsDTO>(list, pageable, 0);

			return new PageResponse<>(PropertiesResponse.INPUT_ERROR_NUMERIC + message,
					PropertiesResponse.STATUS_CODE_CLIENT_ERROR, model.getContent(), model.getNumber(), model.getSize(),
					model.getTotalPages(), model.isLast(), new Long(0));
		}
	}

	@GetMapping("/detail-history-task/{instancesId}")
	@ResponseBody
	public Map<Object, Object> detailtaskLogs(@PathVariable String instancesId) {
		Map<Object, Object> map = new HashMap<Object, Object>();
		try {
			List<TaskLogs> listTask = new ArrayList<TaskLogs>();
			List<TaskLogsResponse> response = new ArrayList<TaskLogsResponse>();
			listTask = taskLogsService.detailTaskLogs(instancesId);
			for (int i = 0; i < listTask.size(); i++) {
				TaskLogsResponse model = new TaskLogsResponse();
				TaskLogs logs = listTask.get(i);
				model.setTaskId(String.valueOf(logs.getTaskId()));
				model.setInstancesId(String.valueOf(logs.getInstanceId().getInstancesId()));
				model.setCreatedBy(logs.getCreatedBy().concat(" - ").concat(findName(logs.getCreatedBy())));
				model.setCreatedDt(formateTimestamp(String.valueOf(logs.getCreatedDt())));
				model.setUserStages(logs.getUserStages().concat(" - ").concat(findName(logs.getUserStages())));
				model.setStages(logs.getStages());
				model.setFlagTaskComplete(logs.getFlagTaskComplete());
				response.add(model);
			}
			map.put("status", 200);
			map.put("listResponse", response);
			return map;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			map.put("status", 400);
			map.put("error", e.getMessage());
			return map;
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes", "unused" })
	private List<TaskResponse> getlistTask(String username) {
		List<TaskResponse> response = new ArrayList<TaskResponse>();

		JBPM_MappingResponse mapping = mappingURL.instancesOwner();
		HttpEntity<LeaveRequest> requestEntity = new HttpEntity<>(headerSettings.setupHeaderAccept());

		UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(mapping.getUrl()).queryParam("user",
				username);

		Object resp = restTemplate.exchange(builder.buildAndExpand(mapping.getUrl()).toUri(), mapping.getMethod(),
				requestEntity, Object.class);

		ResponseEntity<HashMap> responseEntity = (ResponseEntity<HashMap>) resp;
		List<Object> taskList = (List<Object>) responseEntity.getBody().get("task-summary");
		for (int i = 0; i < taskList.size(); i++) {
			Map<String, Object> taskValue = (Map<String, Object>) taskList.get(i);
			response.add(setModel(taskValue));
		}

		return response;
	}

	@PostMapping("/find-date-history")
	@ResponseBody
	public Map<String, Object> validateHistory(@RequestBody HistoryDateRequest request) {
		Map<String, Object> response = new HashMap<String, Object>();

		validateHistory(request);
		return response;
	}

	@GetMapping("/find-history-assigner/{username}")
	@ResponseBody
	public PageResponse<TaskLogsDTO> findHistoryAssigner(@PathVariable String username,
			@RequestParam(name = "instanceId", defaultValue = "") String paramInstanceId,
			@RequestParam(name = "createdBy", defaultValue = "") String paramCreatedBy,
			@RequestParam(name = "startDate", defaultValue = "") String paramStartDate,
			@RequestParam(name = "endDate", defaultValue = "") String paramEndDate,
			@RequestParam(name = "stages", defaultValue = "") String paramStages, Pageable pageable) {

		try {
			Page<TaskLogsDTO> results = taskLogsService.findHistoryAssigner(username, paramInstanceId, paramCreatedBy,
					paramStartDate, paramEndDate, paramStages, pageable);
			return new PageResponse<>(PropertiesResponse.STATUS_MESSAGE_OK, PropertiesResponse.STATUS_CODE_OK,
					results.getContent(), results.getNumber(), results.getSize(), results.getTotalPages(),
					results.isLast(), new Long(0));
		} catch (Exception e) {
			e.printStackTrace();

			List<TaskLogsDTO> list = new ArrayList<TaskLogsDTO>();
			Page<TaskLogsDTO> model = new PageImpl<TaskLogsDTO>(list, pageable, 0);

			return new PageResponse<>(PropertiesResponse.INPUT_ERROR_NUMERIC,
					PropertiesResponse.STATUS_CODE_CLIENT_ERROR, model.getContent(), model.getNumber(), model.getSize(),
					model.getTotalPages(), model.isLast(), new Long(0));
		}
	}

	@GetMapping("/find-history-assigner")
	@ResponseBody
	public PageResponse<TaskLogsDTO> findAllHistoryAssigner(
			@RequestParam(name = "instanceId", defaultValue = "") String paramInstanceId,
			@RequestParam(name = "createdBy", defaultValue = "") String paramCreatedBy,
			@RequestParam(name = "createdDt", defaultValue = "") String paramCreatedDt,
			@RequestParam(name = "stages", defaultValue = "") String paramStages,
			@RequestParam(name = "userStages", defaultValue = "") String paramUserStages,
			@RequestParam(name = "startDate", defaultValue = "") String paramStartDate,
			@RequestParam(name = "endDate", defaultValue = "") String paramEndDate, Pageable pageable) {

		try {
			Page<TaskLogsDTO> results = taskLogsService.findHistoryAll(paramInstanceId, paramCreatedBy, paramStages,
					paramUserStages, paramStartDate, paramEndDate, pageable);
			return new PageResponse<>(PropertiesResponse.STATUS_MESSAGE_OK, PropertiesResponse.STATUS_CODE_OK,
					results.getContent(), results.getNumber(), results.getSize(), results.getTotalPages(),
					results.isLast(), new Long(0));
		} catch (Exception e) {
			e.printStackTrace();

			List<TaskLogsDTO> list = new ArrayList<TaskLogsDTO>();
			Page<TaskLogsDTO> model = new PageImpl<TaskLogsDTO>(list, pageable, 0);

			return new PageResponse<>(PropertiesResponse.INPUT_ERROR_NUMERIC,
					PropertiesResponse.STATUS_CODE_CLIENT_ERROR, model.getContent(), model.getNumber(), model.getSize(),
					model.getTotalPages(), model.isLast(), new Long(0));
		}
	}
}
