package com.mega.ws.login;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "output")
public class Output {
    @XmlElement(name = "ResponseCode")
    public String ResponseCode;
    @XmlElement(name = "ResponseDescription")
    public String ResponseDescription;
}
