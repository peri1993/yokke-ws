package com.mega.ws.login;

import javax.xml.bind.annotation.*;
import com.mega.ws.login.Output;

@XmlRootElement(name = "getServiceLdapResponse", namespace= "http://ldap.service.ws.mega.com/ServiceLdap")
public class Response {
    @XmlElement(name = "output")
    public Output output;
}
